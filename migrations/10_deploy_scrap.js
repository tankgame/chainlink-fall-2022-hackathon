const Scrap = artifacts.require("Scrap");

const settings = {
	name: "Scrap",
	symbol: "SCRAP",
};

module.exports = async function (deployer, network) {
	console.log(`Deploying Scrap on ${network}`);
	await deployer.deploy(Scrap, settings.name, settings.symbol);
};
