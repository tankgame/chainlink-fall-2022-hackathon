const MechanicShop = artifacts.require("MechanicShop");
const Tanks = artifacts.require("Tanks");
const Scrap = artifacts.require("Scrap");
const Metadata = artifacts.require("Metadata");

const settings = {
	restorationRate: "1",
	restorationPeriod: "100",
	restorationPrice: "1",
	refillRate: "1",
	refillPeriod: "100",
	refillPrice: "1",
};

module.exports = async function (deployer) {
	await deployer.deploy(
		MechanicShop,
		Tanks.address,
		Scrap.address,
		Metadata.address,
		settings.restorationRate,
		settings.restorationPeriod,
		settings.restorationPrice,
		settings.refillRate,
		settings.refillPeriod,
		settings.refillPrice
	);
};
