const Ammo = artifacts.require("Ammo");

const settings = {
	ammoTypes: [
		[
			{ name: "Golden Snake Fank", damage: 50, spread: 10, weight: 10 },
			"QmNggot4wberU94K2Nq7fjSeRF9ePNT9D7wj3RjLCGGnoN",
		],
		[
			{ name: "Black Mirror Balls", damage: 75, spread: 2, weight: 15 },
			"QmbHXcjunPtfhHrtEFVnHbTQnKnbV6Q6ja3AzSdMqD3gXC",
		],
	],
};

module.exports = async function (deployer, network, accounts) {
	const ammoInstance = await Ammo.deployed();

	for (const [index, ammo] of settings.ammoTypes) {
		await deployer
			.then(async () => {
				console.log(`Adding URI for token ${index + 1}`);
				await ammoInstance.createAmmoType(
					ammo[0].name,
					ammo[0].damage,
					ammo[0].spread,
					ammo[0].weight
				);
			})
			.then(async () => {
				console.log(`Adding URI for token ${index + 1}`);
				await ammoInstance.setURI(index + 1, ammo[1]);
			});
	}
};
