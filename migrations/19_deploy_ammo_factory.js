const AmmoFactory = artifacts.require("AmmoFactory");
const Ammo = artifacts.require("Ammo");
const Scrap = artifacts.require("Scrap");

module.exports = async function (deployer, network) {
	console.log(`Deploying Ammo Factory on ${network}`);
	await deployer.deploy(AmmoFactory, Ammo.address, Scrap.address);
};
