const VRFCoordinatorV2Mock = artifacts.require("VRFCoordinatorV2Mock");

const settings = {
  baseFee: "0",
  gasPriceLink: "0",
};

module.exports = async function (deployer, network) {
  if (network == "development") {
    console.log("Deploying Mock VRF Coordinator to Development Network");
    await deployer.deploy(
      VRFCoordinatorV2Mock,
      settings.baseFee,
      settings.gasPriceLink
    );
  }
};
