const TerrainGenerator = artifacts.require("TerrainGenerator");

const settings = {
  seed: 1,
  width: 1024,
};

module.exports = async function (deployer) {
  await deployer.deploy(TerrainGenerator, settings.seed, settings.width);
};
