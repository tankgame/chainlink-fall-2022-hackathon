const TankFactory = artifacts.require("TankFactory");
const Tanks = artifacts.require("Tanks");
const Scrap = artifacts.require("Scrap");

const settings = {
	137: {
		leadTime: "600",
	},
	80001: {
		leadTime: "600",
	},
	development: {
		leadTime: "60",
	},
};

module.exports = async function (deployer, network) {
	console.log(`Deploying Tank Factory on ${network}`);
	if (network == "dashboard") {
		let networkId = await getNetworkId();
		await deployer.deploy(
			TankFactory,
			Tanks.address,
			Scrap.address,
			settings[networkId].leadTime
		);
	} else if (network == "development") {
		await deployer.deploy(
			TankFactory,
			Tanks.address,
			Scrap.address,
			settings.development.leadTime
		);
	}
};

async function getNetworkId() {
	let networkId = await web3.eth.getChainId();
	console.log(`Connected Network: ${networkId}`);
	return networkId;
}
