const SoldierName = artifacts.require("SoldierName");
const Scrap = artifacts.require("Scrap");

const settings = {
	minimumNameLength: "3",
	nameChangeCooldown: "604800",
	scrapPrice: "50",
};

module.exports = async function (deployer, network) {
	await deployer.deploy(
		SoldierName,
		Scrap.address,
		settings.minimumNameLength,
		settings.nameChangeCooldown,
		settings.scrapPrice
	);
};

async function getNetworkId() {
	let networkId = await web3.eth.getChainId();
	console.log(`Connected Network: ${networkId}`);
	return networkId;
}
