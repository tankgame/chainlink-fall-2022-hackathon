const Medals = artifacts.require("Medals");

const settings = {
	tokenURI: "",
};

module.exports = async function (deployer, network) {
	console.log(`Deploying Medals on ${network}`);
	await deployer.deploy(Medals, settings.tokenURI);
};
