const Ammo = artifacts.require("Ammo");

const settings = {
	tokenURI: "https://atlascorp.mypinata.io/ipfs/",
};

module.exports = async function (deployer, network) {
	console.log(`Deploying Ammo on ${network}`);
	await deployer.deploy(Ammo, settings.tokenURI);
};
