const Tanks = artifacts.require("Tanks");
const Metadata = artifacts.require("Metadata");

const settings = {
	name: "Tanks",
	symbol: "TANKS",
	tokenURI: "https://tankgame-meta.atlascorp.io/",
};

module.exports = async function (deployer, network) {
	console.log(`Deploying Tanks on ${network}`);
	await deployer.deploy(
		Tanks,
		settings.name,
		settings.symbol,
		settings.tokenURI,
		Metadata.address
	);
};
