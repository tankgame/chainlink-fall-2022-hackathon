const Medals = artifacts.require("Medals");
const Battle = artifacts.require("Battle");

module.exports = async function (deployer) {
	await deployer.then(async () => {
		let instance = await Medals.deployed();
		let role = web3.utils.sha3("MEDAL_AWARDER");
		await grantRole(role, Battle.address, instance);
	});
};

async function grantRole(role, address, instance) {
	console.log(`Granting ${address} the ${role} ROLE`);
	console.log(
		"=================================================================="
	);
	console.log("Confirming Role");
	await instance.grantRole(role, address);
	let hasRole = await instance.hasRole(role, address);
	console.log(`${address}: ${hasRole}`);
}
