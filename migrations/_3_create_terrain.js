const TerrainGenerator = artifacts.require("TerrainGenerator");

const settings = {
  seed: 1,
  width: 1024,
  samplePeriods: [0, 16, 32, 64, 128],
  weights: [1, 10, 100, 1000, 10000],
};

module.exports = async function (deployer) {
  for (let i = 1; i < settings.samplePeriods.length; i++) {
    await deployer.then(async () =>
      populateSampledTerrain(settings.samplePeriods[i])
    );
  }

  await deployer.then(async () =>
    populateSmoothTerrain(settings.samplePeriods, settings.weights, 0, 511)
  );

  await deployer.then(async () =>
    populateSmoothTerrain(settings.samplePeriods, settings.weights, 511, 1024)
  );
};

async function populateSampledTerrain(samplePeriod) {
  console.log(`Populating Sample Terrain with period ${samplePeriod}`);
  let instance = await TerrainGenerator.deployed();
  await instance.populateSampledTerrain(samplePeriod);
}

async function populateSmoothTerrain(samplePeriods, weights, start, end) {
  console.log(
    `Populating Smooth Terrain from index ${start} to ${end} with sample periods ${samplePeriods} and weights ${weights}`
  );
  let instance = await TerrainGenerator.deployed();
  await instance.populateSmoothTerrain(samplePeriods, weights, start, end);
}
