const Metadata = artifacts.require("Metadata");

module.exports = async function (deployer, network) {
	console.log(`Deploying Metadata on ${network}`);
	await deployer.deploy(Metadata);
};
