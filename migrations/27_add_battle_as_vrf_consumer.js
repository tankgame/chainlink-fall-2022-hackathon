const Battle = artifacts.require("Battle");
const VRFCoordinatorV2Mock = artifacts.require("VRFCoordinatorV2Mock");

const settings = {
	137: {
		coordinator: "",
		subscription: 0,
	},
	80001: {
		coordinator: "0x7a1bac17ccc5b313516c5e16fb24f7659aa5ebed",
		subscription: 2549,
	},
};

module.exports = async function (deployer, network) {
	if (network == "development") {
		console.log("Adding Battle as VRF Consumer on MOCK VRF Coordinator");
		const coordinatorInstance = await VRFCoordinatorV2Mock.deployed();
		await deployer.then(async () => {
			await coordinatorInstance.addConsumer(1, Battle.address);
		});
	} else if (network == "dashboard") {
		const networkId = await getNetworkId();
		console.log(
			`Adding Battle as VRF Consumer to subscription ${settings[networkId].subscription} on network ${networkId}`
		);
		const coordinatorInstance = await VRFCoordinatorV2Mock.at(
			settings[networkId].coordinator
		);
		await deployer.then(async () => {
			await coordinatorInstance.addConsumer(
				settings[networkId].subscription,
				Battle.address
			);
		});
	}
};

async function getNetworkId() {
	let networkId = await web3.eth.getChainId();
	console.log(`Connected Network: ${networkId}`);
	return networkId;
}
