const Metadata = artifacts.require("Metadata");
const MechanicShop = artifacts.require("MechanicShop");

module.exports = async function (deployer, network, accounts) {
	await deployer.then(async () => {
		let instance = await Metadata.deployed();
		let role = web3.utils.sha3("METADATA_EDITOR");
		await grantRole(role, MechanicShop.address, instance);
	});
};

async function grantRole(role, address, instance) {
	console.log(`Granting ${address} the ${role} ROLE`);
	console.log(
		"=================================================================="
	);
	console.log("Confirming Role");
	await instance.grantRole(role, address);
	let hasRole = await instance.hasRole(role, address);
	console.log(`${address}: ${hasRole}`);
}
