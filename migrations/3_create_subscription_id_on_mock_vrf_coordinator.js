const VRFCoordinatorV2Mock = artifacts.require("VRFCoordinatorV2Mock");

const settings = {
	amountToFund: "",
};

module.exports = async function (deployer, network) {
	if (network == "development") {
		let instance = await VRFCoordinatorV2Mock.deployed();
		let subscriptionId;
		await deployer
			.then(async () => {
				console.log("Creating Subscription");
				subscriptionId = await instance.createSubscription();
				let response = await web3.eth.getTransactionReceipt(
					subscriptionId.tx
				);

				console.log(response);
				console.log({ logs: response.logs[0].topics[1] });
				subscriptionId = response.logs[0].topics[1];
			})
			.then(async () => {
				console.log(`Funding Subscription ${subscriptionId}`);
				await instance.fundSubscription(subscriptionId, "100000000");
			});
	}
};
