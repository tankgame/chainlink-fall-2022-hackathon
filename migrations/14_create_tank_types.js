const Tanks = artifacts.require("Tanks");

const settings = {
	tankTypes: [
		{
			name: "Hyas",
			description:
				"Fast, handles well, and is equipped with a rapid-fire canon.",
			maxHealth: 100,
			maxGas: 250,
			accuracy: 98,
		},
		{
			name: "Calypso",
			description: "It's yellow and makes a loud disturbing noise.",
			maxHealth: 100,
			maxGas: 250,
			accuracy: 98,
		},
		{
			name: "Dione",
			description:
				"Pound for pound she's more dangerous than a live machine gun!",
			maxHealth: 100,
			maxGas: 250,
			accuracy: 98,
		},
	],
};

module.exports = async function (deployer, network) {
	let instance = await Tanks.deployed();
	console.log("Creating Tank Types");
	for (let tank of settings.tankTypes) {
		await deployer.then(async () => {
			await instance.createTankType(
				tank.name,
				tank.description,
				tank.maxHealth,
				tank.maxGas,
				tank.accuracy
			);
		});
	}
};
