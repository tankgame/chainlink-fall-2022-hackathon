const Metadata = artifacts.require("Metadata");
const Tanks = artifacts.require("Tanks");

const settings = {
	metadataFields: ["health", "gas"],
};

module.exports = async function (deployer) {
	for (let field of settings.metadataFields) {
		await deployer.then(async () => {
			let metadataInstance = await Metadata.deployed();
			console.log(`Adding ${field} to the Tanks metadata`);
			await metadataInstance.addMetadataField(Tanks.address, field);
		});
	}
};
