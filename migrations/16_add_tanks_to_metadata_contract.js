const Metadata = artifacts.require("Metadata");
const Tanks = artifacts.require("Tanks");

module.exports = async function (deployer) {
	await deployer.then(async () => {
		let metadataInstance = await Metadata.deployed();
		console.log(
			`Adding Tanks: ${Tanks.address} to the approved contracts list on the Metadata contract`
		);
		await metadataInstance.addApprovedContract(Tanks.address);
	});
};
