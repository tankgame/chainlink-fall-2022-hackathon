const Ammo = artifacts.require("Ammo");

module.exports = async function (deployer, network, accounts) {
  let role = web3.utils.sha3("AMMO_DEVELOPER");
  let instance = await Ammo.deployed();

  await deployer.then(async () => await grantRole(role, accounts[0], instance));
};

async function grantRole(role, address, instance) {
  console.log(`Granting ${address} the ${role} ROLE`);
  console.log(
    "=================================================================="
  );
  console.log("Confirming Role");
  await instance.grantRole(role, address);
  let hasRole = await instance.hasRole(role, address);
  console.log(`${address}: ${hasRole}`);
}
