const Metadata = artifacts.require("Metadata");

module.exports = async function (deployer, network, accounts) {
	console.log(`Deploying Metadata on ${network}`);
	await deployer.then(async () => {
		let instance = await Metadata.deployed();
		let role = web3.utils.sha3("METADATA_CONTRACT_OPERATOR");
		await grantRole(role, accounts[0], instance);
	});
};

async function grantRole(role, address, instance) {
	console.log(`Granting ${address} the ${role} ROLE`);
	console.log(
		"=================================================================="
	);
	console.log("Confirming Role");
	await instance.grantRole(role, address);
	let hasRole = await instance.hasRole(role, address);
	console.log(`${address}: ${hasRole}`);
}
