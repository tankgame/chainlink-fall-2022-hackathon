const ProjectileCalculator = artifacts.require("ProjectileCalculator");
const Trigonometry = artifacts.require("Trigonometry");

module.exports = async function (deployer) {
	console.log(`Linking Trigonometry Library to Projectile Calculator`);
	await deployer.link(Trigonometry, ProjectileCalculator);
	console.log(`Deploying Projectile Calculator`);
	await deployer.deploy(ProjectileCalculator);
	console.log(`Making Projectile Path`);
	//   let instance = await ProjectileCalculator.deployed();
	//   await instance.makePath(4096, 1, 0, 0, 1000);
	//   await deployer.then(async () => makePathX(2048, 1, 0));
};
