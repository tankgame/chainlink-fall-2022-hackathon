const Metadata = artifacts.require("Metadata");
const Tanks = artifacts.require("Tanks");

module.exports = async function (deployer) {
	await deployer.then(async () => {
		let instance = await Metadata.deployed();
		let role = web3.utils.sha3("METADATA_EDITOR");
		await grantRole(role, Tanks.address, instance);
	});
};

async function grantRole(role, address, instance) {
	console.log(`Granting ${address} the ${role} ROLE`);
	console.log(
		"=================================================================="
	);
	console.log("Confirming Role");
	await instance.grantRole(role, address);
	let hasRole = await instance.hasRole(role, address);
	console.log(`${address}: ${hasRole}`);
}
