const Stockpile = artifacts.require("Stockpile");
const Tanks = artifacts.require("Tanks");
const Ammo = artifacts.require("Ammo");
const Scrap = artifacts.require("Scrap");

module.exports = async function (deployer, network, accounts) {
	let tankManufacturerRole = web3.utils.sha3("TANK_MANUFACTURER");
	let ammoManufacturerRole = web3.utils.sha3("AMMO_MANUFACTURER");
	let armoryManagerRole = web3.utils.sha3("ARMORY_MANAGER");
	let scrapMinerRole = web3.utils.sha3("SCRAP_MINER");
	let tankInstance = await Tanks.deployed();
	let stockpileInstance = await Stockpile.deployed();
	let ammoInstance = await Ammo.deployed();
	let scrapInstance = await Scrap.deployed();

	console.log("Assigning Stockpile the Tank Manufacturer role");
	await deployer.then(
		async () =>
			await grantRole(
				tankManufacturerRole,
				Stockpile.address,
				tankInstance
			)
	);
	// await deployer.then(
	//   async () => await grantRole(tankManufacturerRole, accounts[0], tankInstance)
	// );
	console.log(
		`Assigning Armory Role to ${accounts[0]} on the Stockpile Contract`
	);
	// await deployer.then(
	// 	async () =>
	// 		await grantRole(
	// 			armoryManagerRole,
	// 			Stockpile.address,
	// 			stockpileInstance
	// 		)
	// );
	await deployer.then(
		async () =>
			await grantRole(armoryManagerRole, accounts[0], stockpileInstance)
	);

	console.log(`Assigning Stockpile the Ammo Manufacturer role`);
	await deployer.then(
		async () =>
			await grantRole(
				ammoManufacturerRole,
				Stockpile.address,
				ammoInstance
			)
	);
	// await deployer.then(
	//   async () => await grantRole(ammoManufacturerRole, accounts[0], ammoInstance)
	// );

	console.log(`Assigning Stockpile the Scrap Miner role`);
	await deployer.then(
		async () =>
			await grantRole(scrapMinerRole, Stockpile.address, scrapInstance)
	);
	// await deployer.then(
	//   async () => await grantRole(scrapMinerRole, accounts[0], scrapInstance)
	// );
};

async function grantRole(role, address, instance) {
	console.log(`Granting ${address} the ${role} ROLE`);
	console.log(
		"=================================================================="
	);
	console.log("Confirming Role");
	await instance.grantRole(role, address);
	let hasRole = await instance.hasRole(role, address);
	console.log(`${address}: ${hasRole}`);
}
