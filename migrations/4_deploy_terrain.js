const Terrain = artifacts.require("Terrain");

const settings = {
	gravity: "981",
};

module.exports = async function (deployer, network) {
	console.log(`Deploying Terrain on ${network}`);
	await deployer.deploy(Terrain, settings.gravity, { gas: 24000000 });
};
