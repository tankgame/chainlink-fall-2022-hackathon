const Battle = artifacts.require("Battle");
const Tanks = artifacts.require("Tanks");
const Ammo = artifacts.require("Ammo");
const Scrap = artifacts.require("Scrap");
const Medals = artifacts.require("Medals");
const Metadata = artifacts.require("Metadata");
const Terrain = artifacts.require("Terrain");
const VRFCoordinatorV2Mock = artifacts.require("VRFCoordinatorV2Mock");
const Trigonometry = artifacts.require("Trigonometry");

const settings = {
	137: {
		VRFManagerConstructorArgs: {
			subscriptionId: 377,
			vrfCoordinator: "0xAE975071Be8F8eE67addBC1A82488F1C24858067",
			keyHash:
				"0x6e099d640cde6de9d40ac749b4b594126b0169747122711109c9985d47751f93", // 200 GWei Key Hash
			callbackGasLimit: 2500000,
			requestConfirmations: 3,
		},
	},
	80001: {
		VRFManagerConstructorArgs: {
			subscriptionId: 2549,
			vrfCoordinator: "0x7a1BaC17Ccc5b313516C5E16fb24f7659aA5ebed",
			keyHash:
				"0x4b09e658ed251bcafeebbc69400383d49f344ace09b9576fe248bb02c003fe9f", // 500 GWei Key Hash
			callbackGasLimit: 2500000,
			requestConfirmations: 3,
		},
		terrainContract: "0xC022058daF24182437c196909ec72BFFfbB8cd32",
	},
};

module.exports = async function (deployer, network) {
	if (network == "dashboard") {
		let networkId = await getNetworkId();

		console.log(`Linking Trigonometry Library to Battle`);
		console.log(`Deploying Battle on ${network}`);
		await deployer.link(Trigonometry, Battle);
		await deployer.deploy(
			Battle,
			Terrain.address,
			Tanks.address,
			Ammo.address,
			Scrap.address,
			Medals.address,
			Metadata.address,
			settings[networkId].VRFManagerConstructorArgs
		);
	} else if (network == "development") {
		console.log(`Linking Trigonometry Library to Battle`);
		await deployer.link(Trigonometry, Battle);
		await deployer.deploy(
			Battle,
			settings[networkId].terrainContract,
			Tanks.address,
			Ammo.address,
			Scrap.address,
			Medals.address,
			Metadata.address,
			{
				subscriptionId: 1,
				vrfCoordinator: VRFCoordinatorV2Mock.address,
				keyHash:
					"0x4b09e658ed251bcafeebbc69400383d49f344ace09b9576fe248bb02c003fe9f",
				callbackGasLimit: 1000000,
				requestConfirmations: 3,
			}
		);
	}
};

async function getNetworkId() {
	let networkId = await web3.eth.getChainId();
	console.log(`Connected Network: ${networkId}`);
	return networkId;
}
