const Stockpile = artifacts.require("Stockpile");
const Tanks = artifacts.require("Tanks");
const Ammo = artifacts.require("Ammo");
const Scrap = artifacts.require("Scrap");

const settings = {
	maxClaim: "100",
	scrap: "200",
	ammo: "100",
};

module.exports = async function (deployer) {
	await deployer.deploy(
		Stockpile,
		Tanks.address,
		Scrap.address,
		Ammo.address,
		settings.maxClaim,
		settings.scrap,
		settings.ammo
	);
};
