import terrainContractData from "../contracts/Terrain.json";
import battleContractData from "../contracts/Battle.json";
import stockpileData from "../contracts/Stockpile.json";
import soldierNameData from "../contracts/SoldierName.json";
import scrapData from "../contracts/Scrap.json";
import tankFactoryData from "../contracts/TankFactory.json";
import ammoFactoryData from "../contracts/AmmoFactory.json";
import mechanicShopData from "../contracts/MechanicShop.json";
import tanksData from "../contracts/Tanks.json";
import ammoData from "../contracts/Ammo.json";
import trigonometryData from "../contracts/Trigonometry.json";
import metadataData from "../contracts/Metadata.json";
import medalsData from "../contracts/Medals.json";

export const addresses = (dev: "dev" | "mumbai" | "mainnet") => ({
  terrain:
    dev == "dev"
      ? terrainContractData.networks["1337"].address
      : dev == "mainnet"
      ? ""
      : terrainContractData.networks["80001"].address,
  battle:
    dev == "dev"
      ? battleContractData.networks["1337"].address
      : dev == "mainnet"
      ? ""
      : battleContractData.networks["80001"].address,
  stockpile:
    dev === "dev"
      ? stockpileData.networks["1337"].address
      : dev === "mainnet"
      ? ""
      : stockpileData.networks["80001"].address,
  soldierName:
    dev === "dev"
      ? soldierNameData.networks["1337"].address
      : dev === "mainnet"
      ? ""
      : soldierNameData.networks["80001"].address,
  scrap:
    dev === "dev"
      ? scrapData.networks["1337"].address
      : dev === "mainnet"
      ? ""
      : scrapData.networks["80001"].address,
  tankFactory:
    dev === "dev"
      ? tankFactoryData.networks["1337"].address
      : dev === "mainnet"
      ? ""
      : tankFactoryData.networks["80001"].address,
  ammoFactory:
    dev === "dev"
      ? ammoFactoryData.networks["1337"].address
      : dev === "mainnet"
      ? ""
      : ammoFactoryData.networks["80001"].address,
  mechanicShop:
    dev === "dev"
      ? mechanicShopData.networks["1337"].address
      : dev === "mainnet"
      ? ""
      : mechanicShopData.networks["80001"].address,
  tanks:
    dev === "dev"
      ? tanksData.networks["1337"].address
      : dev === "mainnet"
      ? ""
      : tanksData.networks["80001"].address,
  ammo:
    dev === "dev"
      ? ammoData.networks["1337"].address
      : dev === "mainnet"
      ? ""
      : ammoData.networks["80001"].address,
  trigonometry:
    dev === "dev"
      ? trigonometryData.networks["1337"].address
      : dev === "mainnet"
      ? ""
      : trigonometryData.networks["80001"].address,
  metadata:
    dev === "dev"
      ? metadataData.networks["1337"].address
      : dev === "mainnet"
      ? ""
      : metadataData.networks["80001"].address,
  medals:
    dev === "dev"
      ? medalsData.networks["1337"].address
      : dev === "mainnet"
      ? ""
      : medalsData.networks["80001"].address,
});

export const contracts = (
  dev: "dev" | "mumbai" | "mainnet",
  chainId: Chains
): IContract => ({
  terrain: {
    address: addresses(dev).terrain,
    abi: terrainContractData.abi,
    chainId,
  },
  battle: {
    address: addresses(dev).battle,
    abi: battleContractData.abi,
    chainId,
  },
  stockpile: {
    address: addresses(dev).stockpile,
    abi: stockpileData.abi,
    chainId,
  },
  soldierName: {
    address: addresses(dev).soldierName,
    abi: soldierNameData.abi,
    chainId,
  },
  scrap: {
    address: addresses(dev).scrap,
    abi: scrapData.abi,
    chainId,
  },
  tankFactory: {
    address: addresses(dev).tankFactory,
    abi: tankFactoryData.abi,
    chainId,
  },
  ammoFactory: {
    address: addresses(dev).ammoFactory,
    abi: ammoFactoryData.abi,
    chainId,
  },
  mechanicShop: {
    address: addresses(dev).mechanicShop,
    abi: mechanicShopData.abi,
    chainId,
  },
  tanks: {
    address: addresses(dev).tanks,
    abi: tanksData.abi,
    chainId,
  },
  ammo: {
    address: addresses(dev).ammo,
    abi: ammoData.abi,
    chainId,
  },
});
