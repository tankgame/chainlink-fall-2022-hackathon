import { BigNumber } from "ethers";

export const pages: { page: string; link: string }[] = [
  { page: "New Game", link: "/newgame" },
  { page: "Continue", link: "/continue" },
  { page: "Factories", link: "/factories" },
  { page: "Inventory", link: "/inventory" },
  { page: "Stockpile", link: "/stockpile" },
  { page: "Approvals", link: "/approvals" },
  { page: "Contracts", link: "/contracts" },
];

export const biggestNumber = BigNumber.from(
  "0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"
);
