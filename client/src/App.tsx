import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { LandingPage } from "./common/pages/LandingPage";
import { Footer } from "./common/components/Footer";
import { Inventory } from "./common/pages/Inventory";
import { ProtectedRoute } from "./common/pages/ProtectedRoute";
import { Factory } from "./common/pages/Factory";
import { NotFound } from "./common/pages/NotFound";
import backgroundAudio from "./assets/audio/backgroundAudio.mp3";
import { useRef, useState } from "react";
import { FiringRange } from "./common/pages/FiringRange";
import { Stockpile } from "./common/pages/Stockpile";
import { NewGame } from "./common/pages/NewGame";
import { Approvals } from "./common/pages/Approvals";
import { Continue } from "./common/pages/Continue";
import { Contracts } from "./common/pages/Contracts";

function App() {
  const [modal, setModal] = useState(true);
  const audioRef = useRef<HTMLAudioElement>(null);

  const handleClick = () => {
    if (!audioRef.current) return;
    audioRef.current.volume = 0.5;
    audioRef.current.play();
    setModal(false);
  };

  return (
    <BrowserRouter>
      <div className="App bg-rust bg-landing-bg bg-center bg-cover select-none overflow-auto">
        <div className="flex-1 relative">
          <Routes>
            <Route index element={<LandingPage />} />
            <Route path="*" element={<NotFound />} />
            <Route path="/" element={<ProtectedRoute />}>
              <Route path="/newgame" element={<NewGame />} />
              <Route path="/continue" element={<Continue></Continue>} />
              <Route path="/factories" element={<Factory />} />
              <Route path="/inventory" element={<Inventory />} />
              <Route path="/stockpile" element={<Stockpile />} />
              <Route path="/approvals" element={<Approvals />} />
              <Route path="/contracts" element={<Contracts />} />
              <Route path="/battle/:gameId" element={<FiringRange />} />
            </Route>
          </Routes>
          {modal && (
            <div
              className="absolute flex justify-center items-center bg-modal inset-0 p-3"
              onClick={handleClick}
            >
              <div
                className="bg-rust p-10 rounded-lg flex flex-col items-center gap-3"
                onClick={(e) => e.stopPropagation()}
              >
                <h2 className="font-bold text-lg text-center">
                  This Game was made for the Chainlink Hackathon
                </h2>
                <button
                  className="bg-rust-hover p-2 rounded-lg"
                  onClick={handleClick}
                >
                  Play Game
                </button>
              </div>
            </div>
          )}
        </div>
        <audio ref={audioRef} loop={true}>
          <source src={backgroundAudio} />
        </audio>
        <Footer showSound={!modal} audioRef={audioRef} />
      </div>
    </BrowserRouter>
  );
}

export default App;
