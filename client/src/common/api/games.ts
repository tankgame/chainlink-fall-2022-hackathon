export const activeHomeGames = (address: string) => `
{
  tankGames(
    where:{
      homePlayer_ : {
        id: "${address.toLowerCase()}"
      },
      gameState_not_in: ["GAME_OVER", "ABANDONED"]
    }
  ) {
    id
  }
}`;

export const activeAwayGames = (address: string) => `
{
  tankGames(
    where:{
      awayPlayer_ : {
        id: "${address.toLowerCase()}"
      },
      gameState_not_in: ["GAME_OVER", "ABANDONED"]
    }
  ) {
    id
  }
}`;

export const joinableGamesQuery = (address: string) => `
{
  tankGames(
    where: {
      gameState: "CREATED",
      homePlayer_not: "${address.toLowerCase()}"
    }
  ){
    id
  }
}`;
