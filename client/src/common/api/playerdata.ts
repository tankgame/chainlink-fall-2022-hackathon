export const playerDataFetch = (address: string) => `{
	players (where: {id: "${address}"}) {
		id
		medals {
			id
			quantity
		}
	}
}`;
