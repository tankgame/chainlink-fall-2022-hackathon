export const currentBattle = (battleId: string) => `{
  tankGames(where: { id: ${battleId}}) {
    homePlayerTank {
      health
      gas
      type {
        maxGas
        maxHealth
      }
    }
    awayPlayerTank {
      health
      gas
      type {
        maxGas
        maxHealth
      }
    }
    homePlayer {
      id
    }
    awayPlayer {
      id
    }
    winningTeam
    winner {
      id
    }
  }
}`;
