export const fetchTanksDelivering = (address: string) => `
{
  tankReceipts (
    where: 
    { 
      buyer_: 
      {
        id:"${address.toLowerCase()}"
      },
      delivered: false
    }
  ) {
    id
    timeOfPurchase
    expectedTimeOfDelivery
    delivered
  }
}
`;
