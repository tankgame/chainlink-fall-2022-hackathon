import { BigNumber, Contract } from "ethers";
import { createContext, useContext, useEffect, useMemo } from "react";
// import { toast } from "react-toastify";
import {
  useAccount,
  useContract,
  useContractReads,
  useProvider,
  useSigner,
} from "wagmi";

import { addresses, contracts } from "../../constants/contracts";
import { fetchTanksDelivering } from "../api/tanks";
import { useGetGames } from "../hooks/getGames";
import { useGetPlayerData } from "../hooks/getPlayerData";
import { useGetTanks } from "../hooks/getTanks";

export interface IEthereumContextValues {
  contractData: any;
  approvedList: any;
  battleReader: Contract | null;
  contractSigner?: { [key: string]: Contract };
  chainId: Chains;
  joinableGames: Game[];
  activeGames: Game[];
  tanks: Tank[];
  deliveringTanks: TankReceipt[];
  dev: "dev" | "mumbai" | "mainnet";
}

export enum Functions {
  Terrain,
  ClaimedResources,
  Name,
  TankBalance,
  Ammo1Balance,
  Ammo2Balance,
  ScrapBalance,
}

export enum GameStateEnum {
  OFF,
  CREATED,
  GETTING_INITIAL_VRF,
  HOME,
  AWAY,
  GETTING_HOME_VRF,
  GETTING_AWAY_VRF,
  GAME_OVER,
  ABANDONED,
}

export enum PlayerEnum {
  Home = 3,
  Away,
  Spectator,
}

export const EthereumContext = createContext({} as IEthereumContextValues);

export const useEthereum = () => {
  return useContext(EthereumContext);
};

export const EthereumProvider = ({
  children,
  dev = "dev",
}: {
  children: React.ReactNode;
  dev: "dev" | "mumbai" | "mainnet";
}): JSX.Element => {
  const { ethereum } = window;
  const { data: signer } = useSigner();
  const { address } = useAccount();

  const chainId: Chains =
    dev === "dev" ? 1337 : dev === "mainnet" ? 137 : 80001;

  const provider = useProvider({ chainId });

  const { tanks, fetchTanks, fetchDelivering, deliveringTanks } = useGetTanks();
  const { joinableGames, activeGames, fetchJoinableGames, fetchActiveGames } =
    useGetGames();
  const { playerData, fetchPlayerData } = useGetPlayerData();

  // console.log(chainId);

  const contractDataRead = useContractReads({
    contracts: [
      {
        ...contracts(dev, chainId).terrain,
        functionName: "getTerrain",
        args: [],
      },
      {
        ...contracts(dev, chainId).stockpile,
        functionName: "claimedAddress",
        args: [address],
      },
      {
        ...contracts(dev, chainId).soldierName,
        functionName: "soldierNames",
        args: [address],
      },
      {
        ...contracts(dev, chainId).tanks,
        functionName: "balanceOf",
        args: [address],
      },
      {
        ...contracts(dev, chainId).ammo,
        functionName: "balanceOf",
        args: [address, 1],
      },
      {
        ...contracts(dev, chainId).ammo,
        functionName: "balanceOf",
        args: [address, 2],
      },
      {
        ...contracts(dev, chainId).scrap,
        functionName: "balanceOf",
        args: [address],
      },
    ],
    watch: true,
  });

  const approveRead = useContractReads({
    contracts: [
      {
        ...contracts(dev, chainId).scrap,
        functionName: "allowance",
        args: [address, addresses(dev).tankFactory],
      },
      {
        ...contracts(dev, chainId).scrap,
        functionName: "allowance",
        args: [address, addresses(dev).ammoFactory],
      },
      {
        ...contracts(dev, chainId).scrap,
        functionName: "allowance",
        args: [address, addresses(dev).mechanicShop],
      },
      {
        ...contracts(dev, chainId).scrap,
        functionName: "allowance",
        args: [address, addresses(dev).soldierName],
      },
      {
        ...contracts(dev, chainId).tanks,
        functionName: "isApprovedForAll",
        args: [address, addresses(dev).battle],
      },
      {
        ...contracts(dev, chainId).ammo,
        functionName: "isApprovedForAll",
        args: [address, addresses(dev).battle],
      },
    ],
    watch: true,
  });

  // console.log({ contractDataRead, approveRead });
  const contractData = contractDataRead.data;
  const approvedList = approveRead.data;

  const contractSigner = useMemo(() => {
    if (signer) {
      return {
        stockpile: new Contract(
          addresses(dev).stockpile,
          contracts(dev, chainId).stockpile.abi,
          signer
        ),
        tanks: new Contract(
          addresses(dev).tanks,
          contracts(dev, chainId).tanks.abi,
          signer
        ),
        scrap: new Contract(
          addresses(dev).scrap,
          contracts(dev, chainId).scrap.abi,
          signer
        ),
        ammo: new Contract(
          addresses(dev).ammo,
          contracts(dev, chainId).ammo.abi,
          signer
        ),
        battle: new Contract(
          addresses(dev).battle,
          contracts(dev, chainId).battle.abi,
          signer
        ),
        tankFactory: new Contract(
          addresses(dev).tankFactory,
          contracts(dev, chainId).tankFactory.abi,
          signer
        ),
      };
    }
  }, [signer]);

  const battleReader = useContract({
    ...contracts(dev, chainId).battle,
    signerOrProvider: provider,
  });

  // console.log({ battleReader });

  useEffect(() => {
    const handleAccountsChanged = (accounts: any) => {
      if (accounts) {
        console.log("changed acccount", accounts[0]);
        // toast(`Changed Account: \n ${addressConcat(accounts[0])}`, {
        //   style: { color: "#7F3B2F" },
        // });
      }
    };
    if (ethereum) {
      ethereum.on("accountsChanged", handleAccountsChanged);
    }
    return () => {
      ethereum.removeListener("accountsChanged", handleAccountsChanged);
    };
  }, []);

  useEffect(() => {
    if (!address) return;
    console.log("fetching tanks");
    fetchTanks(address, true);
  }, [(contractData?.[Functions.TankBalance] as any)?.toNumber(), address]);

  useEffect(() => {
    if (!address) return;
    fetchJoinableGames(address);
    fetchActiveGames(address);
    fetchPlayerData(address);
  }, [address]);

  useEffect(() => {
    if (!address) return;
    const interval = setInterval(() => {
      console.log("fetching tanks, fetching joinable and active games");
      fetchTanks(address);
      fetchJoinableGames(address);
      fetchActiveGames(address);
      fetchPlayerData(address);
      fetchDelivering(address);
    }, 2000);

    return () => clearInterval(interval);
  }, [contractData]);

  console.log({ contractData, approvedList, deliveringTanks });

  return (
    <EthereumContext.Provider
      value={{
        dev,
        deliveringTanks,
        tanks,
        joinableGames,
        activeGames,
        approvedList,
        contractData: contractData,
        contractSigner,
        // rescueSigner,
        battleReader,
        chainId,
      }}
    >
      {children}
    </EthereumContext.Provider>
  );
};

export function addressConcat(addr: string, n = 8) {
  const l = addr.length;
  return addr.substring(0, n) + "..." + addr.substring(l - n, l);
}
