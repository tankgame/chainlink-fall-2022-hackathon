import { useNavigate } from "react-router-dom";

export const NotFound = () => {
  const navigate = useNavigate();
  return (
    <div className="h-full w-full bg-notFound flex flex-col justify-center items-center gap-8">
      <p>404</p>
      <h2 className="font-Military text-4xl">
        You Shouldn&apos;t Be Here Soldier
      </h2>
      <p className="font-Military text-xl">This route does not exist.</p>
      <button
        className="text-lg bg-white text-black p-3 rounded"
        onClick={() => navigate("/")}
      >
        Return to Camp
      </button>
    </div>
  );
};
