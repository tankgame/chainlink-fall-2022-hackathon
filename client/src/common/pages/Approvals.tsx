import { useEffect, useState } from "react";
import { biggestNumber } from "../../constants/constants";
import { useEthereum } from "../context/ethereum";
import { FaSpinner } from "react-icons/fa";
import { addresses } from "../../constants/contracts";

export const Approvals = () => {
  const { approvedList, contractSigner, dev } = useEthereum();
  const [approving, setApproving] = useState([
    false,
    false,
    false,
    false,
    false,
    false,
  ]);

  const scrap = contractSigner?.scrap;
  const tanks = contractSigner?.tanks;
  const ammo = contractSigner?.ammo;

  const scrapTank = approvedList[0]?.eq(biggestNumber);
  const scrapAmmo = approvedList[1]?.eq(biggestNumber);
  const scrapMech = approvedList[2]?.eq(biggestNumber);
  const scrapSoldier = approvedList[3]?.eq(biggestNumber);
  const tanksBattle = approvedList[4];
  const ammoBattle = approvedList[5];

  const checkedApproval = [
    scrapTank,
    scrapAmmo,
    scrapMech,
    scrapSoldier,
    tanksBattle,
    ammoBattle,
  ];

  console.log({ checkedApproval });

  const toggleApprove = (index: number, toggle: boolean) => {
    setApproving((prev) => {
      const newArray = [...prev];
      newArray[index] = toggle;
      return newArray;
    });
  };

  const handleApprove = async (index: number) => {
    try {
      toggleApprove(index, true);
      if (index == 0) {
        if (!scrap) return;
        await scrap.approve(addresses(dev).tankFactory, biggestNumber);
      }
      if (index == 1) {
        if (!scrap) return;
        await scrap.approve(addresses(dev).ammoFactory, biggestNumber);
      }
      if (index == 2) {
        if (!scrap) return;
        await scrap.approve(addresses(dev).mechanicShop, biggestNumber);
      }
      if (index == 3) {
        if (!scrap) return;
        await scrap.approve(addresses(dev).soldierName, biggestNumber);
      }
      if (index == 4) {
        if (!tanks) return;
        await tanks.setApprovalForAll(addresses(dev).battle, true);
      }
      if (index == 5) {
        if (!ammo) return;
        await ammo.setApprovalForAll(addresses(dev).battle, true);
      }
    } catch (e) {
      console.log({ e });
      toggleApprove(index, false);
    }
  };

  useEffect(() => {
    checkedApproval.forEach(
      (approval, index) => approval && toggleApprove(index, false)
    );
  }, [...checkedApproval]);

  console.log({ approving });
  return (
    <div className="h-full flex justify-center items-center text-center">
      <div className="bg-rust p-10 rounded-xl flex flex-col gap-5">
        <h2 className="text-2xl">Approvals</h2>
        <div className="flex flex-col text-black items-center">
          <h3 className="text-2xl font-bold">Scrap</h3>
          <button
            className={`text-xl relative w-fit px-1 ${
              scrapTank ? " line-through text-gray" : ""
            }`}
            onClick={() => handleApprove(0)}
            disabled={scrapTank || approving[0]}
          >
            Tank Factory
            {approving[0] && (
              <FaSpinner className="icon-spin absolute right-full top-0 bottom-0 my-auto" />
            )}
          </button>
          <button
            className={`text-xl relative w-fit px-1 ${
              scrapAmmo ? " line-through text-gray" : ""
            }`}
            onClick={() => handleApprove(1)}
            disabled={scrapAmmo || approving[1]}
          >
            Ammo Factory
            {approving[1] && (
              <FaSpinner className="icon-spin absolute right-full top-0 bottom-0 my-auto" />
            )}
          </button>
          <button
            className={`text-xl relative w-fit px-1 ${
              scrapMech ? " line-through text-gray" : ""
            }`}
            onClick={() => handleApprove(2)}
            disabled={scrapMech || approving[2]}
          >
            Mechanical Shop
            {approving[2] && (
              <FaSpinner className="icon-spin absolute right-full top-0 bottom-0 my-auto" />
            )}
          </button>
          <button
            className={`text-xl relative w-fit px-1 ${
              scrapSoldier ? " line-through text-gray" : ""
            }`}
            onClick={() => handleApprove(3)}
            disabled={scrapSoldier || approving[3]}
          >
            Soldier Names
            {approving[3] && (
              <FaSpinner className="icon-spin absolute right-full top-0 bottom-0 my-auto" />
            )}
          </button>
        </div>
        <div className="flex flex-col text-black items-center">
          <h3 className="text-2xl font-bold">Tank</h3>
          <button
            className={`text-xl relative w-fit px-1 ${
              tanksBattle ? " line-through text-gray" : ""
            }`}
            onClick={() => handleApprove(4)}
            disabled={tanksBattle || approving[4]}
          >
            Battle
            {approving[4] && (
              <FaSpinner className="icon-spin absolute right-full top-0 bottom-0 my-auto" />
            )}
          </button>
        </div>
        <div className="flex flex-col items-center text-black">
          <h3 className="text-2xl font-bold">Ammo</h3>
          <button
            className={`text-xl relative w-fit px-1 ${
              ammoBattle ? " line-through text-gray" : ""
            }`}
            disabled={ammoBattle || approving[5]}
            onClick={() => handleApprove(5)}
          >
            Battle
            {approving[5] && (
              <FaSpinner className="icon-spin absolute right-full top-0 bottom-0 my-auto" />
            )}
          </button>
        </div>
      </div>
    </div>
  );
};
