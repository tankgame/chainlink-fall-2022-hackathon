import { useNavigate } from "react-router-dom";
import { useEthereum } from "../context/ethereum";

export const Continue = () => {
  const { activeGames, approvedList } = useEthereum();

  const approved = approvedList?.[5];

  // console.log({ activeGames });
  const navigate = useNavigate();

  if (!approved)
    return (
      <div className="h-full flex flex-col items-center justify-center max-w-7xl text-black mx-auto gap-3 font-Military">
        <div
          className="text-3xl bg-rust p-3 rounded-xl cursor-pointer"
          onClick={() => navigate("/approvals")}
        >
          You need to Approve for Battle, Soldier
        </div>
      </div>
    );

  return (
    <div className="h-full flex flex-col items-center max-w-7xl text-black mx-auto gap-2">
      <h2 className="font-Military text-5xl my-5">Continue Game</h2>
      {activeGames.map((game) => {
        return (
          <div
            className="font-bold h-24 w-[600px] bg-rust  flex justify-center items-center text-3xl rounded-xl hover:shadow-2xl hover:-translate-y-2 hover:border border-black cursor-pointer"
            onClick={() => navigate(`/battle/${game.id}`)}
            key={game.id}
          >
            Continue Game: {game.id}
          </div>
        );
      })}
    </div>
  );
};
