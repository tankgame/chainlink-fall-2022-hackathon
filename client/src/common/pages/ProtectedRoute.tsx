import { Navigate, Outlet } from "react-router-dom";
import { useAccount, useNetwork } from "wagmi";
import { Navbar } from "../components/Navbar";
import { useEthereum } from "../context/ethereum";

export const ProtectedRoute = () => {
  const { address } = useAccount();
  const { chain } = useNetwork();
  const { chainId } = useEthereum();

  const inChain = chain?.id === chainId;
  // console.log(address, chainId, chain?.id);
  if (!address || !inChain) return <Navigate to="/" />;

  return (
    <div className="flex w-full h-full">
      <Navbar />
      <div className="flex-1 text-white overflow-auto">
        <Outlet />
      </div>
    </div>
  );
};
