import { useEffect, useState } from "react";
import { FaSpinner } from "react-icons/fa";
import { Functions, useEthereum } from "../context/ethereum";

export const Stockpile = () => {
  const { contractSigner, contractData } = useEthereum();
  const [minting, setMinting] = useState(false);

  const claimed = contractData?.[Functions.ClaimedResources];

  const stockpileSigner = contractSigner?.stockpile;

  const claimTokens = async () => {
    try {
      setMinting(true);
      if (!stockpileSigner) return;
      const txt = await stockpileSigner.stockpileClaim();

      await txt.wait();
      setMinting(false);
    } catch (e) {
      setMinting(false);
      console.log({ e });
    }
  };

  useEffect(() => {
    setMinting(false);
  }, [claimed]);

  return (
    <div className="flex flex-col h-full justify-center items-center font-Military text-black">
      {!claimed ? (
        <div className="flex gap-2 flex-col p-5 text-center bg-rust rounded-lg text-2xl">
          <h2>Claim Tank, Ammo, and Scrap!</h2>
          <button
            className="bg-white self-center px-5 py-2 rounded font-bold text-xl"
            onClick={claimTokens}
          >
            {minting ? <FaSpinner className="icon-spin" /> : "Claim"}
          </button>
        </div>
      ) : (
        <div className="flex gap-2 flex-col p-5 text-center bg-rust text-black rounded-lg text-2xl">
          One Claim Per Soldier, Soldier!
        </div>
      )}
    </div>
  );
};
