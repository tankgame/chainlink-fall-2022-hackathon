import React from "react";
import { addresses } from "../../constants/contracts";
import { useEthereum } from "../context/ethereum";

export const Contracts = () => {
  const { dev } = useEthereum();
  return (
    <div className="h-full flex justify-center items-center">
      <div className="bg-rust p-5 rounded-xl flex flex-col items-center gap-1">
        <h2 className="text-2xl">Contracts</h2>
        <a
          href={`https://mumbai.polygonscan.com/address/${addresses(dev).ammo}`}
          target={"_blank"}
          rel="noreferrer"
          className="text-xl"
        >
          Ammo
        </a>
        <a
          href={`https://mumbai.polygonscan.com/address/${
            addresses(dev).ammoFactory
          }`}
          target={"_blank"}
          rel="noreferrer"
          className="text-xl"
        >
          Ammo Factory
        </a>
        <a
          href={`https://mumbai.polygonscan.com/address/${
            addresses(dev).battle
          }`}
          target={"_blank"}
          rel="noreferrer"
          className="text-xl"
        >
          Battle
        </a>
        <a
          href={`https://mumbai.polygonscan.com/address/${
            addresses(dev).mechanicShop
          }`}
          target={"_blank"}
          rel="noreferrer"
          className="text-xl"
        >
          Mechanic Shop
        </a>
        <a
          href={`https://mumbai.polygonscan.com/address/${
            addresses(dev).medals
          }`}
          target={"_blank"}
          rel="noreferrer"
          className="text-xl"
        >
          Medals
        </a>
        <a
          href={`https://mumbai.polygonscan.com/address/${
            addresses(dev).metadata
          }`}
          target={"_blank"}
          rel="noreferrer"
          className="text-xl"
        >
          Metadata
        </a>
        <a
          href={`https://mumbai.polygonscan.com/address/${
            addresses(dev).scrap
          }`}
          target={"_blank"}
          rel="noreferrer"
          className="text-xl"
        >
          Scrap
        </a>
        <a
          href={`https://mumbai.polygonscan.com/address/${
            addresses(dev).soldierName
          }`}
          target={"_blank"}
          rel="noreferrer"
          className="text-xl"
        >
          Soldier Name
        </a>
        <a
          href={`https://mumbai.polygonscan.com/address/${
            addresses(dev).stockpile
          }`}
          target={"_blank"}
          rel="noreferrer"
          className="text-xl"
        >
          Stockpile
        </a>
        <a
          href={`https://mumbai.polygonscan.com/address/${
            addresses(dev).tanks
          }`}
          target={"_blank"}
          rel="noreferrer"
          className="text-xl"
        >
          Tanks
        </a>
        <a
          href={`https://mumbai.polygonscan.com/address/${
            addresses(dev).tankFactory
          }`}
          target={"_blank"}
          rel="noreferrer"
          className="text-xl"
        >
          Tank Factory
        </a>
        <a
          href={`https://mumbai.polygonscan.com/address/${
            addresses(dev).terrain
          }`}
          target={"_blank"}
          rel="noreferrer"
          className="text-xl"
        >
          Terrain
        </a>
        <a
          href={`https://mumbai.polygonscan.com/address/${
            addresses(dev).trigonometry
          }`}
          target={"_blank"}
          rel="noreferrer"
          className="text-xl"
        >
          Trigonometry
        </a>
      </div>
    </div>
  );
};
