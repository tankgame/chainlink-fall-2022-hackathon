import React, { useEffect, useRef, useState } from "react";
import leftArrow from "../../assets/images/chevron-left.svg";
import rightArrow from "../../assets/images/chevron-right.svg";
import { TankCard } from "../components/TankCard";
import { AmmoCard } from "../components/AmmoCard";
import { Functions, useEthereum } from "../context/ethereum";

export const Inventory = () => {
  const divTankRef = useRef<HTMLDivElement>(null);
  const divAmmoRef = useRef<HTMLDivElement>(null);
  const [showTankArrows, setShowTankArrows] = useState(false);
  const [showAmmoArrows, setShowAmmoArrows] = useState(false);
  const { tanks, contractData } = useEthereum();

  //scroll width - offset width = maxScrollLeft

  const handleScroll = (
    ref: React.RefObject<HTMLDivElement>,
    direction: "left" | "right"
  ) => {
    if (!ref.current) return;

    // console.log(
    //   direction,
    //   ref.current.scrollWidth,
    //   ref.current.offsetWidth,
    //   ref.current.scrollLeft
    // );
    if (direction == "left") {
      const scroll = ref.current.scrollLeft;
      ref.current.scrollLeft -= 100;
      if (scroll === ref.current.scrollLeft) {
        ref.current.scrollLeft = ref.current.scrollWidth;
      }
    } else {
      const scroll = ref.current.scrollLeft;
      // console.log(scroll);
      ref.current.scrollLeft += 100;
      if (ref.current.scrollLeft == scroll) {
        ref.current.scrollLeft = 0;
      }
    }
  };

  const ammo1: BigNumber = contractData?.[Functions.Ammo1Balance];
  const ammo2: BigNumber = contractData?.[Functions.Ammo2Balance];

  const ammos: Ammo[] = [];
  if (ammo1?.gt(0)) {
    ammos.push({
      id: "1",
      type: "Golden Snake Fang",
      damage: 50,
      amount: ammo1.toNumber(),
      image:
        "https://atlascorp.mypinata.cloud/ipfs/QmUKnXpw1AHE3z3VLAF2siqZTEmq8qAbK2e6JsahLQUYb9",
    });
  }
  if (ammo2?.gt(0)) {
    ammos.push({
      id: "2",
      type: "Black Mirror Ball",
      damage: 75,
      amount: ammo2.toNumber(),
      image:
        "https://atlascorp.mypinata.cloud/ipfs/QmXaXThJB9g3HSEoCANk7poJSMN3YkkyiqQ9VaB3yNGRBW",
    });
  }

  useEffect(() => {
    const handleResize = () => {
      if (divTankRef.current) {
        setShowTankArrows(
          !(divTankRef.current.offsetWidth === divTankRef.current.scrollWidth)
        );
      }
      if (divAmmoRef.current) {
        setShowAmmoArrows(
          !(divAmmoRef.current.offsetWidth === divAmmoRef.current.scrollWidth)
        );
      }
    };
    window.addEventListener("resize", handleResize);
    handleResize();
    return () => window.removeEventListener("resize", handleResize);
    // divTankRef!.current!.scrollLeft = divTankRef.current!.scrollWidth;
  }, [divTankRef, divAmmoRef, tanks.length, ammos.length]);

  return (
    <div className="w-full h-full flex flex-col gap-5 items-center text-black max-w-7xl mx-auto px-1 py-10">
      <div className="w-full py-5 px-1 rounded-xl flex flex-col gap-2">
        <h2 className="text-5xl text-center font-Military">Tanks</h2>
        <div className="w-full flex">
          {showTankArrows && (
            <div
              className="bg-sand h-fit my-auto rounded-full p-1 hover:cursor-pointer"
              onClick={() => handleScroll(divTankRef, "left")}
            >
              <img src={leftArrow} className="inline" />
            </div>
          )}
          <div
            className={`flex flex-row flex-1 overflow-x-hidden gap-10 mx-2 ${
              showTankArrows ? "" : "justify-center"
            }`}
            ref={divTankRef}
          >
            {tanks.map((tank) => {
              return <TankCard {...tank} key={tank.id} />;
            })}
          </div>
          {showTankArrows && (
            <div
              className="bg-sand h-fit my-auto rounded-full p-1 hover:cursor-pointer"
              onClick={() => handleScroll(divTankRef, "right")}
            >
              <img src={rightArrow} className="inline" />
            </div>
          )}
        </div>
      </div>

      <div className="w-full py-5 px-1 rounded-xl flex flex-col gap-2">
        <h2 className="text-5xl text-center font-Military">Ammo</h2>
        <div className="w-full flex">
          {showAmmoArrows && (
            <div
              className="bg-sand h-fit my-auto rounded-full p-1 hover:cursor-pointer"
              onClick={() => handleScroll(divAmmoRef, "left")}
            >
              <img src={leftArrow} className="inline" />
            </div>
          )}
          <div
            className={`flex flex-row flex-1 overflow-x-hidden gap-10 mx-2 ${
              showAmmoArrows ? "" : "justify-center"
            }`}
            ref={divAmmoRef}
          >
            {ammos.map((ammo) => {
              return <AmmoCard {...ammo} key={ammo.id} />;
            })}
          </div>
          {showAmmoArrows && (
            <div
              className="bg-sand h-fit my-auto rounded-full p-1 hover:cursor-pointer"
              onClick={() => handleScroll(divAmmoRef, "right")}
            >
              <img src={rightArrow} className="inline" />
            </div>
          )}
        </div>
      </div>
    </div>
  );
};
