import { BigNumber } from "ethers";
import { useEffect, useState } from "react";
import { Area, ComposedChart, ReferenceDot, XAxis, YAxis } from "recharts";
import {
  addressConcat,
  Functions,
  GameStateEnum,
  PlayerEnum,
  useEthereum,
} from "../context/ethereum";
import { useGetPath } from "../hooks/getPath";
import { useParams } from "react-router-dom";
import { useAccount, useContractReads } from "wagmi";
import { contracts } from "../../constants/contracts";
import { FaSpinner } from "react-icons/fa";
import { useGetBattle } from "../hooks/getBattle";
import tank1Sprite from "../../assets/images/tank-1.png";
import tank3Sprite from "../../assets/images/tank-3.png";
import health from "../../assets/images/health.svg";
import gas from "../../assets/images/gas.svg";
import leftArrow from "../../assets/images/leftArrow.svg";
import rightArrow from "../../assets/images/rightArrow.svg";
import explosion1 from "../../assets/images/explosion1.png";

export const FiringRange = () => {
  const { contractData, contractSigner, dev, chainId } = useEthereum();

  const [ammoSelected, setammoSelected] = useState(0);
  const [fireLoading, setFireLoading] = useState(false);
  const [movementLoading, setMovementLoading] = useState(false);
  const [ammoError, setAmmoError] = useState(false);
  const [movementInput, setMovementInput] = useState(0);

  const { gameId } = useParams();
  const { address } = useAccount();

  // console.log({ gameId });
  const battleSigner = contractSigner?.battle;

  const [form, setForm] = useState({
    power: 0,
    angle: 0,
  });

  const { battleStats, fetchBattleStats } = useGetBattle();

  const getBattleData = useContractReads({
    contracts: [
      {
        ...contracts(dev, chainId).battle,
        functionName: "gameData",
        args: [gameId],
      },
      {
        ...contracts(dev, chainId).battle,
        functionName: "numSpacesPlayersCanMove",
        args: [gameId],
      },
    ],
    watch: true,
  }) as any;

  const battleData: BattleData | undefined = getBattleData?.data?.[0];
  const movement: PlayerMovement | undefined = getBattleData?.data?.[1];

  console.log({ battleData, movement });

  const graphData: any[] | undefined = [];

  contractData?.[Functions.Terrain]?.forEach(
    (terrain: BigNumber, index: number) => {
      graphData[index] = {
        height: terrain.div("1000000").toNumber(),
        x: index,
      };
    }
  );

  const ammo1: BigNumber = contractData?.[Functions.Ammo1Balance];
  const ammo2: BigNumber = contractData?.[Functions.Ammo2Balance];

  const ammoTypes = [];
  if (ammo1?.gt(0)) {
    ammoTypes.push(1);
  }
  if (ammo2?.gt(0)) {
    ammoTypes.push(2);
  }

  const homeX = battleData?.home?.location.toNumber();
  const awayX = battleData?.away?.location.toNumber();

  const turn = battleData?.gameState;

  const player =
    address === battleData?.home?.id
      ? PlayerEnum.Home
      : address === battleData?.away?.id
      ? PlayerEnum.Away
      : PlayerEnum.Spectator;

  const isUserTurn =
    turn === GameStateEnum.HOME
      ? battleData?.home.id === address
      : turn === GameStateEnum.AWAY
      ? battleData?.away.id === address
      : false;

  const fireX = player === PlayerEnum.Home ? homeX : awayX;
  const targetX = player === PlayerEnum.Home ? awayX : homeX;

  const playerCanMove = !isUserTurn
    ? {
        left: 0,
        right: 0,
      }
    : player === PlayerEnum.Home
    ? {
        left: movement?.homeLeft.toNumber() || 0,
        right: movement?.homeRight.toNumber() || 0,
      }
    : {
        left: movement?.awayLeft.toNumber() || 0,
        right: movement?.awayRight.toNumber() || 0,
      };

  const leftPlayer =
    player === PlayerEnum.Away
      ? battleStats.awayPlayerTank
      : battleStats.homePlayerTank;
  const rightPlayer =
    player === PlayerEnum.Away
      ? battleStats.homePlayerTank
      : battleStats.awayPlayerTank;

  const leftPlayerHealth = leftPlayer
    ? (Number(leftPlayer.health) / Number(leftPlayer.type.maxHealth)) * 100
    : 0;

  const rightPlayerHealth = rightPlayer
    ? (Number(rightPlayer.health) / Number(rightPlayer.type.maxHealth)) * 100
    : 0;

  const leftPlayerGas = leftPlayer
    ? (Number(leftPlayer.gas) / Number(leftPlayer.type.maxGas)) * 100
    : 0;

  const rightPlayerGas = rightPlayer
    ? (Number(rightPlayer.gas) / Number(rightPlayer.type.maxGas)) * 100
    : 0;

  const { path, detectCollision, pathMinus, pathPlus, loading } = useGetPath(
    form.angle,
    form.power,
    player,
    homeX,
    awayX,
    graphData
  );

  console.log({ detectCollision, path });

  // // console.log({ graphData, homeX, isUserTurn });

  const handleFire = async () => {
    if (!battleSigner) return;
    console.log("fire in the hole");
    if (!ammoSelected) return setAmmoError(true);
    console.log(((form.power * 1000000) / 100).toString());
    setFireLoading(true);
    try {
      battleSigner.fire(
        gameId,
        ammoSelected,
        Math.floor((form.angle * 16384) / 360),
        ((form.power * 1000000) / 100).toString()
      );
    } catch (e) {
      setFireLoading(false);
      console.log({ e });
    }
  };

  const handleMove = async () => {
    if (!battleSigner) return;
    if (movementInput === 0) return;

    setMovementLoading(true);
    try {
      await battleSigner.moveTank(gameId, movementInput);
    } catch (e) {
      setMovementLoading(false);
      console.log({ e });
    }
  };

  useEffect(() => {
    setForm({ power: 0, angle: 0 });
  }, [address]);

  useEffect(() => {
    console.log("remove fire loaindg");
    setFireLoading(false);
    setMovementLoading(false);
    setMovementInput(0);
  }, [battleData?.gameState]);

  useEffect(() => {
    if (!gameId) return;
    fetchBattleStats(gameId);
  }, [battleData]);

  useEffect(() => {
    if (!gameId) return;
    const interval = setInterval(() => {
      fetchBattleStats(gameId);
      console.log("yeeah yeet");
    }, 2000);

    return () => clearInterval(interval);
  }, []);

  // console.log({ pathPlus, path, pathMinus });
  const bg = Number(gameId) || 0;
  const bgImage =
    bg % 3 === 2 ? "bg-snow" : bg % 3 === 1 ? "bg-pyramid" : "bg-forest";
  const bgFill =
    bg % 3 === 2 ? "#DBD9D9" : bg % 3 === 1 ? "#BCB6A5" : "#898C86";

  console.log({ playerCanMove });
  return (
    <div className={`w-full h-full bg-black bg-hud bg-center bg-cover`}>
      <div className="w-full h-full max-w-7xl mx-auto flex flex-col justify-center items-center">
        {!!graphData.length && (
          <div className="flex flex-col items-center">
            <ComposedChart
              width={1024}
              height={600}
              data={graphData}
              className={`bg-center bg-cover ${bgImage}`}
              margin={{ top: 0, right: 0, bottom: 0, left: 0 }}
            >
              <XAxis domain={[0, 1024]} hide />
              <YAxis domain={[0, 600]} hide />
              {!!contractData?.[Functions.Terrain]?.length && (
                <Area dataKey={"height"} fill={bgFill} stroke="" />
              )}
              {fireX && (
                <ReferenceDot
                  x={fireX + movementInput}
                  y={graphData[fireX + movementInput].height}
                  fill="red"
                  shape={(props) => {
                    return (
                      <image
                        x={props.cx - 45 / 2}
                        y={props.cy - 45 / 2}
                        width="45"
                        height="45"
                        href={tank3Sprite}
                        style={{
                          transformOrigin: "center",
                          transformBox: "content-box",
                        }}
                      />
                    );
                  }}
                  stroke=""
                  r={10}
                />
              )}

              {targetX && (
                <>
                  <ReferenceDot
                    x={targetX}
                    y={graphData[targetX].height}
                    fill="red"
                    shape={(props) => {
                      return (
                        <image
                          x={props.cx - 45 / 2}
                          y={props.cy - 45 / 2}
                          width="45"
                          height="45"
                          href={tank1Sprite}
                          style={{
                            transformOrigin: "center",
                            transformBox: "content-box",
                          }}
                        />
                      );
                    }}
                    stroke=""
                    r={10}
                  />
                  {battleData?.gameState == 7 && (
                    <ReferenceDot
                      x={targetX}
                      y={graphData[targetX].height}
                      fill="red"
                      shape={(props) => {
                        return (
                          <image
                            x={props.cx - 45 / 2}
                            y={props.cy - 45 / 2}
                            width="45"
                            height="45"
                            href={explosion1}
                            style={{
                              transformOrigin: "center",
                              transformBox: "content-box",
                            }}
                          />
                        );
                      }}
                      stroke=""
                      r={10}
                    />
                  )}
                  {detectCollision && (
                    <ReferenceDot
                      x={targetX}
                      y={graphData[targetX].height}
                      fill="red"
                      shape={(props) => {
                        return (
                          <image
                            x={props.cx - 45 / 2}
                            y={props.cy - 45 / 2}
                            width="45"
                            height="45"
                            href={explosion1}
                            style={{
                              transformOrigin: "center",
                              transformBox: "content-box",
                            }}
                          />
                        );
                      }}
                      stroke=""
                      r={10}
                    />
                  )}
                </>
              )}

              {path.points.slice(0, path.len).map((point: Point, index) => {
                return (
                  <ReferenceDot
                    x={Math.floor(point.x)}
                    y={point.y}
                    fill="blue"
                    stroke=""
                    r={2}
                    key={index * 3}
                  />
                );
              })}
              {pathMinus.points
                .slice(0, pathMinus.len)
                .map((point: Point, index) => {
                  return (
                    <ReferenceDot
                      x={Math.floor(point.x)}
                      y={point.y}
                      fill="lightblue"
                      stroke=""
                      r={2}
                      key={index * 3 + 1}
                    />
                  );
                })}
              {pathPlus.points
                .slice(0, pathPlus.len)
                .map((point: Point, index) => {
                  return (
                    <ReferenceDot
                      x={Math.floor(point.x)}
                      y={point.y}
                      fill="lightblue"
                      stroke=""
                      r={2}
                      key={index * 3 + 2}
                    />
                  );
                })}
            </ComposedChart>
            <div
              className="flex flex-col justify-between items-center gap-5 bg-black bg-opacity-50 p-10 rounded-lg"
              style={{ width: 1024 }}
            >
              {(battleData?.gameState || 0) < 7 ? (
                <>
                  <div className="w-full h-full flex">
                    <div className="text-black w-1/3 flex flex-col gap-2">
                      <h2 className="text-white">
                        {addressConcat(
                          player === PlayerEnum.Away
                            ? battleStats.awayPlayer?.id || ""
                            : battleStats.homePlayer?.id || ""
                        )}
                      </h2>
                      <div className="w-full flex gap-1">
                        <img src={health} className={"inline h-5 w-5"} />
                        <div className="h-5 bg-gray flex-1 rounded-full relative overflow-hidden">
                          <div
                            className={`h-full relative overflow-hidden ${
                              leftPlayerHealth > 50
                                ? "bg-green-400"
                                : leftPlayerHealth > 25
                                ? "bg-yellow-400"
                                : "bg-fire"
                            }`}
                            style={{
                              width: `${leftPlayerHealth}%`,
                            }}
                          />
                        </div>
                      </div>
                      <div className="w-full flex gap-1">
                        <img src={gas} className={"inline h-5 w-5"} />
                        <div className="h-5 bg-gray flex-1 rounded-full relative overflow-hidden">
                          <div
                            className={`h-full relative overflow-hidden bg-rust`}
                            style={{
                              width: `${leftPlayerGas}%`,
                            }}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="flex w-1/3 px-3 justify-center items-center">
                      <div className="flex flex-col flex-1 text-black p-2 gap-2">
                        {isUserTurn && (
                          <select
                            className={`p-2 w-full rounded-lg text-black dark:text-white ${
                              ammoError ? "border-4 border-fire" : ""
                            }`}
                            value={ammoSelected}
                            onChange={(e) =>
                              setammoSelected(Number(e.target.value))
                            }
                            onClick={() => setAmmoError(false)}
                          >
                            <option value={"0"}>Choose Ammo</option>
                            {ammoTypes.map((type: number) => {
                              const value =
                                type === 1
                                  ? `Golden Snake Fang - ${ammo1.toString()}`
                                  : `Black Mirror Ball - ${ammo2.toString()}`;
                              return (
                                <option key={type} value={type}>
                                  {value}
                                </option>
                              );
                            })}
                          </select>
                        )}
                        <div className="flex gap-2 items-center">
                          <span className="text-white">angle</span>
                          <input
                            type="number"
                            className="p-1 rounded flex-1 dark:text-white"
                            min={0}
                            max={360}
                            placeholder="Angle"
                            value={form.angle}
                            onChange={(e) => {
                              const value = Number(e.target.value);
                              if (value > 360 || value < 0) return;
                              setForm((prev) => ({
                                ...prev,
                                angle: value,
                              }));
                            }}
                          />
                        </div>
                      </div>
                      <button
                        className={`rounded-full h-12 w-12 grid place-items-center text-black font-bold ${
                          loading || !isUserTurn || movementLoading
                            ? "bg-fog"
                            : "bg-fire"
                        }`}
                        onClick={handleFire}
                        disabled={
                          loading ||
                          !isUserTurn ||
                          fireLoading ||
                          movementLoading
                        }
                      >
                        {loading || fireLoading ? (
                          <FaSpinner className="icon-spin" />
                        ) : (
                          "Fire"
                        )}
                      </button>
                    </div>
                    <div className="text-black w-1/3 flex flex-col gap-2">
                      <h2 className="text-white text-right">
                        {addressConcat(
                          player === PlayerEnum.Away
                            ? battleStats.homePlayer?.id || ""
                            : battleStats.awayPlayer?.id || ""
                        )}
                      </h2>
                      <div className="w-full flex gap-1">
                        <div className="h-5 bg-gray flex-1 rounded-full relative overflow-hidden">
                          <div
                            className={`h-full relative overflow-hidden ${
                              rightPlayerHealth > 50
                                ? "bg-green-400"
                                : rightPlayerHealth > 25
                                ? "bg-yellow-400"
                                : "bg-fire"
                            }`}
                            style={{
                              width: `${rightPlayerHealth}%`,
                            }}
                          />
                        </div>
                        <img src={health} className={"inline h-5 w-5"} />
                      </div>
                      <div className="w-full flex gap-1">
                        <div className="h-5 bg-gray flex-1 rounded-full relative overflow-hidden">
                          <div
                            className={`h-full relative overflow-hidden bg-rust`}
                            style={{
                              width: `${rightPlayerGas}%`,
                            }}
                          />
                        </div>
                        <img src={gas} className={"inline h-5 w-5"} />
                      </div>
                    </div>
                  </div>
                  <div className="flex gap-2 w-1/2">
                    <span className="text-white">power</span>
                    <input
                      type="range"
                      className="p-1 rounded flex-1"
                      min={0}
                      max={100}
                      step={0.1}
                      value={form.power}
                      onChange={(e) =>
                        setForm((prev) => ({
                          ...prev,
                          power: Number(e.target.value),
                        }))
                      }
                    />
                  </div>
                  {isUserTurn && (
                    <div className="flex flex-col gap-4 items-center">
                      <div className="flex gap-4 w-full h-12 items-center justify-center">
                        <button
                          onClick={() => {
                            if (movementInput > -playerCanMove?.left)
                              setMovementInput(movementInput - 1);
                          }}
                        >
                          <img src={leftArrow} className="inline h-12" />
                        </button>
                        <div className="bg-white h-full w-12 text-black rounded flex justify-center items-center text-2xl font-bold">
                          {movementInput}
                        </div>
                        <button
                          onClick={() => {
                            if (movementInput < playerCanMove.right)
                              setMovementInput(movementInput + 1);
                          }}
                        >
                          <img src={rightArrow} className="inline h-12" />
                        </button>
                      </div>
                      <button
                        className="bg-white rounded-xl text-black h-8 w-28 font-bold"
                        disabled={fireLoading || movementLoading}
                        onClick={handleMove}
                      >
                        Move Tank
                      </button>
                    </div>
                  )}
                </>
              ) : (
                <div className="font-bold text-xl">
                  <h2>Winner {battleStats.winner.id}</h2>
                </div>
              )}
            </div>
          </div>
        )}
      </div>
    </div>
  );
};
