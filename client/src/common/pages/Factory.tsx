import { useEffect, useState } from "react";
import { Delivering } from "../components/Delivering";
import { OrderTank } from "../components/OrderTank";

type FactoryTab = "Order" | "Delivery";

function classNames(...classes: string[]) {
  return classes.filter(Boolean).join(" ");
}

const tanksExample: OrderTankCard[] = [
  {
    id: "1",
    name: "Tank 1",
    description: "Description 1",
    maxHealth: 100,
    maxGas: 100,
    accuracy: 98,
    cost: 50,
  },
  {
    id: "2",
    name: "Tank 2",
    description: "Description 2",
    maxHealth: 150,
    maxGas: 100,
    accuracy: 95,
    cost: 50,
  },
  {
    id: "3",
    name: "Tank 3",
    description: "Description 3",
    maxHealth: 75,
    maxGas: 100,
    accuracy: 99,
    cost: 50,
  },
];

const deliveringExample: DeliveringTanks[] = [
  // {
  //   id: 0,
  //   requested: 1667453965000,
  //   expectedTimeOfDelivery: 1667540365000,
  //   leadTime: 86400000,
  // },
  // {
  //   id: 1,
  //   requested: 1667453965000,
  //   expectedTimeOfDelivery: 1667540365000,
  //   leadTime: 86400000,
  // },
];

export const Factory = () => {
  const [tab, setTab] = useState<FactoryTab>("Order");
  const [tanks, setTanks] = useState<OrderTankCard[]>([]);
  const [delivering, setDelivering] = useState<DeliveringTanks[]>([]);

  const tabs: FactoryTab[] = ["Order", "Delivery"];

  useEffect(() => {
    setTanks(tanksExample);
    setDelivering(deliveringExample);
  }, []);

  return (
    <div className="w-full bg-black h-full flex">
      <div className="flex-1 lg:hidden bg-factory-bg bg-cover bg-center"></div>
      <div className="h-full bg-white text-black max-w-xl w-full lg:max-w-none px-10 flex flex-col">
        <h2 className="text-3xl font-Military p-10">Tank Factory</h2>
        <div className="border-b border-fog sm:pb-0">
          <nav className="-mb-px flex space-x-8">
            {tabs.map((tabPage) => (
              <a
                key={tabPage}
                onClick={() => setTab(tabPage)}
                className={classNames(
                  tabPage === tab
                    ? "border-rust text-rust"
                    : "border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300 cursor-pointer",
                  "whitespace-nowrap pb-4 px-1 border-b-2 font-medium text-xl"
                )}
                aria-current={tabPage ? "page" : undefined}
              >
                {tabPage}
              </a>
            ))}
          </nav>
        </div>
        <div className="flex-1 py-10">
          {tab === "Order" ? (
            <OrderTank tankCards={tanks} />
          ) : (
            <>
              <Delivering delivering={delivering} />
            </>
          )}
        </div>
      </div>
    </div>
  );
};
