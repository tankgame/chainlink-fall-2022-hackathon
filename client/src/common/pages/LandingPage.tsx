import { useEffect, useState } from "react";
import { useKeyBoard } from "../hooks/keypress";
import chevronRight from "../../assets/images/chevron-right.svg";
import { useNavigate } from "react-router-dom";
import { useAccount, useNetwork } from "wagmi";
import { useChainModal, useConnectModal } from "@rainbow-me/rainbowkit";
import { useEthereum } from "../context/ethereum";
import { pages } from "../../constants/constants";

export const LandingPage = () => {
  const { openConnectModal } = useConnectModal();
  const { openChainModal } = useChainModal();

  const { address } = useAccount();
  const { chain } = useNetwork();
  const { chainId } = useEthereum();
  const [elemnentSelected, setElemnentSelected] = useState(0);

  const keys = useKeyBoard();

  const inChain = chain?.id === chainId;

  const navigate = useNavigate();

  useEffect(() => {
    if (pages.length && keys.ArrowUp) {
      setElemnentSelected((prevState) =>
        prevState > 0 ? prevState - 1 : pages.length - 1
      );
    }
  }, [keys.ArrowUp]);

  useEffect(() => {
    if (pages.length && keys.ArrowDown) {
      setElemnentSelected((prevState) =>
        prevState < pages.length - 1 ? prevState + 1 : 0
      );
    }
  }, [keys.ArrowDown]);

  useEffect(() => {
    if (pages.length && keys.Enter) {
      navigate(pages[elemnentSelected].link);
    }
  }, [keys.Enter]);

  return (
    <div className="w-full h-full flex justify-center items-center flex-col gap-10 max-w-md mx-auto p-5">
      <div className="bg-sand p-16 flex gap-5 flex-col rounded-lg items-center w-full">
        <h1 className="text-6xl text-center font-Military">
          Tank <br /> Game
        </h1>
        {address && inChain ? (
          <div>
            <ul>
              {pages.map((page, index) => {
                return (
                  <li
                    className={"text-black text-2xl relative"}
                    key={page.page}
                  >
                    {elemnentSelected === index && (
                      <img
                        src={chevronRight}
                        className="absolute right-full"
                      ></img>
                    )}
                    <span onClick={() => navigate(page.link)}>{page.page}</span>
                  </li>
                );
              })}
            </ul>
          </div>
        ) : !address ? (
          <>
            {openConnectModal && (
              <button
                type="button"
                className="inline-flex items-center rounded-md bg-rust px-6 py-3 text-base font-medium text-white shadow-sm hover:bg-rust-hover active:scale-95"
                onClick={openConnectModal}
              >
                Connect Wallet
              </button>
            )}
          </>
        ) : (
          <>
            {openChainModal && (
              <button
                type="button"
                className="inline-flex items-center rounded-md bg-rust px-6 py-3 text-base font-medium text-white shadow-sm hover:bg-rust-hover active:scale-95"
                onClick={openChainModal}
              >
                Switch Network
              </button>
            )}
          </>
        )}
      </div>
    </div>
  );
};
