import { useEffect, useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import leftArrow from "../../assets/images/chevron-left.svg";
import rightArrow from "../../assets/images/chevron-right.svg";
import { TankCard } from "../components/TankCard";
import { useEthereum } from "../context/ethereum";

export const NewGame = () => {
  const [showTankArrows, setShowTankArrows] = useState<boolean>(false);
  const [selectedTank, setSelectedTank] = useState<number>(-1);
  const divTankRef = useRef<HTMLDivElement>(null);
  const { tanks, contractSigner, approvedList, joinableGames } = useEthereum();

  const battleSigner = contractSigner?.battle;

  const approved = approvedList?.[4] && approvedList?.[5];

  // console.log({ approved });
  const navigate = useNavigate();

  useEffect(() => {
    const handleResize = () => {
      if (divTankRef.current) {
        setShowTankArrows(
          !(divTankRef.current.offsetWidth === divTankRef.current.scrollWidth)
        );
      }
    };
    window.addEventListener("resize", handleResize);
    handleResize();
    return () => window.removeEventListener("resize", handleResize);
  }, [divTankRef, tanks.length]);

  const handleScroll = (
    ref: React.RefObject<HTMLDivElement>,
    direction: "left" | "right"
  ) => {
    if (!ref.current) return;

    if (direction == "left") {
      const scroll = ref.current.scrollLeft;
      ref.current.scrollLeft -= 100;
      if (scroll === ref.current.scrollLeft) {
        ref.current.scrollLeft = ref.current.scrollWidth;
      }
    } else {
      const scroll = ref.current.scrollLeft;
      ref.current.scrollLeft += 100;
      if (ref.current.scrollLeft == scroll) {
        ref.current.scrollLeft = 0;
      }
    }
  };

  const handleStart = async () => {
    try {
      if (!battleSigner) return;
      const txn = await battleSigner.createGame(tanks[selectedTank].id);
      await txn.wait();
    } catch (e) {
      console.log({ e });
    }
  };

  const handleJoin = async (gameId: string) => {
    try {
      if (!battleSigner) return;
      console.log(gameId, tanks[selectedTank].id);
      const txn = await battleSigner.joinGame(gameId, tanks[selectedTank].id);
      await txn.wait();
      navigate(`/battle/${gameId}`);
    } catch (e) {
      console.log({ e });
    }
  };

  if (!approved)
    return (
      <div className="h-full flex flex-col items-center justify-center max-w-7xl text-black mx-auto gap-3 font-Military">
        <div
          className="text-3xl bg-rust p-3 rounded-xl cursor-pointer"
          onClick={() => navigate("/approvals")}
        >
          You need to Approve for Battle, Soldier
        </div>
      </div>
    );

  return (
    <div className="h-full flex flex-col items-center max-w-7xl text-black mx-auto gap-2">
      <div className="w-full px-2 py-5 flex flex-col items-center gap-2">
        <h2 className="font-Military text-5xl">Select Your Tank, Soldier</h2>
        <div className="w-full flex">
          {showTankArrows && (
            <div
              className="bg-sand h-fit my-auto rounded-full p-1 hover:cursor-pointer"
              onClick={() => handleScroll(divTankRef, "left")}
            >
              <img src={leftArrow} className="inline" />
            </div>
          )}
          <div
            className={`flex flex-row flex-1 overflow-x-hidden gap-10 mx-2 ${
              showTankArrows ? "" : "justify-center"
            }`}
            ref={divTankRef}
          >
            {tanks.map((tank, index) => {
              return (
                <button
                  key={tank.id}
                  className={`p-2 rounded ${
                    selectedTank === index ? "bg-rust" : "hover:bg-rust-hover"
                  }`}
                  disabled={selectedTank === index}
                  onClick={() => setSelectedTank(index)}
                >
                  <TankCard {...tank} />
                </button>
              );
            })}
          </div>
          {showTankArrows && (
            <div
              className="bg-sand h-fit my-auto rounded-full p-1 hover:cursor-pointer"
              onClick={() => handleScroll(divTankRef, "right")}
            >
              <img src={rightArrow} className="inline" />
            </div>
          )}
        </div>
      </div>
      {selectedTank >= 0 && (
        <>
          <div
            className="font-bold h-24 w-[600px] bg-rust  flex justify-center items-center text-3xl rounded-xl hover:shadow-2xl hover:-translate-y-2 hover:border border-black cursor-pointer"
            onClick={handleStart}
          >
            Create Game
          </div>
          {joinableGames.map((game) => (
            <div
              className="font-bold h-24 w-[600px] bg-rust  flex justify-center items-center text-3xl rounded-xl hover:shadow-2xl hover:-translate-y-2 hover:border border-black cursor-pointer"
              onClick={() => handleJoin(game.id)}
              key={game.id}
            >
              Game: {game.id}
            </div>
          ))}
        </>
      )}
    </div>
  );
};
