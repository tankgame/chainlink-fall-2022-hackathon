import chainlink from "../../assets/images/chainlink.svg";
import quicknode from "../../assets/images/quicknode.svg";
import polygon from "../../assets/images/polygon.svg";
import graph from "../../assets/images/graph.svg";
import { ReactComponent as Speaker } from "../../assets/images/speaker.svg";
import { ReactComponent as SpeakerX } from "../../assets/images/speakerX.svg";

import { useState } from "react";

export const Footer = ({
  showSound,
  audioRef,
}: {
  showSound: boolean;
  audioRef: React.RefObject<HTMLAudioElement>;
}) => {
  const [mute, setMute] = useState(false);
  const [volume, setVolume] = useState(0.5);

  const handleMute = () => {
    if (!audioRef.current) return;
    setMute(!mute);
    audioRef.current.muted = !mute;
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (!audioRef.current) return;

    const volumeValue = Number(e.target.value);
    setVolume(volumeValue);
    audioRef.current.volume = volumeValue;
  };

  return (
    <div className="bg-black py-2 px-5 flex flex-wrap justify-center lg:gap-5 relative">
      <div className="min-w-1/3 flex gap-2 items-center justify-center flex-wrap lg:w-full flex-1 lg:flex-auto">
        <div className="flex gap-10">
          <a href="https://chain.link/" target={"_blank"} rel="noreferrer">
            <img src={chainlink} className="inline h-10" />
          </a>
          <a
            href="https://polygon.technology/"
            target={"_blank"}
            rel="noreferrer"
          >
            <img src={polygon} className="inline h-10" />
          </a>
          <a
            href="https://www.quicknode.com/"
            target={"_blank"}
            rel="noreferrer"
          >
            <img src={quicknode} className="inline" />
          </a>
          <a
            href="https://thegraph.com/en//"
            target={"_blank"}
            rel="noreferrer"
          >
            <img src={graph} className="inline h-10" />
          </a>
        </div>
      </div>
      {showSound && (
        <div className="absolute lg:static right-0 top-0 bottom-0 flex items-center gap-2 p-3">
          {mute ? (
            <SpeakerX className="inline text-lg" onClick={handleMute} />
          ) : (
            <Speaker className="inline text-lg" onClick={handleMute} />
          )}
          <input
            type={"range"}
            min={0}
            max={1}
            step={0.1}
            value={volume}
            onChange={handleChange}
            disabled={mute}
          />
        </div>
      )}
    </div>
  );
};
