import { useNavigate } from "react-router-dom";
import { useAccount, useEnsAvatar } from "wagmi";
import { pages } from "../../constants/constants";
import * as blockies from "blockies-ts";
import { Functions, useEthereum } from "../context/ethereum";
import cog from "../../assets/images/cog-white.svg";

export const Navbar = () => {
  const navigate = useNavigate();
  const { address } = useAccount();
  const { contractData } = useEthereum();

  // const { data } = useEnsAvatar({ addressOrName: address });
  const data = useEnsAvatar({
    addressOrName: "0x3fb38cee8d0ba7dcf59403a8c397626dc9c7a13b",
    chainId: 1,
  });

  const name = contractData?.[Functions.Name] || "No Name";
  const loc = window.location.pathname;

  // console.log({ data });
  return (
    <div className="bg-darkGray flex flex-col px-3 items-center py-3">
      <div className="flex-1">
        <p className="my-5 text-rust text-xl font-Military">Tank Game</p>
        <ul className="flex flex-col gap-2">
          {pages.map((page) => {
            const onPage = page.link === loc;
            return (
              <li key={page.link}>
                <span
                  onClick={() => navigate(page.link)}
                  className={`font-bold ${
                    onPage ? "text-rust" : "cursor-pointer text-sand"
                  }`}
                >
                  {page.page}
                </span>
              </li>
            );
          })}
        </ul>
      </div>
      <div className="flex justify-center items-center gap-2">
        <img
          src={
            data.data ||
            blockies.create({ seed: address?.toLowerCase() }).toDataURL()
          }
          className="rounded-lg h-8 w-8"
        />
        <div className="">
          <p className="text-ellipsis overflow-hidden whitespace-nowrap w-[130px]">
            {name}
          </p>
          <p>
            <img src={cog} className={"h-8 inline"}></img>{" "}
            {contractData?.[Functions.ScrapBalance]?.toString()}
          </p>
        </div>
      </div>
    </div>
  );
};
