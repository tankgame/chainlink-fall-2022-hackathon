import React from "react";

export const Email = () => {
  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
  };

  return (
    <div className="bg-sand p-5 rounded-lg flex justify-center flex-col items-center gap-2 w-full">
      <h2 className="text-black font-Military text-xl text-center">
        Get notified when we&apos;re launching.
      </h2>
      <form className="flex gap-5 w-full" onSubmit={handleSubmit}>
        <input
          className="rounded flex-1 p-2 text-black focus-visible:outline-none"
          placeholder="Enter you email"
          type={"email"}
        />
        <button
          className="bg-white text-black p-2 rounded active:scale-95"
          type="submit"
        >
          Notify Me
        </button>
      </form>
    </div>
  );
};
