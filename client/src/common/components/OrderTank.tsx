import { useState } from "react";
import leftArrow from "../../assets/images/chevron-left.svg";
import rightArrow from "../../assets/images/chevron-right.svg";
import exampleOrderTankImg from "../../assets/images/exampleOrderTankImg.png";
import heart from "../../assets/images/heart.svg";
import cog from "../../assets/images/cog.svg";
import { useEthereum } from "../context/ethereum";
import { biggestNumber } from "../../constants/constants";
import { useNavigate } from "react-router-dom";

export const OrderTank = ({ tankCards }: { tankCards: OrderTankCard[] }) => {
  const [position, setPosition] = useState<number>(0);
  const [direction, setDirection] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);

  const { contractSigner, approvedList } = useEthereum();

  const tankFactorySigner = contractSigner?.tankFactory;

  const approved = approvedList?.[0]?.eq(biggestNumber);

  const navigate = useNavigate();

  if (tankCards.length === 0)
    return (
      <div className="w-full h-full grid place-items-center text-xl">
        No TAanks Available
      </div>
    );

  const handleClick = (left: boolean) => {
    setDirection(left);
    if (left) {
      setPosition((pos) => {
        return pos === 0 ? tankCards.length - 1 : pos - 1;
      });
    } else {
      setPosition((pos) => (pos == tankCards.length - 1 ? 0 : pos + 1));
    }
  };

  const handleCommission = async (tankType: string) => {
    if (!tankFactorySigner) return;
    setLoading(true);
    try {
      const rec = await tankFactorySigner.commissionTank(tankType);
      await rec.wait();

      setLoading(false);
    } catch (e) {
      setLoading(false);
      console.log({ e });
    }
  };

  if (!approved) {
    return (
      <div className="w-full h-full flex flex-col justify-center items-center">
        <div
          className="text-3xl bg-rust p-3 rounded-xl cursor-pointer font-Military text-center"
          onClick={() => navigate("/approvals")}
        >
          You need to Approve the Tank Factory, Soldier
        </div>
      </div>
    );
  }

  return (
    <div className="w-full h-full flex flex-col items-center">
      <div className="flex-1 flex items-center justify-center w-full max-w-md">
        {tankCards.length > 1 && (
          <div
            className="h-fit my-auto rounded-full p-1 hover:cursor-pointer"
            onClick={() => handleClick(true)}
          >
            <img src={leftArrow} className="inline w-10" />
          </div>
        )}
        <div
          key={position}
          className={`${
            !direction ? "animate-fadeInLeft" : "animate-fadeInRight"
          }`}
        >
          <OrderTankCard tank={tankCards[position]} />
        </div>
        {tankCards.length > 1 && (
          <div
            className="h-fit my-auto rounded-full p-1 hover:cursor-pointer"
            onClick={() => handleClick(false)}
          >
            <img src={rightArrow} className="inline w-10" />
          </div>
        )}
      </div>
      <button
        className="mt-10 border border-black rounded-md px-8 py-2 active:scale-95"
        onClick={() => handleCommission(tankCards[position].id)}
      >
        Commission
      </button>
    </div>
  );
};

export const OrderTankCard = ({ tank }: { tank: OrderTankCard }) => {
  return (
    <>
      <div className="flex flex-col justify-center h-fit rounded-xl overflow-hidden border border-black w-80">
        <div className="w-full">
          <img className="w-full aspect-square" src={exampleOrderTankImg} />
        </div>
        <div className="p-5 b">
          <div className="flex justify-between">
            <div className="flex gap-1 items-center">
              <img src={heart} className="inline" />
              <span className="font-bold">{tank.maxHealth}</span>
            </div>
            <div className="flex gap-1 items-center">
              <span className="font-bold">{tank.cost}</span>
              <img src={cog} className={"h-8 inline"}></img>
            </div>
            {/* <div className="flex gap-1 items-center">
              <span className="font-bold">{tank.damage}</span>
              <img src={target} className="inline" />
            </div> */}
          </div>
          <div className="text-center">{tank.description}</div>
        </div>
      </div>
    </>
  );
};
