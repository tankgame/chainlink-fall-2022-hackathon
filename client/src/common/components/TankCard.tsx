export const TankCard = ({ id, homeGames, awayGames, image }: Tank) => {
  const battles = homeGames?.length + awayGames?.length;

  return (
    <div className="shadow-card rounded bg-white p-3 min-w-[200px] flex flex-col gap-2">
      <div className="bg-black w-44 h-44">
        <img
          className="aspect-square w-full object-cover"
          loading="lazy"
          src={image}
        />
      </div>
      <p>Tank ID: {id}</p>
      <p>Number of Battles: {battles}</p>
    </div>
  );
};
