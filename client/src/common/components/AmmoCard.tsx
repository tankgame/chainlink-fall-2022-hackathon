export const AmmoCard = ({ id, type, damage, amount, image }: Ammo) => {
  return (
    <div className="shadow-card rounded bg-white p-3 min-w-[200px] flex flex-col gap-2">
      <div className="relative bg-black w-44 h-44">
        <img className="aspect-square w-full " loading="lazy" src={image} />
        {amount > 1 && (
          <div className="absolute bg-white top-2 left-2 px-2 py-1 rounded********">
            x{amount}
          </div>
        )}
      </div>
      <p className="font-bold">{type}</p>
      <p>Damage: {damage}</p>
    </div>
  );
};
