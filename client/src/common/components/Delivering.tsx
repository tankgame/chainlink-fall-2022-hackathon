import { useEthereum } from "../context/ethereum";
import { useCountdown } from "../hooks/countdown";

export const Delivering = ({
  delivering,
}: {
  delivering: DeliveringTanks[];
}) => {
  const { deliveringTanks } = useEthereum();

  // const enRoute = deliveringTanks.filter((tank) => {
  //   Date.now() < Number(tank.expectedTimeOfDelivery) * 1000;
  // });
  return (
    <div className="w-full h-full flex flex-col">
      {deliveringTanks.map((delivery: TankReceipt) => {
        const countDown = useCountdown(
          Number(delivery.expectedTimeOfDelivery) * 1000
        );
        const progress =
          ((Date.now() - Number(delivery.timeOfPurchase) * 1000) /
            (Number(delivery.expectedTimeOfDelivery) * 1000 -
              Number(delivery.timeOfPurchase) * 1000)) *
          100;

        console.log({ progress });
        return (
          <div key={delivery.id} className={"w-full"}>
            <h2>Commission ID: {delivery.id}</h2>
            <div className="h-10 w-full bg-fog rounded-full overflow-hidden">
              <div
                className={`h-full relative bg-rustTransparent overflow-hidden after:animate-wave after:inset-0 after:absolute after:bg-wave`}
                style={{ width: `${progress.toFixed(2)}%` }}
              />
            </div>
            <h2 className="text-right px-3">
              {countDown[0]}:{countDown[1]}:{countDown[2]}:{countDown[3]}
            </h2>
          </div>
        );
      })}
    </div>
  );
};
