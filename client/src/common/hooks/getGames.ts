import { useState } from "react";
import {
  activeAwayGames,
  activeHomeGames,
  joinableGamesQuery,
} from "../api/games";

export const useGetGames = () => {
  const [activeGames, setActiveGames] = useState<Game[]>([]);
  const [joinableGames, setJoinableGames] = useState<Game[]>([]);

  const fetchActiveGames = async (address: string) => {
    const resHome = await fetch(
      "https://api.thegraph.com/subgraphs/name/seidprojects/tank-game-battle",
      {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          query: activeHomeGames(address),
          variables: null,
        }),
      }
    ).then((res) => res.json());

    const homeGames = resHome?.data?.tankGames;

    const resAway = await fetch(
      "https://api.thegraph.com/subgraphs/name/seidprojects/tank-game-battle",
      {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          query: activeAwayGames(address),
          variables: null,
        }),
      }
    ).then((res) => res.json());

    const awayGames = resAway?.data?.tankGames;

    console.log({ homeGames }, { awayGames });
    setActiveGames([...homeGames, ...awayGames]);
  };

  const fetchJoinableGames = async (address: string) => {
    const res = await fetch(
      "https://api.thegraph.com/subgraphs/name/seidprojects/tank-game-battle",
      {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          query: joinableGamesQuery(address),
          variables: null,
        }),
      }
    ).then((res) => res.json());

    const games = res?.data?.tankGames;
    // console.log({ games });
    setJoinableGames(games);
  };

  return { activeGames, joinableGames, fetchJoinableGames, fetchActiveGames };
};
