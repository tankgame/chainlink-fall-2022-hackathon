import { BigNumber } from "ethers";
import { useEffect, useState } from "react";
import { PlayerEnum, useEthereum } from "../context/ethereum";

export const useGetPath = (
  angle: number,
  power: number,
  player?: PlayerEnum,
  homeX?: number,
  awayX?: number,
  graphData?: { height: number }[]
) => {
  const { battleReader } = useEthereum();
  const [path, setPath] = useState<IPath>({ len: 0, points: [] });
  const [pathMinus, setPathMinus] = useState<IPath>({ len: 0, points: [] });
  const [pathPlus, setPathPlus] = useState<IPath>({ len: 0, points: [] });
  const [detectCollision, setDetectCollision] = useState(false);
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    let flag = true;
    const getPath = async () => {
      if (!battleReader) return;

      setPath({ len: 0, points: [] });
      setPathMinus({ len: 0, points: [] });
      setPathPlus({ len: 0, points: [] });
      setDetectCollision(false);

      const convertedAngle = Math.floor((angle * 16384) / 360);

      if (!homeX || !awayX || !graphData || player === PlayerEnum.Spectator)
        return;

      if (power === 0) return;

      setLoading(true);

      const fireX = player === PlayerEnum.Home ? homeX : awayX;
      const targetX = player === PlayerEnum.Home ? awayX : homeX;

      const fireY = graphData[fireX].height;
      const targetY = graphData[targetX].height;

      const input = [
        {
          x: (fireX * 1000000).toString(),
          y: (fireY * 1000000).toString(),
        },
        {
          x: (targetX * 1000000).toString(),
          y: (targetY * 1000000).toString(),
        },
        {
          angle: convertedAngle,
          power: ((power * 1000000) / 100).toString(),
          ammoType: 1,
          accuracy: 0,
        },
        420000,
      ] as const;

      console.log({ input });

      try {
        // console.log("0");
        const getPathX = await battleReader.drawPathX(...input);
        // console.log("1");
        const getPathY = await battleReader.drawPathY(...input);

        const detectCollision = await battleReader.detectCollision(
          input[0],
          input[1],
          convertedAngle,
          ((power * 1000000) / 100).toString(),
          420000
        );

        const minusAngle = BigNumber.from(convertedAngle)
          .sub(BigNumber.from(16384).div(200).mul(5))
          .mod(16384);
        const plusAngle = BigNumber.from(convertedAngle)
          .add(BigNumber.from(16384).div(200).mul(5))
          .mod(16384);

        const getPathMinusX = await battleReader.drawPathX(
          input[0],
          input[1],
          {
            angle: minusAngle,
            power: input[2].power,
            ammoType: input[2].ammoType,
            accuracy: input[2].accuracy,
          },
          input[3]
        );
        // console.log("2");
        if (!flag) return;
        // console.log("here");
        const getPathMinusY = await battleReader.drawPathY(
          input[0],
          input[1],
          {
            angle: minusAngle,
            power: input[2].power,
            ammoType: input[2].ammoType,
            accuracy: input[2].accuracy,
          },
          input[3]
        );
        // console.log("3");
        if (!flag) return;
        const getPathPlusX = await battleReader.drawPathX(
          input[0],
          input[1],
          {
            angle: plusAngle,
            power: input[2].power,
            ammoType: input[2].ammoType,
            accuracy: input[2].accuracy,
          },
          input[3]
        );
        // console.log("4");
        if (!flag) return;
        const getPathPlusY = await battleReader.drawPathY(
          input[0],
          input[1],
          {
            angle: plusAngle,
            power: input[2].power,
            ammoType: input[2].ammoType,
            accuracy: input[2].accuracy,
          },
          input[3]
        );
        // console.log("5");
        if (!flag) return;
        // console.log(path);
        const path = [];
        const pathMinus = [];
        const pathPlus = [];
        // console.log("len", getPathX[1]);
        for (let i = 0; i < 4096; i++) {
          path.push({
            x: getPathX[0][i].toNumber() / 1000000,
            y: getPathY[0][i].toNumber() / 1000000,
          });
          pathMinus.push({
            x: getPathMinusX[0][i].toNumber() / 1000000,
            y: getPathMinusY[0][i].toNumber() / 1000000,
          });
          pathPlus.push({
            x: getPathPlusX[0][i].toNumber() / 1000000,
            y: getPathPlusY[0][i].toNumber() / 1000000,
          });
        }

        if (!flag) return;
        setPath({
          points: path,
          len: getPathX[1].toNumber(),
        });
        setPathMinus({
          points: pathMinus,
          len: getPathMinusX[1].toNumber(),
        });
        setPathPlus({
          points: pathPlus,
          len: getPathPlusX[1].toNumber(),
        });
        setDetectCollision(detectCollision);

        setLoading(false);
      } catch (e) {
        setLoading(false);
        // console.log("error", { e });
      }
    };

    getPath();

    return () => {
      flag = false;
    };
  }, [battleReader, angle, power]);

  return { path, detectCollision, pathMinus, pathPlus, loading };
};
