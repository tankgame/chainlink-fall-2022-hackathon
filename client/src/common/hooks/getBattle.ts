import { useState } from "react";
import { currentBattle } from "../api/battle";

export const useGetBattle = () => {
  const [battleStats, setBattleStats] = useState({} as BattleStats);

  const fetchBattleStats = async (battleId: string) => {
    try {
      //
      const res = await fetch(
        "https://api.thegraph.com/subgraphs/name/seidprojects/tank-game-battle",
        {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({
            query: currentBattle(battleId),
            variables: null,
          }),
        }
      ).then((res) => res.json());

      const battleStats = res?.data?.tankGames?.[0] || {};

      console.log({ battleStats });

      setBattleStats(battleStats);
    } catch (e) {
      console.log({ e });
    }
  };

  return { battleStats, fetchBattleStats };
};
