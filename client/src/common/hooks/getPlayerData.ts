import { useState } from "react";
import { playerDataFetch } from "../api/playerdata";

export const useGetPlayerData = () => {
  const [playerData, setPlayerData] = useState({} as BattleStats);

  const fetchPlayerData = async (address: string) => {
    try {
      //
      const res = await fetch(
        "https://api.thegraph.com/subgraphs/name/seidprojects/tank-game-battle",
        {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({
            query: playerDataFetch(address.toLowerCase()),
            variables: null,
          }),
        }
      ).then((res) => res.json());

      const playerData = res?.data?.players?.[0] || {};

      console.log({ playerData });

      setPlayerData(playerData);
    } catch (e) {
      console.log({ e });
    }
  };

  return { playerData, fetchPlayerData };
};
