import { useEffect, useState } from "react";

export const useKeyBoard = () => {
  const [keyPressed, setKeyPressed] = useState<{ [key: string]: boolean }>({
    ArrowUp: false,
    ArrowDown: false,
    Enter: false,
  });

  function downHandler({ key }: KeyboardEvent) {
    if (keyPressed[key] != undefined) {
      setKeyPressed((prev) => {
        return { ...prev, [key]: true };
      });
    }
  }

  const upHandler = ({ key }: KeyboardEvent) => {
    if (keyPressed[key] != undefined) {
      setKeyPressed((prev) => {
        return { ...prev, [key]: false };
      });
    }
  };

  useEffect(() => {
    window.addEventListener("keydown", downHandler);
    window.addEventListener("keyup", upHandler);
    return () => {
      window.removeEventListener("keydown", downHandler);
      window.removeEventListener("keyup", upHandler);
    };
  }, []);

  return keyPressed;
};
