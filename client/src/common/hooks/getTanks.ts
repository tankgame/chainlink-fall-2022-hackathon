import { useState } from "react";
import { fetchTanksDelivering } from "../api/tanks";

export const useGetTanks = () => {
  const [tanks, setTanks] = useState<Tank[]>([]);
  const [deliveringTanks, setDeliveringTanks] = useState<TankReceipt[]>([]);

  const fetchTanks = async (address: string, wait?: boolean) => {
    try {
      wait && (await new Promise((resolve) => setTimeout(resolve, 10000)));
      const res = await fetch(
        "https://api.thegraph.com/subgraphs/name/seidprojects/tank-game-battle",
        {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({
            query: `{ players (where: {id: "${address.toLowerCase()}"}) { id tanks { id uri type { name accuracy } homeGames { id } awayGames { id } } } }`,
            variables: null,
          }),
        }
      ).then((res) => res.json());

      const tanks: Tank[] = res?.data?.players[0]?.tanks || [];

      const promises: Promise<any>[] = [];

      // console.log("yeet", { tanks });

      tanks.forEach((tank) => {
        promises.push(fetch(tank.uri).then((res) => res.json()));
      });

      const tankData = await Promise.all(promises);

      // console.log("yeet", { tankData });

      // console.log(
      //   "yeet",
      //   tanks.map((tank, index) => ({
      //     ...tank,
      //     image: tankData?.[index]?.image,
      //   }))
      // );

      setTanks(
        tanks.map((tank, index) => ({
          ...tank,
          image: tankData?.[index]?.image,
        }))
      );
      // console.log(tanks);
    } catch (e) {
      console.log({ e });
    }
  };

  const fetchDelivering = async (address: string) => {
    const res = await fetch(
      "https://api.thegraph.com/subgraphs/name/seidprojects/tank-game-battle",
      {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          query: fetchTanksDelivering(address),
          variables: null,
        }),
      }
    ).then((res) => res.json());

    console.log("res", res.data);
    setDeliveringTanks(res.data.tankReceipts);
  };

  return { tanks, fetchTanks, deliveringTanks, fetchDelivering };
};
