import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import "./index.css";
import "@rainbow-me/rainbowkit/styles.css";
import { chain, configureChains, createClient, WagmiConfig } from "wagmi";
import { jsonRpcProvider } from "wagmi/providers/jsonRpc";
import { publicProvider } from "wagmi/providers/public";
import { getDefaultWallets, RainbowKitProvider } from "@rainbow-me/rainbowkit";
import { EthereumProvider } from "./common/context/ethereum";

const { chains, provider } = configureChains(
  [
    // chain.polygon,
    chain.polygonMumbai,
    // chain.localhost,
    chain.mainnet,
  ],
  [
    jsonRpcProvider({
      rpc: (chain) =>
        chain.id === 1337
          ? { http: "http://localhost:8545" }
          : chain.id === 137
          ? {
              http: "https://nameless-weathered-feather.matic.quiknode.pro/9b5ab369a9cf5af7d42ecad8b57952ed58f70b1d/",
            }
          : {
              http: "https://quiet-fragrant-flower.matic-testnet.quiknode.pro/752c376c3164f58990c50845e96119740bf8d266/",
            },
    }),
    publicProvider(),
  ]
);

const { connectors } = getDefaultWallets({
  appName: "Tank Game Dapp",
  chains,
});

const wagmiClient = createClient({
  autoConnect: true,
  connectors,
  provider,
});

// console.log(chains);

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <WagmiConfig client={wagmiClient}>
      <RainbowKitProvider chains={chains} coolMode>
        <EthereumProvider dev="mumbai">
          <App />
        </EthereumProvider>
      </RainbowKitProvider>
    </WagmiConfig>
  </React.StrictMode>
);
