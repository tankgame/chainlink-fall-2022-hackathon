/// <reference types="vite/client" />
/// <reference types="vite-plugin-svgr/client" />

interface Window {
  ethereum: any;
}

interface TankType {
  name: string;
  accuracy: string;
}
interface Tank {
  id: string | number;
  image: string;
  uri: string;
  type?: TankType;
  homeGames: number[];
  awayGames: number[];
}

interface Ammo {
  id: string | number;
  type: string;
  damage: number;
  amount: number;
  image?: string;
}

interface OrderTankCard {
  id: string;
  name: string;
  description: string;
  maxHealth: number;
  maxGas: number;
  accuracy: number;
  cost: number;
}

interface DeliveringTanks {
  id: string | number;
  requested: number;
  leadTime: number;
  expectedTimeOfDelivery: number;
}

type Point = { x: number; y: number };
interface IPath {
  len: number;
  points: Point[];
}

interface Position {
  x: number;
  y: number;
}

interface IForm {
  fire: Position;
  target: Position;
  power: number;
  angle: number;
}

interface IContract {
  [key: string]: {
    address: string;
    abi: any[];
    chainId: Chains;
  };
}

type Chains = 137 | 1337 | 80001;

type GameState =
  | "OFF"
  | "CREATED"
  | "GETTING_INITIAL_VRF"
  | "HOME"
  | "AWAY"
  | "GETTING_HOME_VRF"
  | "GETTING_AWAY_VRF"
  | "GAME_OVER"
  | "ABANDONED";

interface Game {
  id: string;
  gameState: GameState;
}

type BigNumber = import("ethers").BigNumber;

interface Player {
  id: string;
  tankNumber: BigNumber;
  location: BigNumber;
  joinTime: BigNumber;
}

interface Shot {
  power: BigNumber;
  angle: BigNumber;
  accuracy: BigNumber;
  ammoType: BigNumber;
}

interface BattleData {
  home: Player;
  away: Player;
  gameState: import("./common/context/ethereum").GameStateEnum;
  shot: Shot;
  timeOfLastAction: BigNumber;
}

interface PlayerTank {
  health: string;
  gas: string;
  type: {
    maxGas: string;
    maxHealth: string;
  };
}
interface BattleStats {
  homePlayerTank: PlayerTank;
  awayPlayerTank: PlayerTank;
  homePlayer: PlayerGraph;
  awayPlayer: PlayerGraph;
  winningTeam: string;
  winner: {
    id: string;
  };
}

interface PlayerMovement {
  awayLeft: BigNumber;
  awayRight: BigNumber;
  homeLeft: BigNumber;
  homeRight: BigNumber;
}

interface PlayerGraph {
  id: string;
  medals: {
    id: string;
    quantity: BigNumber;
  };
}

interface TankReceipt {
  id: string;
  buyer: PlayerGraph;
  timeOfPurchase: string;
  expectedTimeOfDelivery: string;
  delivered: boolean;
}

// interface TankStats {
//   awayPlayerTank:
// }
