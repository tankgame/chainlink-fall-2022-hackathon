/** @type {import('tailwindcss').Config} */

const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    screens: {
      // ...defaultTheme.screens,
      xl: { max: "1280px" },
      lg: { max: "1024px" },
      md2: { max: "850px" },
      md: { max: "768px" },
      sm: { max: "640px" },
    },
    extend: {
      boxShadow: {
        "3xl": "0 35px 60px -15px rgba(0, 0, 0, 0.3)",
      },
      colors: {
        modal: "rgb(0,0,0,0.75)",
        fire: "#B5120D",
        "fire-dark": "#940808",
        fog: "#DBD9D9",
        lake: "#2280E3",
        sand: "#DAC097e9",
        rust: "#A97E43",
        rust: "#A97E43",
        rustTransparent: "#A97E43aa",
        "rust-hover": "#654b28",
        black: "#101010",
        white: "#efefef",
        gray: "#404040",
        darkGray: "#202020",
        lime: "#BCFFBB",
        shellMetal: "#2F3A36",
      },
      minWidth: {
        "1/3": "33.33333%",
        40: "10rem",
      },
      backgroundImage: {
        wave: "linear-gradient(90deg,transparent,#A97E43,#A97E43,transparent)",
        "landing-bg": "url('./assets/images/landing-bg.png')",
        "factory-bg": "url('./assets/images/factory.png')",
        notFound: "url('./assets/images/notFound.png')",
        snow: "url('./assets/images/snow.png')",
        forest: "url('./assets/images/forest.png')",
        pyramid: "url('./assets/images/pyramid.png')",
        hud: "url('./assets/images/hud.png')",
      },
      fontFamily: {
        Military: ["Special Elite", "cursive"],
      },
      boxShadow: {
        card: "0px 4px 4px rgba(0, 0, 0, 0.25)",
      },
      keyframes: {
        fadeInLeft: {
          "0%": { opacity: 0, translate: "100px" },
          "100%": { opacity: 100, translate: "0px" },
        },
        fadeInRight: {
          "0%": { opacity: 0, translate: "-100px" },
          "100%": { opacity: 100, translate: "0px" },
        },
      },
      animation: {
        fadeInLeft: "fadeInLeft .25s ease-in",
        fadeInRight: "fadeInRight .25s ease-in",
        wave: "wave 2s ease-in infinite",
      },
    },
  },
  plugins: [],
};
