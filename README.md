# Tank Game - A web3 gaming ecosystem with on-chain physics
## Inspiration
When we first learned of the Chainlink Fall Hackathon, our team wanted to build something that would invoke a bit of nostalgia; what game from our past could we reinvent anew on blockchain? We chose tank game as we all had fond memories of our childhood playing games like Scorched Earth on our earliest home computers. Given that resource management on blockchains has many parallels to resource management on early computers, we thought it was a great fit and a fun experiment.

## What it does
Tank Game is web3, PvP battleground on the Polygon Blockchain! Tank Game is a series of smart contracts - a solidity protocol - for players to engage in tank battles. Players can join games to battle other players using their Tank NFTs (ERC-721s) to lob volleys at each other across a randomly generated map. Each shot leverages on chain physics to determine the parabolic arc taken in an attempt to smite their opponent, with a call to Chainlink VRF to determine the actual accuracy of the shot to make the game more fun to play. 

The losing player loses their tank - it explodes into Scrap, the native ERC-20 token of the ecosystem, and is unevenly distributed to the winner and the loser. New tanks can be purchased with scrap from the Tank Factory. Tanks take time to be assembled and delivered (e.g. 24 hours), but are sent automatically to players once completed using Chainlink Keepers. Tanks can be repaired at the mechanic shop by sending your Tank NFT, whose time to repair is determined by number of health and gas needing to be restored.

Try it out at [tankgame.atlascorp.io](https://tankgame.xyz)

## How we built it
We built this game using a micro-contract framework; we used many independent, tightly-scoped smart contracts interwoven together in lieu of one large contract to maximize modularity so we can add or amend features of the game over time. This project deploys the majority of the imagined gaming ecosystem on the Polygon Blockchain (and Mumbai testnet), including:
* A Tank contract (ERC-721) that has defined Tank Types and unique, dynamic metadata
* A metadata contract to manage the current stats (health, gas) of player's tanks 
* An ammunition contract, as shots consume ammunition. Ammo are ERC-1155 tokens.
* A scrap contract, the native token for the ecosystem. Scrap can be used to make tanks, ammo, changing your soldier name, and speeding up repairs with the mechanic contract. At the end of battles, the losing tank explodes into scrap and is dispersed between the winner and the loser. Scrap are ERC-20 tokens.
* A Tank / Ammunition stockpile to claim tanks for free to play
* A mechanic contract which can repair health points of tanks, and refill gas tanks over time.
* A terrain contract which holds the map details. (We did build the functions to have this randomly generated but for reasons discussed below its unfortunately infeasible).
* A Tank Battle contract that facilitates a PvP game against two players and their tanks.
* A Medals contract, to distribute medals per victory that can be traded in to get more advanced medals as ERC-1155 tokens.

The game relies on the above protocol of smart contracts and allows people to play the game via a React dApp (though the game could be played directly on Etherscan or equivalent, though we recommend against it given its complexity). A subgraph indexes many of the smart contracts to provide data to the dApp to minimize the number of smart contract calls.

All randomization is provided by Chainlink VRF, and all automated processes (e.g. tank delivery after the assembly window) is done using Chainlink Keepers. All blockchain connectivity is powered by Quicknode. All cloud infrastructure is powered by Digital Ocean.

All artwork was generated by AI (e.g. Midjourney). All Music composed by Drew Gilman.

## Challenges we ran into
We ran into a few fundamental, almost comical issues when it comes to running calculations on chain. We had originally planned on generating the terrain (the map) dynamically. After the code was written and we did some estimation, it was determined that to generate a full map would use _6 full blocks of gas_ or almost 180 million gas. Not quite feasible.

## Accomplishments that we're proud of
To be frank, we're proud of everything. This was a lot of fun to design, build and implement. A few key innovations stand out.

One area we spent a lot of time on is on the randomness around the accuracy of player shots. Without any randomness, the user just could choose the correct shot angle every time and whoever shoots first would win. But we needed something that feels realistic and is simple to compute. To achieve this, we took advantage of the law of large numbers, summing independent random variables which efficiently creates a distribution curve that makes the game more "life like". The outcome is a probability density function that represents a normally distributed set of random numbers in lieu of a constant density function evenly distributed from 0-1. 

We've also built some complicated smart contracts before, but never anything as modular as Tank Game. Every single facet of the tank game ecosystem has its own smart contract which can be updated, replaced, or tweaked as necessary without disruption of the overall game. 

Also, with everything on chain there is no need to maintain any infrastructure. Tank Game can in theory live on forever on Polygon and IPFS without any human intervention.

## What we learned
We learned how to make effective use of Chainlink keepers during this hackathon. In the past we've relied on cron jobs on cloud servers to interact with smart contracts, but using Chainlink Keepers is much more stable and reliable. We think it's pretty cool that you can place an order for a tank and after a predetermined period of time it gets shipped to you without having to come claim it.

## What's next for Tank Game
There's a lot we wanted to do with Tank Game that we weren't able to in the time allotted for the hackathon. 

There were a few features we ran out of time but would have loved to include. We wanted to introduce the concept of Tank Pilots which would be independent of tanks with their own health and skills. Like tanks, they would need to be "healed" at a medbay/hospital contract, and might affect the shot by increasing accuracy.

We also wanted to include air resistance which would affect the path the shot takes, which would come with its own VRF random number. This could be included in future releases as a property of the terrain map.

For now we plan on releasing the game after the hackathon ends for people to play for free. We would love feedback from the community as to how much fun it is to play and how the game might be improved.

We thank the community, the Chainlink team, its sponsors, and all of our hackathon peers for the opportunity to demonstrate our game and have fun playing it with us.

See you on the battlefield!

# Open React Dapp Locally
* note this will not work with the graph

Start Ganache Local Blockchain
```bash
ganache-cli -d true -i 1337
```

Deploy the contracts
```bash
sudo truffle migrate --network development
```

Enter the client directory and install dependency

```bash
cd client
npm install
```

in client/src/app.tsx make sure on line 53:
```html
<EthereumProvider dev="dev">
```

### Run the App
```bash
npm run dev
```
