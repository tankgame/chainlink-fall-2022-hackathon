module.exports = {
	contracts_build_directory: "./client/src/contracts",
	dashboard: {
		port: 25012,
	},

	networks: {
		development: {
			host: "127.0.0.1", // Localhost (default: none)
			port: 8545, // Standard Ganache cli port
			network_id: "*",
		},

		live: {
			network_id: 137, // Polygon Mainnet
			confirmations: 2, // # of confs to wait between deployments. (default: 0)
			timeoutBlocks: 200, // # of blocks before a deployment times out  (minimum/default: 50)
		},
		mumbai: {
			network_id: 80001,
			confirmations: 2,
		},
	},

	// Set default mocha options here, use special reporters etc.
	mocha: {
		colors: true,
		timeout: 100000,
	},

	// Configure your compilers
	compilers: {
		solc: {
			version: "0.8.17", // Fetch exact version from solc-bin (default: truffle's version)
			settings: {
				// See the solidity docs for advice about optimization and evmVersion
				optimizer: {
					enabled: true,
					runs: 200,
				},
			},
		},
	},
	plugins: ["truffle-plugin-verify"],
	api_keys: {
		etherscan: "ZY6KPCXM6VVR6MRE6ARGIN8VIUQRSCM3V7",
		polygonscan: "6B3QSDS3FQNY7X8J6NV9WSPVWN24SMDP4Z",
	},
};
