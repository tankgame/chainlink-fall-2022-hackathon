import { ByteArray, BigInt, store, log } from "@graphprotocol/graph-ts"
import {
  TankGameBattle,
  FireFulfilled,
  FireRequested,
  GameAbandoned,
  GameCreated,
  GameJoined,
  GameOver,
  GameSet,
  Move,
  Hit,
  Miss,
  RoleAdminChanged,
  RoleGranted,
  RoleRevoked
} from "../generated/TankGameBattle/TankGameBattle"
import {
  mechanic,
  RefillPeriodUpdated,
  RefillPriceUpdated,
  RefillRateUpdated,
  RestorationPeriodUpdated,
  RestorationPriceUpdated,
  RestorationRateUpdated,
  TankReceived,
  TankReturned
} from "../generated/mechanic/mechanic"
import {
  tanks,
  Approval,
  ApprovalForAll,
  OwnershipTransferred,
  TankTypeCreated,
  Transfer
} from "../generated/tanks/tanks"
import {
  metadata,
  MetadataFieldUpdated
} from "../generated/metadata/metadata"
import {
  Ammo,
  AmmoTypeCreated,
  TransferBatch,
  TransferSingle,
  URI
} from "../generated/Ammo/Ammo"
import {
  Medals,
  TransferBatch as MedalsTransferBatch,
  TransferSingle as MedalsTransferSingle,
  URI as MedalsURI
} from "../generated/Medals/Medals"
import {
  TankFactory,
  FactoryClosed,
  FactoryOpened,
  LeadTimeUpdated,
  ScrapContractUpdated,
  ScrapWithdrawn,
  TankCommissioned,
  TankDelivered,
  TankContractUpdated,
  TankPriceUpdated
} from "../generated/TankFactory/TankFactory"
import { tankGame, Shot, Player, TankType, Tank, RepairTicket, AmmunitionType, AmmunitionBalance, MedalType, MedalBalance, Factory, TankReceipt } from "../generated/schema"

let gameStates = ["OFF", "CREATED","GETTING_INITIAL_VRF","HOME","AWAY","GETTING_HOME_VRF","GETTING_AWAY_VRF","GAME_OVER","ABANDONED"]

export function handleFireFulfilled(event: FireFulfilled): void {
  let game = tankGame.load(event.params.gameId.toString())
  let shot = Shot.load(game!.id.toString() + "-" + game!.shotCount.toString())
  let contract = TankGameBattle.bind(event.address)
  let gameData = contract.gameData(BigInt.fromString(game!.id))
  shot!.accuracy = gameData.value3.accuracy
  game!.shotCount = game!.shotCount.plus(BigInt.fromI32(1))
  game!.save()
  shot!.save()
}

export function handleFireRequested(event: FireRequested): void {
  let game = tankGame.load(event.params.gameId.toString())
  let contract = TankGameBattle.bind(event.address)
  let gameData = contract.gameData(BigInt.fromString(game!.id))
  game!.gameState = gameStates[gameData.value2]
  let shot = new Shot(game!.id.toString() + "-" + game!.shotCount.toString())
  if(game!.gameState === "GETTING_HOME_VRF"){
    shot.player = game!.homePlayer
  }else{
    shot.player = game!.awayPlayer!
  }
  shot.game = game!.id
  shot.power = gameData.value3.power
  shot.angle = BigInt.fromI32(gameData.value3.angle)
  shot.ammoType= gameData.value3.ammoType.toString()
  let shots = game!.shots
  shots!.push(shot.id)
  game!.shots = shots
  shot.save()
  game!.save()
}

export function handleGameAbandoned(event: GameAbandoned): void {
  let game = tankGame.load(event.params.gameId.toString())
  let contract = TankGameBattle.bind(event.address)
  let gameData = contract.gameData(BigInt.fromString(game!.id))
  game!.gameState = gameStates[gameData.value2]
  game!.save()
}

export function handleGameCreated(event: GameCreated): void {
    //init game
    let contract = TankGameBattle.bind(event.address)
    let game = tankGame.load(event.params.gameId.toString())
    if (!game) {
      game = new tankGame(event.params.gameId.toString())
      let gameData = contract.gameData(BigInt.fromString(game.id))
      let playerAddr = gameData.value0.id
      let player = Player.load(playerAddr.toHexString())
      if(!player){
        player = new Player(playerAddr.toHexString())
        player.numberOfBattles = BigInt.fromI32(0)
        //player.rank = BigInt.fromI32(0)
      }
      game.homePlayer = playerAddr.toHexString()
      game.homePlayerTank = gameData.value0.tankNumber.toString()
      game.homePlayerLocation = gameData.value0.location
      game.homePlayerJoinTime = gameData.value0.joinTime
      game.gameState = gameStates[gameData.value2]
      game.shotCount = BigInt.fromI32(0)
      game.shots = []
      player.numberOfBattles = player.numberOfBattles.plus(BigInt.fromI32(1))
      let tank = Tank.load(gameData.value0.tankNumber.toString())
      tank!.numberOfBattles = tank!.numberOfBattles.plus(BigInt.fromI32(1))
      tank!.save()
      player.save() 
    }
    game.save()  
}

export function handleGameJoined(event: GameJoined): void {
  let contract = TankGameBattle.bind(event.address)
  let game = tankGame.load(event.params.gameId.toString())
  let gameData = contract.gameData(BigInt.fromString(game!.id))
  let playerAddr = gameData.value1.id
  let player = Player.load(playerAddr.toHexString())
  if(!player){
    player = new Player(playerAddr.toHexString())
    player.numberOfBattles = BigInt.fromI32(0)
    //player.rank = BigInt.fromI32(0)
  }
  game!.awayPlayer = playerAddr.toHexString()
  game!.awayPlayerTank = gameData.value1.tankNumber.toString()
  game!.awayPlayerLocation = gameData.value1.location
  game!.awayPlayerJoinTime = gameData.value1.joinTime
  game!.gameState = gameStates[gameData.value2]
  player.numberOfBattles = player.numberOfBattles.plus(BigInt.fromI32(1))
  let tank = Tank.load(gameData.value0.tankNumber.toString())
  tank!.numberOfBattles = tank!.numberOfBattles.plus(BigInt.fromI32(1))
  tank!.save()
  game!.save()   
  player.save()
}

export function handleGameOver(event: GameOver): void {
  let game = tankGame.load(event.params.gameId.toString())
  let contract = TankGameBattle.bind(event.address)
  let gameData = contract.gameData(BigInt.fromString(game!.id))
  game!.gameState = gameStates[gameData.value2]

  //who won?
  let homeTank = Tank.load(game!.homePlayerTank)
  if(homeTank!.health == BigInt.fromI32(0)){
    game!.winner = game!.awayPlayer!
    game!.winningTeam = "AWAY"
  }else{
    game!.winner = game!.homePlayer
    game!.winningTeam = "HOME"
  }
  game!.save()
}

export function handleGameSet(event: GameSet): void {
  let game = tankGame.load(event.params.gameId.toString())
  let contract = TankGameBattle.bind(event.address)
  let gameData = contract.gameData(BigInt.fromString(game!.id))
  game!.homePlayerLocation = gameData.value0.location
  game!.awayPlayerLocation = gameData.value1.location
  game!.gameState = gameData.value2.toString()
  game!.save()
}

export function handleMove(event: Move): void {
  let game = tankGame.load(event.params.gameId.toString())
  let contract = TankGameBattle.bind(event.address)
  let gameData = contract.gameData(BigInt.fromString(game!.id))
  game!.homePlayerLocation = gameData.value0.location
  game!.awayPlayerLocation = gameData.value1.location
  game!.gameState = gameStates[gameData.value2]
  game!.save()
}

export function handleHit(event: Hit): void {
  let game = tankGame.load(event.params.gameId.toString())
  let shot = Shot.load(game!.id.toString() + "-" + game!.shotCount.minus(BigInt.fromI32(1)).toString())
  shot!.hit = "HIT"
  shot!.save()
}

export function handleMiss(event: Miss): void {
  let game = tankGame.load(event.params.gameId.toString())
  let shot = Shot.load(game!.id.toString() + "-" + game!.shotCount.minus(BigInt.fromI32(1)).toString())
  shot!.hit = "MISS"
  shot!.save()
}

export function handleRoleAdminChanged(event: RoleAdminChanged): void {}

export function handleRoleGranted(event: RoleGranted): void {}

export function handleRoleRevoked(event: RoleRevoked): void {}

//Start Tank Contract functions
export function handleApproval(event: Approval): void {}

export function handleApprovalForAll(event: ApprovalForAll): void {}

export function handleOwnershipTransferred(event: OwnershipTransferred): void {}

export function handleTankTypeCreated(event: TankTypeCreated): void {
    let contract = tanks.bind(event.address)
    let tanktype = TankType.load(event.params.tankType.toString())
    if (!tanktype) {
      tanktype = new TankType(event.params.tankType.toString())
    }
    let tankTypeData = contract.tankTypes(BigInt.fromString(tanktype.id.toString()))
    tanktype.name = tankTypeData.value0.toString()
    tanktype.description = tankTypeData.value1.toString()
    tanktype.maxHealth = tankTypeData.value2
    tanktype.maxGas = tankTypeData.value3
    tanktype.accuracy = tankTypeData.value4
    tanktype.developed = tankTypeData.value5
    tanktype.save()  
}

export function handleTransfer(event: Transfer): void {
    let sender = event.params.from.toHexString()
    let contract = tanks.bind(event.address)
    if (sender == '0x0000000000000000000000000000000000000000') {
      let tank = Tank.load(event.params.tokenId.toString())
      if(!tank){
        tank = new Tank(event.params.tokenId.toString())
        tank.numberOfBattles = BigInt.fromI32(0)
        tank.type = contract.tankTypePerTokenId(event.params.tokenId).toString()
        let tanktype = TankType.load(contract.tankTypePerTokenId(event.params.tokenId).toString())
        let tankTypeData = contract.tankTypes(BigInt.fromString(tanktype!.id.toString()))
        tank.health = tankTypeData.value2
        tank.gas = tankTypeData.value3
        tank.uri = contract.tokenURI(event.params.tokenId)
        let player = Player.load(event.params.to.toHexString())
        if(!player){
            player = new Player(event.params.to.toHexString())
            player.numberOfBattles = BigInt.fromI32(0)
            //player.rank = BigInt.fromI32(0)
            player.save()
        }
        tank.player = player.id
        tank.save()
      }
    }else{
      let player = Player.load(event.params.to.toHexString())
      if(!player){
          player = new Player(event.params.to.toHexString())
          player.numberOfBattles = BigInt.fromI32(0)
          //player.rank = BigInt.fromI32(0)
          player.save()
        }
        let tank = Tank.load(event.params.tokenId.toString())
        tank!.player = player.id
        tank!.save()
    }
  }
  

//Start Mechanic Contract functions

export function handleRefillPeriodUpdated(event: RefillPeriodUpdated): void {}

export function handleRefillPriceUpdated(event: RefillPriceUpdated): void {}

export function handleRefillRateUpdated(event: RefillRateUpdated): void {}

export function handleRestorationPeriodUpdated(event: RestorationPeriodUpdated): void {}

export function handleRestorationPriceUpdated(event: RestorationPriceUpdated): void {}

export function handleRestorationRateUpdated(event: RestorationRateUpdated): void {}

export function handleTankReceived(event: TankReceived): void {
  let mechanic_contract = mechanic.bind(event.address)
  let repairInfo = mechanic_contract.repairTickets(event.params.tankId)
  let repairTicket = new RepairTicket(event.params.tankId.toString())
  repairTicket.tank = event.params.tankId.toString()
  repairTicket.player = repairInfo.value0.toHexString()
  repairTicket.arrivalTime = repairInfo.value1
  repairTicket.restorationRate = repairInfo.value2
  repairTicket.restorationPeriod = repairInfo.value3
  repairTicket.refillRate = repairInfo.value4
  repairTicket.refillPeriod = repairInfo.value5
  repairTicket.save()
}

export function handleTankReturned(event: TankReturned): void {
  store.remove('RepairTicket',event.params.tankId.toString())
}

//metadata contract - update later to handle metadata of multiple contracts on the first field
export function handleMetadataFieldUpdated(event: MetadataFieldUpdated): void {
    let tank = Tank.load(event.params._tokenId.toString())
    if(event.params._metadataField.toHexString() == '0x9005b4bf3e8c5705ff2b172ed7c0ff76f0096efed0943457e640b66da01b97d2'){
      log.info('HEALTH',[])
      tank!.health = event.params._value
    }else if(event.params._metadataField.toHexString() == "0x4498c2139ad6cf2beef3ae7bec34c4856d471c8680dfd28d553f117df74df6b7"){
      log.info('GAS',[])
      tank!.gas = event.params._value
    }
    tank!.save()
}

//ammo contract
export function handleAmmoTypeCreated(event: AmmoTypeCreated): void {
  let contract = Ammo.bind(event.address)
  let ammoType = AmmunitionType.load(event.params._id.toString())
  if (!ammoType) {
    ammoType = new AmmunitionType(event.params._id.toString())
  }
  let ammoTypeData = contract.ammoTypes(event.params._id)
  ammoType.developed = ammoTypeData.value0
  ammoType.name = ammoTypeData.value1
  ammoType.damage = ammoTypeData.value2
  ammoType.spread = ammoTypeData.value3
  ammoType.weight = ammoTypeData.value4
  ammoType.save()  
}

export function handleTransferBatch(event: TransferBatch): void {
  let from_player = Player.load(event.params.from.toHexString())
  if(!from_player){
    from_player = new Player(event.params.from.toHexString())
    from_player.numberOfBattles = BigInt.fromI32(0)
    from_player.save()      
  }
  let to_player = Player.load(event.params.to.toHexString())
  if(!to_player){
    to_player = new Player(event.params.from.toHexString())
    to_player.numberOfBattles = BigInt.fromI32(0)
    to_player.save()
  }
  
  let counter = 0
  for(let i = 0; i < event.params.ids.length; i++){
    let ammotype = AmmunitionType.load(event.params.ids.at(1).toString())
    let from_balance = AmmunitionBalance.load(from_player.id.concat('-').concat(ammotype!.id))
    if(!from_balance){
      from_balance = new AmmunitionBalance(from_player.id.concat('-').concat(ammotype!.id))
      from_balance.type = ammotype!.id
      from_balance.player = from_player.id
      from_balance.quantity = BigInt.fromI32(0)
    }
    let to_balance = AmmunitionBalance.load(to_player.id.concat('-').concat(ammotype!.id))
    if(!to_balance){
      to_balance = new AmmunitionBalance(to_player.id.concat('-').concat(ammotype!.id))
      to_balance.type = ammotype!.id
      to_balance.player = to_player.id
      to_balance.quantity = BigInt.fromI32(0)
    }
    from_balance.quantity = from_balance.quantity.minus(event.params.values.at(i))
    to_balance.quantity = to_balance.quantity.plus(event.params.values.at(i))
    from_balance.save()
    to_balance.save()  
    counter += 1  
  }
}

export function handleTransferSingle(event: TransferSingle): void {
    let from_player = Player.load(event.params.from.toHexString())
    if(!from_player){
      from_player = new Player(event.params.from.toHexString())
      from_player.numberOfBattles = BigInt.fromI32(0)
      from_player.save()      
    }
    let to_player = Player.load(event.params.to.toHexString())
    if(!to_player){
      to_player = new Player(event.params.from.toHexString())
      to_player.numberOfBattles = BigInt.fromI32(0)
      to_player.save()
    }
    let ammotype = AmmunitionType.load(event.params.id.toString())

    let from_balance = AmmunitionBalance.load(from_player.id.concat('-').concat(event.params.id.toString()))
    if(!from_balance){
      from_balance = new AmmunitionBalance(from_player.id.concat('-').concat(event.params.id.toString()))
      from_balance.type = ammotype!.id
      from_balance.player = from_player.id
      from_balance.quantity = BigInt.fromI32(0)
    }
    let to_balance = AmmunitionBalance.load(to_player.id.concat('-').concat(event.params.id.toString()))
    if(!to_balance){
      to_balance = new AmmunitionBalance(to_player.id.concat('-').concat(event.params.id.toString()))
      to_balance.type = ammotype!.id
      to_balance.player = to_player.id
      to_balance.quantity = BigInt.fromI32(0)
    }
    from_balance.quantity = from_balance.quantity.minus(event.params.value)
    to_balance.quantity = to_balance.quantity.plus(event.params.value)
    from_balance.save()
    to_balance.save()
}

export function handleURI(event: URI): void {
  let ammotype = AmmunitionType.load(event.params.id.toString())
  ammotype!.uri = event.params.value
  ammotype!.save()
}

//Medals
export function handleMedalsTransferBatch(event: MedalsTransferBatch): void {
  let from_player = Player.load(event.params.from.toHexString())
  if(!from_player){
    from_player = new Player(event.params.from.toHexString())
    from_player.numberOfBattles = BigInt.fromI32(0)
    from_player.save()      
  }
  let to_player = Player.load(event.params.to.toHexString())
  if(!to_player){
    to_player = new Player(event.params.from.toHexString())
    to_player.numberOfBattles = BigInt.fromI32(0)
    to_player.save()
  }
  
  let counter = 0
  for(let i = 0; i < event.params.ids.length; i++){
    let medaltype = MedalType.load(event.params.ids.at(1).toString())
    let from_balance = MedalBalance.load(from_player.id.concat('-').concat(medaltype!.id))
    if(!from_balance){
      from_balance = new MedalBalance(from_player.id.concat('-').concat(medaltype!.id))
      from_balance.type = medaltype!.id
      from_balance.player = from_player.id
      from_balance.quantity = BigInt.fromI32(0)
    }
    let to_balance = AmmunitionBalance.load(to_player.id.concat('-').concat(medaltype!.id))
    if(!to_balance){
      to_balance = new AmmunitionBalance(to_player.id.concat('-').concat(medaltype!.id))
      to_balance.type = medaltype!.id
      to_balance.player = to_player.id
      to_balance.quantity = BigInt.fromI32(0)
    }
    from_balance.quantity = from_balance.quantity.minus(event.params.values.at(i))
    to_balance.quantity = to_balance.quantity.plus(event.params.values.at(i))
    //to_player.rank = to_player.rank.plus(BigInt.fromI32(3).pow(event.params.ids.at(i).minus(BigInt.fromI32(1)).toI32()))
    //from_player.rank = to_player.rank.minus(BigInt.fromI32(3).pow(event.params.ids.at(i).minus(BigInt.fromI32(1)).toI32()))
    from_balance.save()
    to_balance.save()  
    counter += 1  
  }
}

export function handleMedalsTransferSingle(event: MedalsTransferSingle): void {
  let medaltype = MedalType.load(event.params.id.toString())
  if (!medaltype) {
    medaltype = new MedalType(event.params.id.toString())
  }
  medaltype.save()
  let from_player = Player.load(event.params.from.toHexString())
    if(!from_player){
      from_player = new Player(event.params.from.toHexString())
      from_player.numberOfBattles = BigInt.fromI32(0)
      from_player.save()      
    }
    let to_player = Player.load(event.params.to.toHexString())
    if(!to_player){
      to_player = new Player(event.params.from.toHexString())
      to_player.numberOfBattles = BigInt.fromI32(0)
      to_player.save()
    }

    let from_balance = MedalBalance.load(from_player.id.concat('-').concat(event.params.id.toString()))
    if(!from_balance){
      from_balance = new MedalBalance(from_player.id.concat('-').concat(event.params.id.toString()))
      from_balance.type = medaltype.id
      from_balance.player = from_player.id
      from_balance.quantity = BigInt.fromI32(0)
    }
    let to_balance = MedalBalance.load(to_player.id.concat('-').concat(event.params.id.toString()))
    if(!to_balance){
      to_balance = new MedalBalance(to_player.id.concat('-').concat(event.params.id.toString()))
      to_balance.type = medaltype.id
      to_balance.player = to_player.id
      to_balance.quantity = BigInt.fromI32(0)
    }
    from_balance.quantity = from_balance.quantity.minus(event.params.value)
    to_balance.quantity = to_balance.quantity.plus(event.params.value)
    //to_player.rank = to_player.rank.plus(BigInt.fromI32(3).pow(event.params.id.minus(BigInt.fromI32(1)).toI32()))
    //from_player.rank = to_player.rank.minus(BigInt.fromI32(3).pow(event.params.id.minus(BigInt.fromI32(1)).toI32()))
    from_balance.save()
    to_balance.save()
}

export function handleMedalsURI(event: MedalsURI): void {
  let medaltype = MedalType.load(event.params.id.toString())
  medaltype!.uri = event.params.value
  medaltype!.save()
}

//tank factory

export function handleFactoryClosed(event: FactoryClosed): void {
  let tankfactory = Factory.load("Tank")
  if(!tankfactory){
    tankfactory = new Factory("Tank")
    tankfactory.type = "Tank"
    tankfactory.open = false
  }
  tankfactory.save()
}

export function handleFactoryOpened(event: FactoryOpened): void {
  let tankfactory = Factory.load("Tank")
  if(!tankfactory){
    tankfactory = new Factory("Tank")
    tankfactory.type = "Tank"
    tankfactory.open = true
  }
  tankfactory.save()
}

export function handleTankCommissioned(event: TankCommissioned): void {
  let tankreceipt = TankReceipt.load(event.params.receiptNumber.toString())
  let contract = TankFactory.bind(event.address)
  let receipt = contract.commissionReceipts(event.params.receiptNumber)
  if(!tankreceipt){
    tankreceipt = new TankReceipt(event.params.receiptNumber.toString())
    tankreceipt.buyer = receipt.value0.toHexString()
    tankreceipt.tankType = receipt.value1.toString()
    tankreceipt.timeOfPurchase = receipt.value2
    tankreceipt.expectedTimeOfDelivery = receipt.value3
    tankreceipt.delivered = false
  }
  tankreceipt.save()
}

export function handleTankDelivered(event: TankDelivered): void {
  let tankreceipt = TankReceipt.load(event.params.receiptNumber.toString())
  tankreceipt!.delivered = true
  tankreceipt!.save()
}