google.charts.load("current", { packages: ["corechart"] });
google.charts.setOnLoadCallback(drawChart);

async function drawChart() {
  const provider = new ethers.providers.Web3Provider(window.ethereum, "any");
  await provider.send("eth_requestAccounts", []);
  const signer = provider.getSigner();

  console.log("got provider");

  const terrainGenerator = await fetch(
    "./build/contracts/TerrainGenerator.json"
  ).then((response) => response.json());
  const ProjectileCalculator = await fetch(
    "./build/contracts/ProjectileCalculator.json"
  ).then((response) => response.json());

  const terrainAddress = terrainGenerator.networks[1337].address;
  const projectileAddress = ProjectileCalculator.networks[1337].address;

  const terrainAbi = terrainGenerator.abi;
  const projectileAbi = ProjectileCalculator.abi;

  const TerrainContract = new ethers.Contract(
    terrainAddress,
    terrainAbi,
    provider
  );

  const ProjectileContract = new ethers.Contract(
    projectileAddress,
    projectileAbi,
    signer
  );

  //prints Terrains
  let terrains = [];
  const samples = [0, 16, 32, 64, 128];
  var options = {
    legend: { position: "none" },
    title: "Company Performance",

    hAxis: {
      title: "Year",
      titleTextStyle: { color: "#333" },
      viewWindowMode: "maximized",
      viewWindow: {
        min: 0,
        max: 1024,
      },
    },

    chartArea: { left: 0, top: 0, width: "100%", height: "100%" },
    vAxis: {
      minValue: 0,
    },
    color: ["red", "green"],
    explorer: {
      actions: ["dragToZoom", "rightClickToReset"],
      axis: "horizontal",
    },
  };

  for (let i = 0; i < samples.length; i++) {
    console.log("yeet");
    const Terraindata = await TerrainContract.getSampledTerrain(samples[i]);
    // console.log(Terraindata.map((value) => value.toString()));
    terrains.push(
      google.visualization.arrayToDataTable([
        ["point", "height"],
        ...Terraindata.map((value, index) => {
          return [index, +value.toString()];
        }),
      ])
    );

    var chart = new google.visualization.AreaChart(
      document.getElementById(`chart_div${i}`)
    );
    chart.draw(terrains[i], options);
  }

  const smoothTerrain = await TerrainContract.getSmoothTerrain();
  const smoothData = google.visualization.arrayToDataTable([
    ["point", "height"],
    ...smoothTerrain.map((value, index) => {
      return [index, +value.toString()];
    }),
  ]);
  var chart = new google.visualization.LineChart(
    document.getElementById(`chart_div${samples.length}`)
  );
  chart.draw(smoothData, { ...options, curveType: "function" });

  //Prints Projectile Path

  options = {
    ...options,
    vAxis: {
      ...options.vAxis,
      viewWindow: {
        min: 0,
        max: 15,
      },
    },
  };

  console.log("yo");
  let reciept = await ProjectileContract.makePath(4096, 1, 0, 0, 1000);
  console.log(reciept);
  await reciept.wait();
  // await ProjectileContract.makePath(4096, 1, 0, 0, 1000); //90deg

  let [pathX, pathY] = await ProjectileContract.getPath().catch((err) =>
    console.log({ err })
  );

  let projectile = google.visualization.arrayToDataTable([
    ["X", "Y"],
    ...pathX.map((x, i) => [+x.toString(), +pathY[i].toString()]),
  ]);

  var chart = new google.visualization.ScatterChart(
    document.getElementById("chart_div6")
  );

  chart.draw(projectile, options);

  // await ProjectileContract.makePath(2048, 1, 0, 0, 1000).catch((err) =>
  //   console.log({ err })
  // );
  reciept = await ProjectileContract.makePath(2048, 1, 0, 0, 1000); //45deg
  await reciept.wait();
  [pathX, pathY] = await ProjectileContract.getPath();

  console.log(pathX.toString(), pathY.toString());

  const projectile1 = google.visualization.arrayToDataTable([
    ["X", "Y"],
    ...pathX.map((x, i) => [+x.toString(), +pathY[i].toString()]),
  ]);

  var chart = new google.visualization.ScatterChart(
    document.getElementById("chart_div7")
  );

  chart.draw(projectile1, options);

  //30deg
  reciept = await ProjectileContract.makePath(200, 10, 0, 0, 1000);
  await reciept.wait();
  [pathX, pathY] = await ProjectileContract.getPath();

  console.log(pathX.toString(), pathY.toString());

  const projectile2 = google.visualization.arrayToDataTable([
    ["X", "Y"],
    ...pathX.map((x, i) => [+x.toString(), +pathY[i].toString()]),
  ]);

  var chart = new google.visualization.ScatterChart(
    document.getElementById("chart_div8")
  );

  chart.draw(projectile2, options);
}
