// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;

import "./libraries/Trigonometry.sol";

contract ProjectileCalculator {
    int256 public FLOAT_CONSTANT;

    // mapping(uint256 => int256) public pathX;
    // mapping(uint256 => int256) public pathY;
    uint256 pathLength;
    uint256 dummyVariable;

    constructor() {}

    function getVectorX(uint16 _angle, int256 _power)
        public
        pure
        returns (int256)
    {
        return _power * Trigonometry.cos(_angle);
    }

    function getVectorY(uint16 _angle, int256 _power)
        public
        pure
        returns (int256)
    {
        return _power * Trigonometry.sin(_angle);
    }

    function collisionDetection(
        uint16 _angle,
        int256 _power,
        int256 startingX,
        int256 startingY,
        int256 gravity
    ) public {
        int256 x = startingX;
        int256 y = startingY;
        int256 Vx = getVectorX(_angle, _power) / 5000;
        int256 Vy = getVectorY(_angle, _power) / 5000;
        pathLength = 0;

        for (uint256 i = 0; i < 1024; i++) {
            if (x < 0 || x >= 1024 || y < 0) return;
            // pathX[i] = x;
            // pathY[i] = y;
            pathLength++;
            x += Vx;
            y += Vy;
            Vy -= 1;
        }
    }

    // function getPath() public view returns (int256[] memory, int256[] memory) {
    //     int256[] memory resultX = new int256[](pathLength);
    //     int256[] memory resultY = new int256[](pathLength);
    //     for (uint256 i = 0; i < pathLength; i++) {
    //         resultX[i] = pathX[i];
    //         resultY[i] = pathY[i];
    //     }
    //     return (resultX, resultY);
    // }
}
