// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/access/IAccessControlEnumerable.sol";
import "@openzeppelin/contracts/token/ERC1155/IERC1155.sol";

interface IMedals is IERC1155, IAccessControlEnumerable {
    function awardMedal(address _destination, uint256 _tokenId) external;
}
