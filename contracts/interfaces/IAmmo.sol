// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/access/IAccessControlEnumerable.sol";
import "@openzeppelin/contracts/token/ERC1155/IERC1155.sol";

struct AmmoType {
    bool developed;
    string name;
    uint256 damage;
    uint256 spread;
    uint256 weight;
}

abstract contract IAmmo is IERC1155, IAccessControlEnumerable {
    mapping(uint256 => AmmoType) public ammoTypes;
    uint256 public ammoTypeCounter;

    function mint(
        address _to,
        uint256 _id,
        uint256 _amount
    ) external virtual;

    function mintBatch(
        address _to,
        uint256[] calldata _ids,
        uint256[] calldata _amounts
    ) external virtual;

    function shoot(
        address _from,
        uint256 _id,
        uint256 _amount
    ) external virtual;
}
