// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;

abstract contract IMetadata {
    mapping(address => mapping(uint256 => mapping(string => uint256)))
        public metadata;
    mapping(address => string[]) public metadataFieldsByContract;
    mapping(address => mapping(string => bool))
        public metadataFieldsApprovedByContract;

    function updateMetadataField(
        address _contract,
        uint256 _tokenId,
        string calldata _field,
        uint256 _value
    ) external virtual;
}
