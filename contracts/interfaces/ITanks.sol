// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/access/IAccessControlEnumerable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/IERC721Enumerable.sol";

struct TankType {
    string name;
    string description;
    uint256 maxHealth;
    uint256 maxGas;
    uint256 accuracy;
    bool developed;
}

abstract contract ITanks is IAccessControlEnumerable, IERC721Enumerable {
    mapping(uint256 => TankType) public tankTypes;
    mapping(uint256 => uint256) public tankTypePerTokenId;
    uint256 public tankTypeCounter;

    function mint(address _destination, uint256 _tankType) external virtual;

    function destroyTank(uint256 _tankId) external virtual;
}
