// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;

abstract contract ITerrain {
    mapping(int256 => int256) public terrain;
    int256 public width;
    int256 public gravity;
}
