// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/access/AccessControlEnumerable.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "./interfaces/IAmmo.sol";
import "./interfaces/IScrap.sol";
import "./Tag.sol";

/// @title Ammo Factory
/// @author Atlas C.O.R.P.
contract AmmoFactory is AccessControlEnumerable {
    struct ProductionDetails {
        bool active;
        uint256 scrapToAmmo;
    }

    using SafeERC20 for IScrap;

    bytes32 public constant FACTORY_MANAGER = keccak256("FACTORY_MANAGER");

    address public ammoContract;
    address public scrapContract;
    bool public businessHours;

    mapping(uint256 => ProductionDetails) public productionDetails;

    constructor(address _ammoContract, address _scrapContract) {
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);

        ammoContract = _ammoContract;
        scrapContract = _scrapContract;
        businessHours = false;
    }

    /// @param _ammoType is the collection id
    /// @param _amount is the amount of ammo
    function pressAmmo(uint256 _ammoType, uint256 _amount) external {
        require(businessHours, "pressAmmo: ammo factory closed");
        require(
            productionDetails[_ammoType].active,
            "pressAmmo: ammo not currently in production"
        );

        IScrap(scrapContract).safeTransferFrom(
            msg.sender,
            address(this),
            _amount * productionDetails[_ammoType].scrapToAmmo
        );

        // burn some?

        IAmmo(ammoContract).mint(msg.sender, _ammoType, _amount);
    }

    /// @param _ammoType is the collection id
    /// @param _productionDetails setting the bool and scrap amount
    function setProductionDetails(
        uint256 _ammoType,
        ProductionDetails calldata _productionDetails
    ) external onlyRole(FACTORY_MANAGER) {
        productionDetails[_ammoType] = _productionDetails;
    }

    /// @param _ammoContract is the address of the ammo contract
    function setAmmoContract(address _ammoContract)
        external
        onlyRole(FACTORY_MANAGER)
    {
        ammoContract = _ammoContract;
    }

    /// @param _scrapContract is the address of the scrap contract
    function setScrapContract(address _scrapContract)
        external
        onlyRole(FACTORY_MANAGER)
    {
        scrapContract = _scrapContract;
    }

    function openFactory() external onlyRole(FACTORY_MANAGER) {
        businessHours = true;
    }

    function closeFactory() external onlyRole(FACTORY_MANAGER) {
        businessHours = false;
    }

    function withdrawScrap() external onlyRole(FACTORY_MANAGER) {
        IScrap(scrapContract).safeTransfer(
            msg.sender,
            IERC20(scrapContract).balanceOf(address(this))
        );
    }
}
