// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "./interfaces/IScrap.sol";

/// @title Soldier Name
/// @author Atlas C.O.R.P.
contract SoldierName is Ownable {
    using SafeERC20 for IScrap;

    address public scrapContract;

    uint256 public MINIMUM_NAME_LENGTH;
    uint256 public NAME_CHANGE_COOLDOWN;
    uint256 public SCRAP_PRICE;

    constructor(
        address _scrapContract,
        uint256 _minimumNameLength,
        uint256 _nameChangeCooldown,
        uint256 _scrapPrice
    ) {
        scrapContract = _scrapContract;
        MINIMUM_NAME_LENGTH = _minimumNameLength;
        NAME_CHANGE_COOLDOWN = _nameChangeCooldown;
        SCRAP_PRICE = _scrapPrice;
    }

    mapping(address => string) public soldierNames;
    mapping(string => bool) public nameUsed;
    mapping(address => uint256) public timeOfLastChange;

    /// @param _name is the name caller is setting for their profile name
    function setName(string calldata _name) external {
        require(
            bytes(soldierNames[msg.sender]).length == 0,
            "setName: name already set"
        );
        require(
            bytes(_name).length >= MINIMUM_NAME_LENGTH,
            "setName: name not long enough"
        );
        require(!nameUsed[_name], "setName: name already in use");
        soldierNames[msg.sender] = _name;
        nameUsed[_name] = true;
        timeOfLastChange[msg.sender] = block.timestamp;
    }

    /// @param _name is the name caller is setting for their profile name
    function updateName(string calldata _name) external {
        require(
            timeOfLastChange[msg.sender] + NAME_CHANGE_COOLDOWN <=
                block.timestamp,
            "updateName: caller must wait for cooldown period"
        );
        require(
            bytes(soldierNames[msg.sender]).length != 0,
            "updateName: name already set"
        );

        require(
            bytes(_name).length >= MINIMUM_NAME_LENGTH,
            "updateName: name not long enough"
        );

        require(!nameUsed[_name], "updateName: name already in use");

        delete nameUsed[soldierNames[msg.sender]];
        soldierNames[msg.sender] = _name;
        nameUsed[_name] = true;
        timeOfLastChange[msg.sender] = block.timestamp;
    }

    /// @param _name is the name caller is setting for their profile name
    function updateNameNow(string calldata _name) external {
        require(
            bytes(soldierNames[msg.sender]).length != 0,
            "updateNameNow: name already set"
        );

        require(!nameUsed[_name], "updateNameNow: name already in use");

        require(
            bytes(_name).length >= MINIMUM_NAME_LENGTH,
            "updateNameNow: name not long enough"
        );

        IScrap(scrapContract).safeTransferFrom(
            msg.sender,
            address(this),
            SCRAP_PRICE
        );

        delete nameUsed[soldierNames[msg.sender]];
        soldierNames[msg.sender] = _name;
        nameUsed[_name] = true;
        timeOfLastChange[msg.sender] = block.timestamp;
    }

    /// @param _minimumNameLength is the number of letters
    function setMinimumNameLength(uint256 _minimumNameLength)
        external
        onlyOwner
    {
        MINIMUM_NAME_LENGTH = _minimumNameLength;
    }

    /// @param _nameChangeCooldown is the number in seconds for cooldown after name change
    function setNameChangeCooldown(uint256 _nameChangeCooldown)
        external
        onlyOwner
    {
        NAME_CHANGE_COOLDOWN = _nameChangeCooldown;
    }

    /// @param _scrapPrice new price of scrap
    function setScrapPrice(uint256 _scrapPrice) external onlyOwner {
        SCRAP_PRICE = _scrapPrice;
    }

    /// @param _destination is the address owner wants to withdraw to
    /// @param _amount is the amount owner wants to withdraw
    function withdraw(address _destination, uint256 _amount)
        external
        onlyOwner
    {
        IScrap(scrapContract).safeTransfer(_destination, _amount);
    }
}
