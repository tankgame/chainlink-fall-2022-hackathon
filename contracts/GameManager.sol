// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;

struct GameData {
    Player home;
    Player away;
    GameState gameState;
    Shot shot;
    uint256 timeOfLastAction;
}

struct Shot {
    int256 power;
    uint16 angle;
    uint256 ammoType;
    int256 accuracy;
}

struct Player {
    address id;
    uint256 tankNumber;
    int256 location;
    uint256 joinTime;
}

struct Position {
    int256 x;
    int256 y;
}

enum GameState {
    OFF,
    CREATED,
    GETTING_INITIAL_VRF,
    HOME,
    AWAY,
    GETTING_HOME_VRF,
    GETTING_AWAY_VRF,
    GAME_OVER,
    ABANDONED
}

contract GameManager {
    event GameCreated(uint256 indexed gameId);
    event GameJoined(uint256 indexed gameId);

    mapping(uint256 => GameData) public gameData;

    uint256 public gameCounter;
    bool public active;

    constructor() {
        active = true;
    }

    function createGame(uint256 _tankNumber) public virtual {
        gameCounter++;

        gameData[gameCounter].home.id = msg.sender;
        gameData[gameCounter].home.tankNumber = _tankNumber;
        gameData[gameCounter].home.joinTime = block.timestamp;
        gameData[gameCounter].gameState = GameState.CREATED;
        emit GameCreated(gameCounter);
    }

    function joinGame(uint256 _gameId, uint256 _tankNumber) public virtual {
        require(
            gameData[_gameId].home.id != msg.sender,
            "joinGame: caller cannot be the same as the home player"
        );
        require(
            gameData[_gameId].gameState == GameState.CREATED,
            "joinGame: game must be in created state"
        );

        gameData[_gameId].away.id = msg.sender;
        gameData[_gameId].away.tankNumber = _tankNumber;
        gameData[_gameId].away.joinTime = block.timestamp;

        emit GameJoined(_gameId);
    }

    function setActive(bool _active) public virtual {
        active = _active;
    }
}
