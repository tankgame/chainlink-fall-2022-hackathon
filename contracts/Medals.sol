// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/access/AccessControlEnumerable.sol";
import "@openzeppelin/contracts/token/ERC1155/extensions/ERC1155Supply.sol";
import "@openzeppelin/contracts/token/ERC1155/extensions/ERC1155URIStorage.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "./Tag.sol";

contract Medals is
    ERC1155Supply,
    ERC1155URIStorage,
    AccessControlEnumerable,
    Ownable
{
    error SoulBoundToken();

    bytes32 public constant MEDAL_AWARDER = keccak256("MEDAL_AWARDER");

    uint256 public constant UPGRADE_THRESHOLD = 3;

    event LevelUpgraded();

    constructor(string memory _tokenURI) ERC1155(_tokenURI) {
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
    }

    function awardMedal(address _destination, uint256 _tokenId)
        external
        onlyRole(MEDAL_AWARDER)
    {
        _mint(_destination, _tokenId, 1, "");
    }

    function levelUp(uint256 currentLevel) external {
        _burn(msg.sender, currentLevel, UPGRADE_THRESHOLD);
        _mint(msg.sender, currentLevel + 1, 1, "");
        emit LevelUpgraded();
    }

    /// @dev caller needs AMMO_DEVELOPER
    /// @notice to change the IPFS URI link
    function setURI(uint256 _tokenId, string calldata _tokenURI)
        external
        onlyOwner
    {
        _setURI(_tokenId, _tokenURI);
    }

    /// @dev caller needs AMMO_DEVELOPER
    /// @notice to change the IPFS URI link
    /// @param _baseURI the new link
    function setBaseURI(string calldata _baseURI) external onlyOwner {
        _setBaseURI(_baseURI);
    }

    function uri(uint256 tokenId)
        public
        view
        virtual
        override(ERC1155, ERC1155URIStorage)
        returns (string memory)
    {
        return ERC1155URIStorage.uri(tokenId);
    }

    function safeTransferFrom(
        address,
        address,
        uint256,
        uint256,
        bytes memory
    ) public virtual override {
        revert SoulBoundToken();
    }

    function safeBatchTransferFrom(
        address,
        address,
        uint256[] memory,
        uint256[] memory,
        bytes memory
    ) public virtual override {
        revert SoulBoundToken();
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(ERC1155, AccessControlEnumerable)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }

    function _beforeTokenTransfer(
        address operator,
        address from,
        address to,
        uint256[] memory ids,
        uint256[] memory amounts,
        bytes memory data
    ) internal override(ERC1155, ERC1155Supply) {
        super._beforeTokenTransfer(operator, from, to, ids, amounts, data);
    }
}
