// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;

import "./libraries/Trigonometry.sol";
import "@openzeppelin/contracts/access/AccessControlEnumerable.sol";
import "@openzeppelin/contracts/utils/math/SignedMath.sol";
import "./interfaces/IMetadata.sol";
import "./VRFManager.sol";
import "./GameManager.sol";
import "./interfaces/ITerrain.sol";
import "./interfaces/ITanks.sol";
import "./interfaces/IAmmo.sol";
import "./interfaces/IScrap.sol";
import "./interfaces/IMedals.sol";
import "./Tag.sol";

/// @title Battle
/// @author Atlas C.O.R.P.

contract Battle is VRFManager, GameManager, AccessControlEnumerable {
    using SignedMath for int256;

    uint32 public constant NUMBER_OF_INITIAL_RANDOM_WORDS = 4;
    uint32 public constant NUMBER_OF_RANDOM_WORDS_FOR_FIRE = 5;
    uint256 public constant LEFT_WINDOW_OFFSET = 6;
    uint256 public constant RIGHT_WINDOW_OFFSET = 517;
    uint256 public constant WINDOW_WIDTH = 500;
    int256 public constant MAP_SCALE = 1000000; //1M
    int256 public constant MAX_POWER = 1000000; //1M
    uint256 public constant WINNER_SCRAP = 70;
    uint256 public constant LOSER_SCRAP = 30;
    uint256 public constant MEDAL_FOR_WINNER = 1;
    int256 public constant TRIG_COEFFICIENT = 32767;
    int256 public VEL_COEFFICIENT = 20;
    uint256 public constant MAX_TIME_STEPS = 4096;
    int256 public constant TANK_HEIGHT = 10000000; //10M
    int256 public constant ACCURACY_VARIANCE = 200;
    uint256 public constant MAX_MOVEMENT = 100;
    uint256 public constant GAS_PER_SPACE_MOVED = 1;
    uint256 public constant TIMEOUT = 3600;

    address public terrainContract;
    address public tankContract;
    address public ammoContract;
    address public scrapContract;
    address public medalsContract;
    address public metadataContract;

    bytes32 public constant GAME_MANAGER = keccak256("GAME_MANAGER");

    mapping(uint256 => uint256) public gameByRequestId;

    event GameSet(uint256 indexed gameId);
    event FireRequested(uint256 indexed gameId);
    event FireFulfilled(uint256 indexed gameId);
    event Move(uint256 indexed gameId);
    event GameOver(uint256 indexed gameId);
    event GameAbandoned(uint256 indexed gameId);
    event Hit(uint256 indexed gameId);
    event Miss(uint256 indexed gameId);

    constructor(
        address _terrainContract,
        address _tankContract,
        address _ammoContract,
        address _scrapContarct,
        address _medalsContarct,
        address _metadataContract,
        VRFManagerConstructorArgs memory _VRFManagerConstructorArgs
    ) VRFManager(_VRFManagerConstructorArgs) {
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _grantRole(GAME_MANAGER, msg.sender);
        terrainContract = _terrainContract;
        tankContract = _tankContract;
        ammoContract = _ammoContract;
        scrapContract = _scrapContarct;
        medalsContract = _medalsContarct;
        metadataContract = _metadataContract;
    }

    /// @param _gameId is the number of the game
    /// @param _ammoType is the collection id
    /// @param _angle is the number of the angle
    /// @param _power is the number of the power
    function fire(
        uint256 _gameId,
        uint256 _ammoType,
        uint16 _angle,
        int256 _power
    ) public {
        if (gameData[_gameId].gameState == GameState.HOME) {
            if (gameData[_gameId].home.id == msg.sender) {
                gameData[_gameId].gameState = GameState.GETTING_HOME_VRF;
            } else if (
                (block.timestamp >=
                    gameData[_gameId].timeOfLastAction + TIMEOUT) &&
                gameData[_gameId].away.id == msg.sender
            ) {
                gameData[_gameId].gameState = GameState.GETTING_AWAY_VRF;
            } else {
                revert();
            }
        } else if (gameData[_gameId].gameState == GameState.AWAY) {
            if (gameData[_gameId].away.id == msg.sender) {
                gameData[_gameId].gameState = GameState.GETTING_AWAY_VRF;
            } else if (
                (block.timestamp >=
                    gameData[_gameId].timeOfLastAction + TIMEOUT) &&
                gameData[_gameId].home.id == msg.sender
            ) {
                gameData[_gameId].gameState = GameState.GETTING_HOME_VRF;
            } else {
                revert();
            }
        }

        require(
            _angle >= 0 && _angle < Trigonometry.ANGLES_IN_CYCLE,
            "fire: angle out of bounds"
        );
        require(
            _power > 0 && _power <= MAX_POWER,
            "fire: power must be greater than zero and no greater than max power"
        );

        IAmmo(ammoContract).shoot(msg.sender, _ammoType, 1);

        // request random words
        uint256 requestId = requestRandomWords(NUMBER_OF_RANDOM_WORDS_FOR_FIRE);
        gameByRequestId[requestId] = _gameId;

        gameData[_gameId].shot.power = _power;
        gameData[_gameId].shot.angle = _angle;
        gameData[_gameId].shot.ammoType = _ammoType;
        gameData[_gameId].shot.accuracy = 0;
        gameData[_gameId].timeOfLastAction = block.timestamp;

        emit FireRequested(_gameId);
    }

    function moveTank(uint256 gameId, int256 _numberOfSpaces) external {
        uint256 tankNumber;
        int256 tankPosition;
        int256 newTankPosition;

        if (gameData[gameId].gameState == GameState.HOME) {
            if (msg.sender == gameData[gameId].home.id) {
                tankNumber = gameData[gameId].home.tankNumber;
                tankPosition = gameData[gameId].home.location;
                newTankPosition = tankPosition + _numberOfSpaces;

                require(
                    newTankPosition >= 0 &&
                        newTankPosition <= ITerrain(terrainContract).width() - 1
                );

                gameData[gameId].home.location = newTankPosition;
                gameData[gameId].gameState = GameState.AWAY;
            } else if (
                (block.timestamp >=
                    gameData[gameId].timeOfLastAction + TIMEOUT) &&
                gameData[gameId].away.id == msg.sender
            ) {
                tankNumber = gameData[gameId].away.tankNumber;
                tankPosition = gameData[gameId].away.location;
                newTankPosition = tankPosition + _numberOfSpaces;

                require(
                    newTankPosition >= 0 &&
                        newTankPosition <= ITerrain(terrainContract).width() - 1
                );

                gameData[gameId].away.location = newTankPosition;
            } else {
                revert();
            }
        } else if (gameData[gameId].gameState == GameState.AWAY) {
            if (msg.sender == gameData[gameId].away.id) {
                tankNumber = gameData[gameId].away.tankNumber;
                tankPosition = gameData[gameId].away.location;
                newTankPosition = tankPosition + _numberOfSpaces;

                require(
                    newTankPosition >= 0 &&
                        newTankPosition <= ITerrain(terrainContract).width() - 1
                );

                gameData[gameId].away.location = newTankPosition;
                gameData[gameId].gameState = GameState.HOME;
            } else if (
                (block.timestamp >=
                    gameData[gameId].timeOfLastAction + TIMEOUT) &&
                gameData[gameId].home.id == msg.sender
            ) {
                tankNumber = gameData[gameId].home.tankNumber;
                tankPosition = gameData[gameId].home.location;
                newTankPosition = tankPosition + _numberOfSpaces;

                require(
                    newTankPosition >= 0 &&
                        newTankPosition <= ITerrain(terrainContract).width() - 1
                );

                gameData[gameId].home.location = newTankPosition;
            } else {
                revert();
            }
        } else {
            revert();
        }

        require(
            _numberOfSpaces.abs() <= MAX_MOVEMENT,
            "moveTank: cannot move more than max movement"
        );

        uint256 gasInTank = IMetadata(metadataContract).metadata(
            tankContract,
            tankNumber,
            "gas"
        );
        uint256 gasSpent = _numberOfSpaces.abs() * GAS_PER_SPACE_MOVED;

        require(
            gasInTank >= gasSpent,
            "moveTank: caller's tank does not have sufficient gas"
        );

        IMetadata(metadataContract).updateMetadataField(
            tankContract,
            tankNumber,
            "gas",
            gasInTank - gasSpent
        );

        gameData[gameId].timeOfLastAction = block.timestamp;

        emit Move(gameId);
    }

    function playerCanSubmit(uint256 _gameId, address _player)
        public
        view
        returns (bool)
    {
        return
            (gameData[_gameId].gameState == GameState.HOME &&
                (gameData[_gameId].home.id == _player ||
                    ((block.timestamp >=
                        gameData[_gameId].timeOfLastAction + TIMEOUT) &&
                        gameData[_gameId].away.id == _player))) ||
            (gameData[_gameId].gameState == GameState.AWAY &&
                (gameData[_gameId].away.id == _player ||
                    ((block.timestamp >=
                        gameData[_gameId].timeOfLastAction + TIMEOUT) &&
                        gameData[_gameId].home.id == _player)));
    }

    function numSpacesPlayersCanMove(uint256 _gameId)
        public
        view
        returns (
            int256 homeLeft,
            int256 homeRight,
            int256 awayLeft,
            int256 awayRight
        )
    {
        uint256 homeGasInTank = IMetadata(metadataContract).metadata(
            tankContract,
            gameData[_gameId].home.tankNumber,
            "gas"
        );

        uint256 awayGasInTank = IMetadata(metadataContract).metadata(
            tankContract,
            gameData[_gameId].away.tankNumber,
            "gas"
        );

        int256 homeMaxMovementByGas = homeGasInTank >=
            MAX_MOVEMENT * GAS_PER_SPACE_MOVED
            ? int256(MAX_MOVEMENT)
            : int256(homeGasInTank / GAS_PER_SPACE_MOVED);

        int256 awayMaxMovementByGas = awayGasInTank >=
            MAX_MOVEMENT * GAS_PER_SPACE_MOVED
            ? int256(MAX_MOVEMENT)
            : int256(awayGasInTank / GAS_PER_SPACE_MOVED);

        homeLeft = gameData[_gameId].home.location - homeMaxMovementByGas < 0
            ? gameData[_gameId].home.location
            : homeMaxMovementByGas;

        homeRight = gameData[_gameId].home.location + homeMaxMovementByGas >
            ITerrain(terrainContract).width() - 1
            ? ITerrain(terrainContract).width() -
                1 -
                gameData[_gameId].home.location
            : homeMaxMovementByGas;
        awayLeft = gameData[_gameId].away.location - awayMaxMovementByGas < 0
            ? gameData[_gameId].away.location
            : awayMaxMovementByGas;
        awayRight = gameData[_gameId].away.location + awayMaxMovementByGas >
            ITerrain(terrainContract).width() - 1
            ? ITerrain(terrainContract).width() -
                1 -
                gameData[_gameId].away.location
            : awayMaxMovementByGas;
    }

    /// @param _tankNumber is the tank id
    function createGame(uint256 _tankNumber) public override {
        ITanks(tankContract).transferFrom(
            msg.sender,
            address(this),
            _tankNumber
        );
        super.createGame(_tankNumber);
    }

    /// @param _gameId is the game number
    /// @param _tankNumber is the tank id
    function joinGame(uint256 _gameId, uint256 _tankNumber) public override {
        ITanks(tankContract).transferFrom(
            msg.sender,
            address(this),
            _tankNumber
        );
        super.joinGame(_gameId, _tankNumber);
        initializeGame(_gameId);
    }

    function withdrawFromGame(uint256 _gameId) external {
        require(
            gameData[_gameId].gameState == GameState.CREATED,
            "withdrawFromGame: caller cannot withdraw in current state"
        );
        require(
            gameData[_gameId].home.id == msg.sender,
            "withdrawFromGame: caller must be home player to initiate withdraw"
        );
        ITanks(tankContract).safeTransferFrom(
            address(this),
            msg.sender,
            gameData[_gameId].home.tankNumber
        );
        gameData[_gameId].gameState = GameState.ABANDONED;
        emit GameAbandoned(_gameId);
    }

    function initializeGame(uint256 _gameId) private {
        gameData[_gameId].gameState = GameState.GETTING_INITIAL_VRF;
        gameData[_gameId].timeOfLastAction = block.timestamp;
        uint256 requestId = requestRandomWords(NUMBER_OF_INITIAL_RANDOM_WORDS);
        gameByRequestId[requestId] = _gameId;
    }

    function requestRandomWords(uint32 _numWords) private returns (uint256) {
        return
            COORDINATOR.requestRandomWords(
                keyHash,
                subscriptionId,
                requestConfirmations,
                callbackGasLimit,
                _numWords
            );
    }

    function fulfillRandomWords(uint256 requestId, uint256[] memory randomWords)
        internal
        override
    {
        uint256 gameId = gameByRequestId[requestId];

        if (gameData[gameId].gameState == GameState.GETTING_INITIAL_VRF) {
            uint256 homeAssignment = randomWords[0] % 2;
            uint256 whoGoesFirst = randomWords[3] % 2;
            // left window 6-505
            // right window 517-1016
            if (homeAssignment == 0) {
                gameData[gameId].home.location = int256(
                    (randomWords[1] % WINDOW_WIDTH) + LEFT_WINDOW_OFFSET
                );
                gameData[gameId].away.location = int256(
                    (randomWords[2] % WINDOW_WIDTH) + RIGHT_WINDOW_OFFSET
                );
            } else {
                gameData[gameId].away.location = int256(
                    (randomWords[1] % WINDOW_WIDTH) + LEFT_WINDOW_OFFSET
                );
                gameData[gameId].home.location = int256(
                    (randomWords[2] % WINDOW_WIDTH) + RIGHT_WINDOW_OFFSET
                );
            }

            if (whoGoesFirst == 0) {
                gameData[gameId].gameState = GameState.HOME;
            } else {
                gameData[gameId].gameState = GameState.AWAY;
            }

            emit GameSet(gameId);
        } else if (gameData[gameId].gameState == GameState.GETTING_AWAY_VRF) {
            emit FireFulfilled(gameId);

            (uint16 adjustedAngle, int256 accuracy) = getAdjustedAngle(
                randomWords,
                gameData[gameId].shot.angle
            );

            gameData[gameId].shot.accuracy = accuracy;
            if (
                detectCollision(
                    Position({
                        x: gameData[gameId].away.location * MAP_SCALE,
                        y: ITerrain(terrainContract).terrain(
                            gameData[gameId].away.location
                        )
                    }),
                    Position({
                        x: gameData[gameId].home.location * MAP_SCALE,
                        y: ITerrain(terrainContract).terrain(
                            gameData[gameId].home.location
                        )
                    }),
                    adjustedAngle,
                    gameData[gameId].shot.power,
                    ITerrain(terrainContract).gravity()
                )
            ) {
                emit Hit(gameId);

                (, , uint256 damage, , ) = IAmmo(ammoContract).ammoTypes(
                    gameData[gameId].shot.ammoType
                );

                uint256 health = IMetadata(metadataContract).metadata(
                    tankContract,
                    gameData[gameId].home.tankNumber,
                    "health"
                );

                if (health > damage) {
                    IMetadata(metadataContract).updateMetadataField(
                        tankContract,
                        gameData[gameId].home.tankNumber,
                        "health",
                        health - damage
                    );
                    gameData[gameId].gameState = GameState.HOME;
                } else {
                    IMetadata(metadataContract).updateMetadataField(
                        tankContract,
                        gameData[gameId].home.tankNumber,
                        "health",
                        0
                    );
                    gameData[gameId].gameState = GameState.GAME_OVER;

                    emit GameOver(gameId);

                    ITanks(tankContract).destroyTank(
                        gameData[gameId].home.tankNumber
                    );

                    ITanks(tankContract).safeTransferFrom(
                        address(this),
                        gameData[gameId].away.id,
                        gameData[gameId].away.tankNumber
                    );

                    IScrap(scrapContract).collect(
                        gameData[gameId].away.id,
                        WINNER_SCRAP
                    );

                    IScrap(scrapContract).collect(
                        gameData[gameId].home.id,
                        LOSER_SCRAP
                    );

                    IMedals(medalsContract).awardMedal(
                        gameData[gameId].away.id,
                        MEDAL_FOR_WINNER
                    );
                }
            } else {
                gameData[gameId].gameState = GameState.HOME;
                emit Miss(gameId);
            }
        } else if (gameData[gameId].gameState == GameState.GETTING_HOME_VRF) {
            emit FireFulfilled(gameId);

            (uint16 adjustedAngle, int256 accuracy) = getAdjustedAngle(
                randomWords,
                gameData[gameId].shot.angle
            );

            gameData[gameId].shot.accuracy = accuracy;

            if (
                detectCollision(
                    Position({
                        x: gameData[gameId].home.location * MAP_SCALE,
                        y: ITerrain(terrainContract).terrain(
                            gameData[gameId].home.location
                        )
                    }),
                    Position({
                        x: gameData[gameId].away.location * MAP_SCALE,
                        y: ITerrain(terrainContract).terrain(
                            gameData[gameId].away.location
                        )
                    }),
                    adjustedAngle,
                    gameData[gameId].shot.power,
                    ITerrain(terrainContract).gravity()
                )
            ) {
                emit Hit(gameId);

                (, , uint256 damage, , ) = IAmmo(ammoContract).ammoTypes(
                    gameData[gameId].shot.ammoType
                );

                uint256 health = IMetadata(metadataContract).metadata(
                    tankContract,
                    gameData[gameId].away.tankNumber,
                    "health"
                );

                if (health > damage) {
                    IMetadata(metadataContract).updateMetadataField(
                        tankContract,
                        gameData[gameId].away.tankNumber,
                        "health",
                        health - damage
                    );
                    gameData[gameId].gameState = GameState.AWAY;
                } else {
                    IMetadata(metadataContract).updateMetadataField(
                        tankContract,
                        gameData[gameId].away.tankNumber,
                        "health",
                        0
                    );
                    gameData[gameId].gameState = GameState.GAME_OVER;

                    emit GameOver(gameId);

                    ITanks(tankContract).destroyTank(
                        gameData[gameId].away.tankNumber
                    );

                    ITanks(tankContract).safeTransferFrom(
                        address(this),
                        gameData[gameId].home.id,
                        gameData[gameId].home.tankNumber
                    );

                    IScrap(scrapContract).collect(
                        gameData[gameId].home.id,
                        WINNER_SCRAP
                    );

                    IScrap(scrapContract).collect(
                        gameData[gameId].away.id,
                        LOSER_SCRAP
                    );

                    IMedals(medalsContract).awardMedal(
                        gameData[gameId].home.id,
                        MEDAL_FOR_WINNER
                    );
                }
            } else {
                gameData[gameId].gameState = GameState.AWAY;
                emit Miss(gameId);
            }
        }
    }

    function getAdjustedAngle(uint256[] memory _randomWords, uint16 angle)
        public
        pure
        returns (uint16, int256)
    {
        int256 accuracy;
        for (uint256 i; i < 5; i++) {
            unchecked {
                accuracy += int256(_randomWords[i] % 3) - 1;
            }
        }
        accuracy = (accuracy * 16384) / ACCURACY_VARIANCE;
        unchecked {
            if (accuracy >= 0) {
                return (
                    uint16(
                        Trigonometry.bits(
                            angle + uint16(uint256(accuracy)),
                            14,
                            0
                        )
                    ),
                    accuracy
                );
            } else {
                return (
                    uint16(
                        Trigonometry.bits(
                            angle - uint16(uint256(accuracy * -1)),
                            14,
                            0
                        )
                    ),
                    accuracy
                );
            }
        }
    }

    function detectCollision(
        Position memory _positionFire,
        Position memory _positionTarget,
        uint16 _angle,
        int256 _power,
        int256 _gravity
    ) public view returns (bool) {
        int256 x = _positionFire.x;
        int256 y = _positionFire.y + TANK_HEIGHT;
        int256 positionTargetAdjustedHeight = _positionTarget.y + TANK_HEIGHT;

        int256 tempX;
        int256 tempY;

        int256 Vx = (_power * Trigonometry.cos(_angle) * VEL_COEFFICIENT) /
            TRIG_COEFFICIENT;
        int256 Vy = (_power * Trigonometry.sin(_angle) * VEL_COEFFICIENT) /
            TRIG_COEFFICIENT;

        int256 maxX = ITerrain(terrainContract).width() * MAP_SCALE;

        for (uint256 i = 0; i < MAX_TIME_STEPS; i++) {
            tempX = x;
            tempY = y;

            x += Vx;
            y += Vy;
            Vy -= _gravity;

            if (x < 0 || x > maxX) return false;
            else if (betweenXValues(_positionTarget.x, x, tempX)) {
                if (betweenYValues(positionTargetAdjustedHeight, y, tempY)) {
                    return true;
                }
                return false;
            } else if (y <= ITerrain(terrainContract).terrain(x / MAP_SCALE)) {
                return false;
            }
        }
        return false;
    }

    function betweenXValues(
        int256 position,
        int256 x,
        int256 tempX
    ) public pure returns (bool) {
        return
            (position >= tempX && position <= x) ||
            (position <= tempX && position >= x);
    }

    function betweenYValues(
        int256 position,
        int256 y,
        int256 tempY
    ) public pure returns (bool) {
        return
            (position >= tempY && position <= y) ||
            (position <= tempY && position >= y);
    }

    /// @dev x position values must be scaled
    function drawPathX(
        Position memory _positionFire,
        Position memory _positionTarget,
        Shot calldata _shot,
        int256 _gravity
    ) public view returns (int256[] memory, uint256) {
        int256 Vx = (_shot.power *
            Trigonometry.cos(_shot.angle) *
            VEL_COEFFICIENT) / TRIG_COEFFICIENT;
        int256 Vy = (_shot.power *
            Trigonometry.sin(_shot.angle) *
            VEL_COEFFICIENT) / TRIG_COEFFICIENT;

        int256 tempX;
        int256 tempY;
        _positionFire.y += TANK_HEIGHT;

        int256 maxX = ITerrain(terrainContract).width() * MAP_SCALE;

        int256[] memory pathX = new int256[](MAX_TIME_STEPS);

        uint256 i;
        for (; i < MAX_TIME_STEPS; ) {
            unchecked {
                tempX = _positionFire.x;
                tempY = _positionFire.y;

                pathX[i] = _positionFire.x;
                _positionFire.x += Vx;
                _positionFire.y += Vy;
                Vy -= _gravity;

                if (_positionFire.x < 0 || _positionFire.x >= maxX) break;
                else if (
                    (_positionTarget.x >= tempX &&
                        _positionTarget.x <= _positionFire.x) ||
                    (_positionTarget.x <= tempX &&
                        _positionTarget.x >= _positionFire.x)
                ) {
                    if (
                        (_positionTarget.y + TANK_HEIGHT >= tempY &&
                            _positionTarget.y + TANK_HEIGHT <=
                            _positionFire.y) ||
                        (_positionTarget.y + TANK_HEIGHT <= tempY &&
                            _positionTarget.y + TANK_HEIGHT >= _positionFire.y)
                    ) {
                        i++;
                        pathX[i] = _positionFire.x;
                        break;
                    }
                }
                if (
                    _positionFire.y <=
                    ITerrain(terrainContract).terrain(
                        _positionFire.x / MAP_SCALE
                    )
                ) {
                    i++;
                    pathX[i] = _positionFire.x;
                    break;
                }
                i++;
            }
        }
        return (pathX, i);
    }

    function drawPathY(
        Position memory _positionFire,
        Position memory _positionTarget,
        Shot calldata _shot,
        int256 _gravity
    ) public view returns (int256[] memory, uint256) {
        int256 Vx = (_shot.power *
            Trigonometry.cos(_shot.angle) *
            VEL_COEFFICIENT) / TRIG_COEFFICIENT;
        int256 Vy = (_shot.power *
            Trigonometry.sin(_shot.angle) *
            VEL_COEFFICIENT) / TRIG_COEFFICIENT;

        int256 tempX;
        int256 tempY;
        _positionFire.y += TANK_HEIGHT;

        int256 maxX = ITerrain(terrainContract).width() * MAP_SCALE;

        int256[] memory pathY = new int256[](MAX_TIME_STEPS);

        uint256 i;
        for (; i < MAX_TIME_STEPS; ) {
            unchecked {
                tempX = _positionFire.x;
                tempY = _positionFire.y;

                pathY[i] = _positionFire.y;
                _positionFire.x += Vx;
                _positionFire.y += Vy;
                Vy -= _gravity;

                if (_positionFire.x < 0 || _positionFire.x >= maxX) break;
                else if (
                    (_positionTarget.x >= tempX &&
                        _positionTarget.x <= _positionFire.x) ||
                    (_positionTarget.x <= tempX &&
                        _positionTarget.x >= _positionFire.x)
                ) {
                    if (
                        (_positionTarget.y + TANK_HEIGHT >= tempY &&
                            _positionTarget.y + TANK_HEIGHT <=
                            _positionFire.y) ||
                        (_positionTarget.y + TANK_HEIGHT <= tempY &&
                            _positionTarget.y + TANK_HEIGHT >= _positionFire.y)
                    ) {
                        i++;
                        pathY[i] = _positionFire.y;
                        break;
                    }
                }
                if (
                    _positionFire.y <=
                    ITerrain(terrainContract).terrain(
                        _positionFire.x / MAP_SCALE
                    )
                ) {
                    i++;
                    pathY[i] = _positionFire.y;
                    break;
                }
                i++;
            }
        }
        return (pathY, i);
    }

    function cos(uint16 _angle) public pure returns (int256) {
        return Trigonometry.cos(_angle);
    }

    function sin(uint16 _angle) public pure returns (int256) {
        return Trigonometry.sin(_angle);
    }

    function setActive(bool _active) public override onlyRole(GAME_MANAGER) {
        super.setActive(_active);
    }

    /// @param _terrainContract is the address of the terrain contract
    function updateTerrainContract(address _terrainContract)
        external
        onlyRole(GAME_MANAGER)
    {
        terrainContract = _terrainContract;
    }

    /// @param _tankContract is the address of the tank contract
    function updateTankContract(address _tankContract)
        external
        onlyRole(GAME_MANAGER)
    {
        tankContract = _tankContract;
    }

    /// @param _ammoContract is the address of the ammo contract
    function updateAmmoContract(address _ammoContract)
        external
        onlyRole(GAME_MANAGER)
    {
        ammoContract = _ammoContract;
    }

    /// @param _scrapContract is the address of the scrap contract
    function updateScrapContract(address _scrapContract)
        external
        onlyRole(GAME_MANAGER)
    {
        scrapContract = _scrapContract;
    }

    /// @param _medalsContract is the address of the medals contract
    function updateMedalsContract(address _medalsContract)
        external
        onlyRole(GAME_MANAGER)
    {
        medalsContract = _medalsContract;
    }

    /// @param _metadataContract is the address of the metadata contract
    function updateMetadataContract(address _metadataContract)
        external
        onlyRole(GAME_MANAGER)
    {
        ammoContract = _metadataContract;
    }

    /// @param _subscriptionId is the Id from the Chainlink subscription manager
    function updateSubscriptionId(uint64 _subscriptionId)
        public
        override
        onlyRole(GAME_MANAGER)
    {
        super.updateSubscriptionId(_subscriptionId);
    }

    /// @param _keyHash is the keyhash from the Chainlink subscription manager
    function updateKeyHash(bytes32 _keyHash)
        public
        override
        onlyRole(GAME_MANAGER)
    {
        super.updateKeyHash(_keyHash);
    }

    function updateCallbackGasLimit(uint32 _callbackGasLimit)
        public
        override
        onlyRole(GAME_MANAGER)
    {
        super.updateCallbackGasLimit(_callbackGasLimit);
    }

    /// @param _requestConfirmations the confirmation number from the request
    function updateRequestConfirmations(uint16 _requestConfirmations)
        public
        override
        onlyRole(GAME_MANAGER)
    {
        super.updateRequestConfirmations(_requestConfirmations);
    }

    function updateGameState(uint256 _gameId, GameState _gameState)
        public
        onlyRole(GAME_MANAGER)
    {
        gameData[_gameId].gameState = _gameState;
    }

    function updateGameState(int256 _velocityCoef)
        public
        onlyRole(GAME_MANAGER)
    {
        VEL_COEFFICIENT = _velocityCoef;
    }
}
