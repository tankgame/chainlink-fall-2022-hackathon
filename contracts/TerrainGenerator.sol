// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/access/AccessControlEnumerable.sol";
import "./interfaces/ITerrain.sol";
import "./Tag.sol";

/// @title Terrain Generator
/// @author Atlas C.O.R.P.

contract TerrainGenerator is AccessControlEnumerable {
    bytes32 public constant CARTOGRAPHER = keccak256("CARTOGRAPHER");

    // mapping(uint256 => uint256) public naiveTerrain;
    mapping(uint256 => mapping(uint256 => int256)) public sampledTerrain;
    mapping(uint256 => int256) public terrain;
    uint256 public width;
    int256 public gravity;

    int256 public constant FLOAT_CONSTANT = 10000000;

    constructor(
        uint256 _seed,
        uint256 _width,
        int256 _gravity
    ) {
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        width = _width;
        gravity = _gravity;
        populateNaiveTerrain(_seed, width);
    }

    /// @param _seed is the number of seed
    /// @param _quantity is the number of quantity
    function populateNaiveTerrain(uint256 _seed, uint256 _quantity) public {
        uint256 randomNumber = uint256(keccak256(abi.encodePacked(_seed)));

        for (uint256 i = 0; i < _quantity; i++) {
            sampledTerrain[0][i] = int256(
                reduceRandomNumberToRange(randomNumber, 1, 10)
            );
            randomNumber = uint256(keccak256(abi.encodePacked(randomNumber)));
        }
    }

    function getSampledTerrain(uint256 _samplePeriod)
        external
        view
        returns (int256[] memory)
    {
        int256[] memory result = new int256[](width);
        for (uint256 i = 0; i < width; i++) {
            result[i] = sampledTerrain[_samplePeriod][i];
        }
        return result;
    }

    function getSmoothTerrain() external view returns (int256[] memory) {
        int256[] memory result = new int256[](width);
        for (uint256 i = 0; i < width; i++) {
            result[i] = terrain[i];
        }
        return result;
    }

    function populateSampledTerrain(uint256 _samplePeriod) external {
        uint256 numSampledPoints = width / _samplePeriod;
        int256 a;
        int256 b;

        for (uint256 i = 0; i < numSampledPoints; i++) {
            a = sampledTerrain[0][i * _samplePeriod];
            a = sampledTerrain[0][i * _samplePeriod];

            // b = sampledTerrain[0][((i.add(1)).mul(_samplePeriod)).mod(numSampledPoints)];
            b = sampledTerrain[0][((i + 1) * _samplePeriod) % width];

            sampledTerrain[_samplePeriod][i * _samplePeriod] =
                a *
                (FLOAT_CONSTANT);
            for (uint256 j = 0; j < _samplePeriod - 1; j++) {
                int256 mu = (int256(j + 1) * (FLOAT_CONSTANT)) /
                    int256(_samplePeriod);
                sampledTerrain[_samplePeriod][
                    (i * _samplePeriod) + j + 1
                ] = linp(a, b, mu);
            }
        }
    }

    function linp(
        int256 _a,
        int256 _b,
        int256 mu
    ) public view returns (int256) {
        int256 part1 = (FLOAT_CONSTANT - mu) * _a;
        int256 part2 = _b * mu;
        return part1 + part2;
    }

    function populateSmoothTerrain(
        uint256[] calldata _samplePeriods,
        int256[] calldata _weights,
        uint256 _startingIndex,
        uint256 _endIndex
    ) external {
        int256 weightedSum;
        for (uint256 i = _startingIndex; i < _endIndex; i++) {
            weightedSum = 0;

            for (uint256 j = 0; j < _samplePeriods.length; j++) {
                weightedSum +=
                    sampledTerrain[_samplePeriods[j]][i] *
                    _weights[j];
            }

            terrain[i] = weightedSum;
        }
    }

    function reduceRandomNumberToRange(
        uint256 _seed,
        uint256 _lower,
        uint256 _upper
    ) public pure returns (uint256) {
        return (_seed % (_upper + 1 - _lower)) + _lower;
    }

    /// @param _gravity is the new number for gravity
    function updateGravity(int256 _gravity) external onlyRole(CARTOGRAPHER) {
        gravity = _gravity;
    }
}
