// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/access/AccessControlEnumerable.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721Receiver.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/utils/math/Math.sol";
import "./interfaces/IScrap.sol";
import "./interfaces/ITanks.sol";
import "./interfaces/IMetadata.sol";
import "./Tag.sol";

contract MechanicShop is IERC721Receiver, AccessControlEnumerable {
    struct RepairTicket {
        address owner;
        uint256 arrivalTime;
        uint256 restorationRate;
        uint256 restorationPeriod;
        uint256 refillRate;
        uint256 refillPeriod;
    }

    using SafeERC20 for IScrap;
    using Math for uint256;

    bytes32 public constant SHOP_MANAGER = keccak256("SHOP_MANAGER");

    uint256 public restorationRate;
    uint256 public restorationPeriod;
    uint256 public restorationPrice;

    uint256 public refillRate;
    uint256 public refillPeriod;
    uint256 public refillPrice;

    address public tankContract;
    address public scrapContract;
    address public metadataContract;

    mapping(uint256 => RepairTicket) public repairTickets;

    event TankReceived(uint256 indexed tankId);
    event TankReturned(uint256 indexed tankId);
    event RestorationRateUpdated();
    event RestorationPeriodUpdated();
    event RestorationPriceUpdated();
    event RefillRateUpdated();
    event RefillPeriodUpdated();
    event RefillPriceUpdated();

    constructor(
        address _tankContract,
        address _scrapContract,
        address _metadataContract,
        uint256 _restorationRate,
        uint256 _restorationPeriod,
        uint256 _restorationPrice,
        uint256 _refillRate,
        uint256 _refillPeriod,
        uint256 _refillPrice
    ) {
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        tankContract = _tankContract;
        scrapContract = _scrapContract;
        metadataContract = _metadataContract;
        restorationRate = _restorationRate;
        restorationPeriod = _restorationPeriod;
        restorationPrice = _restorationPrice;
        refillRate = _refillRate;
        refillPeriod = _refillPeriod;
        refillPrice = _refillPrice;
    }

    function retrieveTank(uint256 _tankId) external {
        require(
            repairTickets[_tankId].owner == msg.sender,
            "retrieveTank: caller must be owner of tank"
        );

        uint256 elapsedTime = block.timestamp -
            repairTickets[_tankId].arrivalTime;

        uint256 elapsedPeriods = elapsedTime /
            repairTickets[_tankId].restorationPeriod;

        uint256 healthGain = elapsedPeriods *
            repairTickets[_tankId].restorationRate;

        uint256 gasGain = elapsedPeriods * repairTickets[_tankId].refillRate;

        uint256 intialHealth = IMetadata(metadataContract).metadata(
            tankContract,
            _tankId,
            "health"
        );

        uint256 intialGas = IMetadata(metadataContract).metadata(
            tankContract,
            _tankId,
            "gas"
        );

        (, , uint256 maxHealth, uint256 maxGas, , ) = ITanks(tankContract)
            .tankTypes(ITanks(tankContract).tankTypePerTokenId(_tankId));

        IMetadata(metadataContract).updateMetadataField(
            tankContract,
            _tankId,
            "health",
            maxHealth.min(intialHealth + healthGain)
        );

        IMetadata(metadataContract).updateMetadataField(
            tankContract,
            _tankId,
            "gas",
            maxGas.min(intialGas + gasGain)
        );

        ITanks(tankContract).safeTransferFrom(
            address(this),
            msg.sender,
            _tankId
        );

        emit TankReturned(_tankId);

        delete repairTickets[_tankId];
    }

    function payForRepair(uint256 _tankId, uint256 _gain) external {
        require(
            ITanks(tankContract).ownerOf(_tankId) == msg.sender,
            "payForRepair: caller must have tank in wallet"
        );

        uint256 intialHealth = IMetadata(metadataContract).metadata(
            tankContract,
            _tankId,
            "health"
        );

        (, , uint256 maxHealth, , , ) = ITanks(tankContract).tankTypes(
            ITanks(tankContract).tankTypePerTokenId(_tankId)
        );

        require(
            _gain + intialHealth <= maxHealth,
            "payForRepair: cannot exceed max health"
        );

        IScrap(scrapContract).safeTransferFrom(
            msg.sender,
            address(this),
            _gain * restorationPrice
        );

        IMetadata(metadataContract).updateMetadataField(
            tankContract,
            _tankId,
            "health",
            intialHealth + _gain
        );
    }

    function payForRefill(uint256 _tankId, uint256 _gain) external {
        require(
            ITanks(tankContract).ownerOf(_tankId) == msg.sender,
            "payForRefill: caller must have tank in wallet"
        );

        uint256 initialGas = IMetadata(metadataContract).metadata(
            tankContract,
            _tankId,
            "gas"
        );

        (, , , uint256 maxGas, , ) = ITanks(tankContract).tankTypes(
            ITanks(tankContract).tankTypePerTokenId(_tankId)
        );

        require(
            _gain + initialGas <= maxGas,
            "payForRefill: cannot exceed max gas"
        );

        IScrap(scrapContract).safeTransferFrom(
            msg.sender,
            address(this),
            _gain * refillPrice
        );

        IMetadata(metadataContract).updateMetadataField(
            tankContract,
            _tankId,
            "gas",
            initialGas + _gain
        );
    }

    function updateRestorationRate(uint256 _rate)
        external
        onlyRole(SHOP_MANAGER)
    {
        restorationRate = _rate;

        emit RestorationRateUpdated();
    }

    function updateRestorationPeriod(uint256 _period)
        external
        onlyRole(SHOP_MANAGER)
    {
        restorationPeriod = _period;
        emit RestorationPeriodUpdated();
    }

    function updateRestorationPrice(uint256 _price)
        external
        onlyRole(SHOP_MANAGER)
    {
        restorationPrice = _price;
        emit RestorationPriceUpdated();
    }

    function updateRefillRate(uint256 _rate) external onlyRole(SHOP_MANAGER) {
        refillRate = _rate;

        emit RefillRateUpdated();
    }

    function updateRefillPeriod(uint256 _period)
        external
        onlyRole(SHOP_MANAGER)
    {
        refillPeriod = _period;
        emit RefillPeriodUpdated();
    }

    function updateRefillPrice(uint256 _price) external onlyRole(SHOP_MANAGER) {
        refillPrice = _price;
        emit RefillPriceUpdated();
    }

    /// @param _tankContract is the address of the tank contract
    function setTankContract(address _tankContract)
        external
        onlyRole(SHOP_MANAGER)
    {
        tankContract = _tankContract;
    }

    /// @param _scrapContract is the address of the scrap contract
    function setScrapContract(address _scrapContract)
        external
        onlyRole(SHOP_MANAGER)
    {
        scrapContract = _scrapContract;
    }

    /// @param _metadataContract is the address of the metadata contract
    function setMetadataContract(address _metadataContract)
        external
        onlyRole(SHOP_MANAGER)
    {
        metadataContract = _metadataContract;
    }

    function onERC721Received(
        address,
        address _from,
        uint256 _tankId,
        bytes calldata
    ) external returns (bytes4) {
        require(
            msg.sender == tankContract,
            "onERC721Received: token must be on the tank contract"
        );

        repairTickets[_tankId] = RepairTicket({
            owner: _from,
            arrivalTime: block.timestamp,
            restorationRate: restorationRate,
            restorationPeriod: restorationPeriod,
            refillRate: refillRate,
            refillPeriod: refillPeriod
        });

        emit TankReceived(_tankId);

        return this.onERC721Received.selector;
    }

    function withdrawScrap() external onlyRole(SHOP_MANAGER) {
        IScrap(scrapContract).safeTransfer(
            msg.sender,
            IERC20(scrapContract).balanceOf(address(this))
        );
    }
}
