// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;

import "@chainlink/contracts/src/v0.8/AutomationCompatible.sol";
import "@openzeppelin/contracts/access/AccessControlEnumerable.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "./interfaces/ITanks.sol";
import "./interfaces/IScrap.sol";
import "./Tag.sol";

/// @title Tank Factory
/// @author Atlas C.O.R.P.
contract TankFactory is AccessControlEnumerable, AutomationCompatibleInterface {
    struct ProductionDetails {
        uint256 price;
        uint256 leadTime;
        bool active;
        bool developed;
    }

    struct CommissionReceipts {
        address buyer;
        uint256 tankType;
        uint256 timeOfPurchase;
        uint256 expectedTimeOfDelivery;
    }

    using SafeERC20 for IScrap;

    bytes32 public constant FACTORY_MANAGER = keccak256("FACTORY_MANAGER");

    address public tankContract;
    address public scrapContract;

    bool public businessHours;
    uint256 public leadTime;

    mapping(uint256 => CommissionReceipts) public commissionReceipts;
    uint256 public endOfQueuePointer;
    uint256 public fulfilledPointer;

    mapping(uint256 => ProductionDetails) public productionDetails;

    event TankCommissioned(uint256 indexed receiptNumber);
    event TankDelivered(uint256 indexed receiptNumber);
    event TankContractUpdated();
    event ScrapContractUpdated();
    event TankPriceUpdated(uint256 price);
    event LeadTimeUpdated();
    event FactoryOpened();
    event FactoryClosed();
    event ScrapWithdrawn();

    constructor(
        address _tankContract,
        address _scrapContract,
        uint256 _leadTime
    ) {
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        tankContract = _tankContract;
        scrapContract = _scrapContract;
        businessHours = false;
        leadTime = _leadTime;
    }

    function commissionTank(uint256 _tankType) external {
        require(businessHours, "commissionTank: factory closed");

        require(
            productionDetails[_tankType].active &&
                productionDetails[_tankType].developed
        );

        IScrap(scrapContract).safeTransferFrom(
            msg.sender,
            address(this),
            productionDetails[_tankType].price
        );

        commissionReceipts[endOfQueuePointer] = CommissionReceipts({
            buyer: msg.sender,
            timeOfPurchase: block.timestamp,
            tankType: _tankType,
            expectedTimeOfDelivery: block.timestamp + leadTime
        });

        emit TankCommissioned(endOfQueuePointer);

        unchecked {
            ++endOfQueuePointer;
        }
    }

    function addProductionDetails(
        uint256 _tankType,
        ProductionDetails calldata _productionDetails
    ) external onlyRole(FACTORY_MANAGER) {
        require(
            _tankType <= ITanks(tankContract).tankTypeCounter(),
            "addProductionDetails: tank type not valid"
        );
        productionDetails[_tankType] = _productionDetails;
    }

    function toggleProductionActive(uint256 _tankType)
        external
        onlyRole(FACTORY_MANAGER)
    {
        require(
            productionDetails[_tankType].developed,
            "toggleProductionActive: tank type not developed"
        );
        productionDetails[_tankType].developed = !productionDetails[_tankType]
            .developed;
    }

    function setProductionPrice(uint256 _tankType, uint256 _price)
        external
        onlyRole(FACTORY_MANAGER)
    {
        require(
            productionDetails[_tankType].developed,
            "setProductionPrice: tank type not developed"
        );
        productionDetails[_tankType].price = _price;
    }

    function setProductionLeadTime(uint256 _tankType, uint256 _leadTime)
        external
        onlyRole(FACTORY_MANAGER)
    {
        require(
            productionDetails[_tankType].developed,
            "setProductionPrice: tank type not developed"
        );
        productionDetails[_tankType].leadTime = _leadTime;
    }

    function checkUpkeep(
        bytes calldata /* checkData */
    )
        public
        view
        override
        returns (bool upkeepNeeded, bytes memory performData)
    {
        uint256 i = fulfilledPointer;

        for (; i < endOfQueuePointer; ) {
            if (
                commissionReceipts[i].expectedTimeOfDelivery >= block.timestamp
            ) {
                break;
            }

            unchecked {
                ++i;
            }
        }

        upkeepNeeded = fulfilledPointer != i;
        if (upkeepNeeded) {
            performData = abi.encodePacked(i);
        }
    }

    function performUpkeep(bytes calldata performData) external override {
        uint256 lastCommissionToProcess = abi.decode(performData, (uint256));

        uint256 i = fulfilledPointer;

        for (; i < lastCommissionToProcess; ) {
            if (
                commissionReceipts[i].expectedTimeOfDelivery <= block.timestamp
            ) {
                ITanks(tankContract).mint(
                    commissionReceipts[i].buyer,
                    commissionReceipts[i].tankType
                );
                emit TankDelivered(fulfilledPointer);
                unchecked {
                    fulfilledPointer++;
                }
            } else {
                break;
            }

            unchecked {
                ++i;
            }
        }
    }

    /// @param _tankContract is the address of the tank contract
    function setTankContract(address _tankContract)
        external
        onlyRole(FACTORY_MANAGER)
    {
        tankContract = _tankContract;

        emit TankContractUpdated();
    }

    /// @param _scrapContract is the address of the scrap contract
    function setScrapContract(address _scrapContract)
        external
        onlyRole(FACTORY_MANAGER)
    {
        scrapContract = _scrapContract;

        emit ScrapContractUpdated();
    }

    /// @param _leadTime is the number of seconds
    function setLeadTime(uint256 _leadTime) external onlyRole(FACTORY_MANAGER) {
        leadTime = _leadTime;

        emit LeadTimeUpdated();
    }

    function openFactory() external onlyRole(FACTORY_MANAGER) {
        businessHours = true;
        emit FactoryOpened();
    }

    function closeFactory() external onlyRole(FACTORY_MANAGER) {
        businessHours = false;
        emit FactoryClosed();
    }

    /// @param _destination is the address being withdrawn to
    function withdrawScrap(address _destination)
        external
        onlyRole(FACTORY_MANAGER)
    {
        IScrap(scrapContract).safeTransfer(
            _destination,
            IERC20(scrapContract).balanceOf(address(this))
        );
        emit ScrapWithdrawn();
    }
}
