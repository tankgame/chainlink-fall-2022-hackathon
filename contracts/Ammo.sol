// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/access/AccessControlEnumerable.sol";
import "@openzeppelin/contracts/token/ERC1155/extensions/ERC1155Supply.sol";
import "@openzeppelin/contracts/token/ERC1155/extensions/ERC1155URIStorage.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "./Tag.sol";

/// @title Ammo
/// @author Atlas C.O.R.P.
contract Ammo is
    ERC1155Supply,
    ERC1155URIStorage,
    AccessControlEnumerable,
    Ownable
{
    struct AmmoType {
        bool developed;
        string name;
        uint256 damage;
        uint256 spread;
        uint256 weight;
    }

    bytes32 public constant AMMO_DEVELOPER = keccak256("AMMO_DEVELOPER");
    bytes32 public constant AMMO_MANUFACTURER = keccak256("AMMO_MANUFACTURER");
    bytes32 public constant AMMO_CONSUMER = keccak256("AMMO_CONSUMER");

    mapping(uint256 => AmmoType) public ammoTypes;
    uint256 public ammoTypeCounter;

    event AmmoTypeCreated(uint256 _id);

    constructor(string memory _tokenURI) ERC1155(_tokenURI) {
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
    }

    /// @param _to address being minted to
    /// @param _id collection Id
    /// @param _amount is the amount of the tokens caller wants to mint
    function mint(
        address _to,
        uint256 _id,
        uint256 _amount
    ) external onlyRole(AMMO_MANUFACTURER) {
        require(ammoTypes[_id].developed, "mint: ammo type not developed");
        _mint(_to, _id, _amount, "");
    }

    /// @param _to address being minted to
    /// @param _ids collection ids
    /// @param _amounts amounts of each collection id
    function mintBatch(
        address _to,
        uint256[] calldata _ids,
        uint256[] calldata _amounts
    ) external onlyRole(AMMO_MANUFACTURER) {
        uint256 i;
        for (; i < _ids.length; ) {
            require(
                ammoTypes[_ids[i]].developed,
                "mintBatch: ammo type not developed"
            );
            unchecked {
                ++i;
            }
        }
        _mintBatch(_to, _ids, _amounts, "");
    }

    /// @param _name is the name of the collection
    /// @param _damage number of damage
    /// @param _spread number of spread
    /// @param _weight number of weight
    function createAmmoType(
        string calldata _name,
        uint256 _damage,
        uint256 _spread,
        uint256 _weight
    ) external onlyRole(AMMO_DEVELOPER) {
        ammoTypes[++ammoTypeCounter] = AmmoType({
            developed: true,
            name: _name,
            damage: _damage,
            spread: _spread,
            weight: _weight
        });

        emit AmmoTypeCreated(ammoTypeCounter);
    }

    /// @param _from address being shot from
    /// @param _id is the collection id
    /// @param _amount is the amount being shot
    function shoot(
        address _from,
        uint256 _id,
        uint256 _amount
    ) public onlyRole(AMMO_CONSUMER) {
        _burn(_from, _id, _amount);
    }

    /// @dev caller needs AMMO_DEVELOPER
    /// @notice to change the IPFS URI link
    function setURI(uint256 _tokenId, string calldata _tokenURI)
        external
        onlyRole(AMMO_DEVELOPER)
    {
        _setURI(_tokenId, _tokenURI);
    }

    /// @dev caller needs AMMO_DEVELOPER
    /// @notice to change the IPFS URI link
    /// @param _baseURI the new link
    function setBaseURI(string calldata _baseURI)
        external
        onlyRole(AMMO_DEVELOPER)
    {
        _setBaseURI(_baseURI);
    }

    function uri(uint256 tokenId)
        public
        view
        virtual
        override(ERC1155, ERC1155URIStorage)
        returns (string memory)
    {
        return ERC1155URIStorage.uri(tokenId);
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(ERC1155, AccessControlEnumerable)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }

    function _beforeTokenTransfer(
        address operator,
        address from,
        address to,
        uint256[] memory ids,
        uint256[] memory amounts,
        bytes memory data
    ) internal override(ERC1155, ERC1155Supply) {
        super._beforeTokenTransfer(operator, from, to, ids, amounts, data);
    }
}
