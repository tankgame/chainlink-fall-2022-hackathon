// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/access/AccessControlEnumerable.sol";
import "./Tag.sol";

contract Metadata is AccessControlEnumerable {
    using EnumerableSet for EnumerableSet.AddressSet;

    bytes32 public constant METADATA_CONTRACT_OPERATOR =
        keccak256("METADATA_CONTRACT_OPERATOR");
    bytes32 public constant METADATA_EDITOR = keccak256("METADATA_EDITOR");

    // Declare a set state variable
    EnumerableSet.AddressSet private approvedContracts;

    // contract => tokenID => metadata field => number value
    mapping(address => mapping(uint256 => mapping(string => uint256)))
        public metadata;

    mapping(address => string[]) public metadataFieldsByContract;
    mapping(address => mapping(string => bool))
        public metadataFieldsApprovedByContract;

    event ContractApproved(address indexed _contractAddress);
    event MetadataFieldAdded(
        address indexed _contract,
        string indexed _metadataField
    );
    event MetadataFieldUpdated(
        address indexed _contract,
        uint256 indexed _tokenId,
        string indexed _metadataField,
        uint256 _value
    );

    constructor() {
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
    }

    function addApprovedContract(address _address)
        external
        onlyRole(METADATA_CONTRACT_OPERATOR)
    {
        approvedContracts.add(_address);
        emit ContractApproved(_address);
    }

    function getApprovedContracts() external view returns (address[] memory) {
        return approvedContracts.values();
    }

    function isContractApproved(address _contract)
        external
        view
        returns (bool)
    {
        return approvedContracts.contains(_contract);
    }

    function addMetadataField(address _contract, string calldata _metadataField)
        external
        onlyRole(METADATA_CONTRACT_OPERATOR)
    {
        require(
            approvedContracts.contains(_contract),
            "addMetadataField: contract address not approved"
        );

        require(
            !metadataFieldsApprovedByContract[_contract][_metadataField],
            "addMetadataField: field already added to contract"
        );

        metadataFieldsApprovedByContract[_contract][_metadataField] = true;

        metadataFieldsByContract[_contract].push(_metadataField);

        emit MetadataFieldAdded(_contract, _metadataField);
    }

    function updateMetadataField(
        address _contract,
        uint256 _tokenId,
        string calldata _metadataField,
        uint256 _value
    ) public onlyRole(METADATA_EDITOR) {
        require(
            approvedContracts.contains(_contract),
            "updateMetadataField: contract address not approved"
        );

        require(
            metadataFieldsApprovedByContract[_contract][_metadataField],
            "updateMetadataField: metadata field missing from contract"
        );

        metadata[_contract][_tokenId][_metadataField] = _value;

        emit MetadataFieldUpdated(_contract, _tokenId, _metadataField, _value);
    }
}
