// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/access/AccessControlEnumerable.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "./interfaces/IAmmo.sol";
import "./interfaces/IScrap.sol";
import "./interfaces/ITanks.sol";
import "./Tag.sol";

/// @title Stockpile
/// @author Atlas C.O.R.P.
contract Stockpile is AccessControlEnumerable {
    enum ClaimState {
        OFF,
        ON
    }

    using SafeERC20 for IScrap;

    bytes32 public constant ARMORY_MANAGER = keccak256("ARMORY_MANAGER");

    address public tankContract;
    address public scrapContract;
    address public ammoContract;

    uint256 public counter;
    uint256 public immutable maxClaim;
    uint256 public immutable AMOUNT_OF_SCRAP;
    uint256 public immutable AMOUNT_OF_AMMO;
    uint256 public constant TANK_TYPE = 1;
    uint256 public constant AMMO_TYPE = 1;

    ClaimState public claimState;

    mapping(address => bool) public claimedAddress;
    event Claimed(address indexed soldier);

    constructor(
        address _tankContract,
        address _scrapContract,
        address _ammoContract,
        uint256 _maxClaim,
        uint256 _scrap,
        uint256 _ammo
    ) {
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        tankContract = _tankContract;
        scrapContract = _scrapContract;
        ammoContract = _ammoContract;
        maxClaim = _maxClaim;
        AMOUNT_OF_SCRAP = _scrap;
        AMOUNT_OF_AMMO = _ammo;
    }

    function stockpileClaim() external {
        require(
            claimState == ClaimState.ON,
            "stockpileClaim: must be ON in order to claim"
        );
        require(
            !claimedAddress[msg.sender],
            "stockpileClaim: caller already claimed"
        );
        require(counter < maxClaim, "stockpileClaim: no more left to claim");

        IScrap(scrapContract).collect(msg.sender, AMOUNT_OF_SCRAP);
        ITanks(tankContract).mint(msg.sender, TANK_TYPE);
        IAmmo(ammoContract).mint(msg.sender, AMMO_TYPE, AMOUNT_OF_AMMO);

        claimedAddress[msg.sender] = true;

        emit Claimed(msg.sender);

        unchecked {
            ++counter;
        }
    }

    /// @param _claimState is a number 0 or 1 to set the state of contract
    function setState(ClaimState _claimState)
        external
        onlyRole(ARMORY_MANAGER)
    {
        claimState = _claimState;
    }

    /// @param _ammoContract is the address of the ammo contract
    function setAmmoContract(address _ammoContract)
        external
        onlyRole(ARMORY_MANAGER)
    {
        ammoContract = _ammoContract;
    }

    /// @param _tankContract is the address of the tank contract
    function setTankContract(address _tankContract)
        external
        onlyRole(ARMORY_MANAGER)
    {
        tankContract = _tankContract;
    }

    /// @param _scrapContract is the address of the scrap contract
    function setScrapContract(address _scrapContract)
        external
        onlyRole(ARMORY_MANAGER)
    {
        scrapContract = _scrapContract;
    }
}
