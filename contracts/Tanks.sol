// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/access/AccessControlEnumerable.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
import "./interfaces/IMetadata.sol";

/// @title Tanks
/// @author Atlas C.O.R.P.
contract Tanks is ERC721Enumerable, AccessControlEnumerable, Ownable {
    struct TankType {
        string name;
        string description;
        uint256 maxHealth;
        uint256 maxGas;
        uint256 accuracy;
        bool developed;
    }

    bytes32 public constant TANK_MANUFACTURER = keccak256("TANK_MANUFACTURER");
    bytes32 public constant TANK_DESTROYER = keccak256("TANK_DESTROYER");
    bytes32 public constant TANK_DEVELOPER = keccak256("TANK_DEVELOPER");

    mapping(uint256 => TankType) public tankTypes;
    mapping(uint256 => uint256) public tankTypePerTokenId;
    uint256 public tankTypeCounter;

    string public baseURI;
    uint256 public numberOfTanks;

    address public metadataContract;

    event TankTypeCreated(uint256 tankType);

    constructor(
        string memory _name,
        string memory _symbol,
        string memory _tokenURI,
        address _metadataContract
    ) ERC721(_name, _symbol) {
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        baseURI = _tokenURI;
        metadataContract = _metadataContract;
    }

    function mint(address _destination, uint256 _tankType)
        external
        onlyRole(TANK_MANUFACTURER)
    {
        require(
            tankTypes[_tankType].developed,
            "mint: tank type not developed"
        );

        _safeMint(_destination, ++numberOfTanks);
        tankTypePerTokenId[numberOfTanks] = _tankType;

        IMetadata(metadataContract).updateMetadataField(
            address(this),
            numberOfTanks,
            "health",
            tankTypes[_tankType].maxHealth
        );

        IMetadata(metadataContract).updateMetadataField(
            address(this),
            numberOfTanks,
            "gas",
            tankTypes[_tankType].maxGas
        );
    }

    function destroyTank(uint256 _tankId) external onlyRole(TANK_DESTROYER) {
        _burn(_tankId);
    }

    /// @param _URI is the new IPFS link
    function setBaseURI(string memory _URI) external onlyOwner {
        baseURI = _URI;
    }

    function _baseURI() internal view override(ERC721) returns (string memory) {
        return baseURI;
    }

    function createTankType(
        string calldata _name,
        string calldata _description,
        uint256 _maxHealth,
        uint256 _maxGas,
        uint256 _accuracy
    ) external onlyRole(TANK_DEVELOPER) {
        tankTypes[++tankTypeCounter] = TankType({
            name: _name,
            description: _description,
            maxHealth: _maxHealth,
            maxGas: _maxGas,
            accuracy: _accuracy,
            developed: true
        });

        emit TankTypeCreated(tankTypeCounter);
    }

    /// @param _metadataContract is the address of the ammo contract
    function updateMetadataContract(address _metadataContract)
        external
        onlyRole(TANK_DEVELOPER)
    {
        metadataContract = _metadataContract;
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(ERC721Enumerable, AccessControlEnumerable)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }
}
