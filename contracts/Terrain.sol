// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/access/AccessControlEnumerable.sol";
import "./Tag.sol";

/// @title Terrain
/// @author Atlas C.O.R.P.
contract Terrain is AccessControlEnumerable {
    bytes32 public constant CARTOGRAPHER = keccak256("CARTOGRAPHER");

    // uint256 public constant MINIMUM_TERRAIN_WIDTH = 1024;

    mapping(int256 => int256) public terrain;
    int256 public constant width = 1024;
    int256 public gravity;

    constructor(int256 _gravity) {
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        gravity = _gravity;
        int256 i;
        for (; i < width; ) {
            terrain[i] = 100000000; //100M
            unchecked {
                ++i;
            }
        }
    }

    /// @param _terrain is the new number for terrain
    function updateTerrain(int256[] calldata _terrain)
        external
        onlyRole(CARTOGRAPHER)
    {
        uint256 i;
        for (; i < _terrain.length; ) {
            terrain[int256(i)] = _terrain[i];

            unchecked {
                ++i;
            }
        }
    }

    function getTerrain() public view returns (int256[] memory) {
        int256[] memory terrainPath = new int256[](uint256(width));

        for (uint256 i; i < uint256(width); i++) {
            terrainPath[i] = terrain[int256(i)];
        }

        return terrainPath;
    }

    /// @param _gravity is the new number for gravity
    function updateGravity(int256 _gravity) external onlyRole(CARTOGRAPHER) {
        gravity = _gravity;
    }
}
