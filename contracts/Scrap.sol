// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/access/AccessControlEnumerable.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";
import "./Tag.sol";

/// @title Scrap
/// @author Atlas C.O.R.P.
contract Scrap is ERC20Burnable, AccessControlEnumerable {
    bytes32 public constant SCRAP_MINER = keccak256("SCRAP_MINER");

    constructor(string memory _name, string memory _symbol)
        ERC20(_name, _symbol)
    {
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
    }

    /// @param _destination is the address the caller wants to mint to
    /// @param _amount is the number being minted
    function collect(address _destination, uint256 _amount)
        external
        onlyRole(SCRAP_MINER)
    {
        _mint(_destination, _amount);
    }

    function decimals() public view virtual override returns (uint8) {
        return 0;
    }
}
