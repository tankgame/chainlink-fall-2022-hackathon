const { BN, expectEvent, expectRevert } = require("@openzeppelin/test-helpers");
const soldierName = artifacts.require("SoldierName");
const scrap = artifacts.require("Scrap");
const delay = require("delay");
const cliProgress = require("cli-progress");
const { italic } = require("ansi-colors");
const { web3 } = require("@openzeppelin/test-helpers/src/setup");

contract("setting soldier name", function (accounts) {
  let instance;
  before("Initializing...", async function () {
    instance = await soldierName.deployed();
    scrapInstance = await scrap.deployed();

    const scrapMinerRole = web3.utils.sha3("SCRAP_MINER");

    await grantRole(scrapMinerRole, accounts[0], scrapInstance);

    let amountToMint = web3.utils.toWei("2000");

    await scrapInstance.collect(accounts[0], amountToMint);

    await scrapInstance.approve(
      instance.address,
      web3.utils.toWei("10000000000000000000", "ether")
    );
  });
  it("Should fail when you try to update name when your name length is 0", async () => {
    instance = await soldierName.deployed();

    expectRevert(instance.updateName("Ryan"), "updateName: name already set");
  });

  it("Should fail when you try to set name with under the minimum requirement", async () => {
    expectRevert(instance.setName("Ry"), "setName: name not long enough");
  });

  it("Should allow me to set my name", async () => {
    const name = "Ryan";
    await instance.setName(name);
  });

  it("Should fail when trying to setName after setting", async () => {
    const name = "Ryan";
    expectRevert(instance.setName(name), "setName: name already set");
  });

  it("Should fail when trying to updateNow when name is already in use", async () => {
    const name = "Ryan";
    expectRevert(
      instance.updateNameNow(name),
      "updateNameNow: name already in use"
    );
  });

  it("Should fail when trying to updateName because of cooldown", async () => {
    const name = "Ryan";
    expectRevert(
      instance.updateName(name),
      "updateName: caller must wait for cooldown period"
    );
  });

  it("Should allow me to set my name to accounts[1]", async () => {
    const name = "James";
    await instance.setName(name, { from: accounts[1] });
  });

  it("Should return me the price of scrap", async () => {
    let price = await instance.SCRAP_PRICE();
    console.log(`The price of scrap is ${price}`);
  });

  it("Should fail when trying to updateNow when name is already in use", async () => {
    const name = "Ryan";
    expectRevert(
      instance.updateNameNow(name),
      "updateNameNow: name already in use"
    );
  });

  it("Should allow me to set my name to accounts[2]", async () => {
    const name = "Ryan1";
    await instance.setName(name, { from: accounts[2] });
  });

  it("Should allow you to updateNow when paying with scrap", async () => {
    const name = "Ryan2";
    await instance.updateNameNow(name);
  });
});

async function grantRole(role, address, instance) {
  console.log(`Granting ${address} the ${role} ROLE`);
  console.log(
    "=================================================================="
  );
  console.log("Confirming Role");
  await instance.grantRole(role, address);
  let hasRole = await instance.hasRole(role, address);
  console.log(`${address}: ${hasRole}`);
}
