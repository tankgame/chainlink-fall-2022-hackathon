const ProjectileCalculator = artifacts.require("ProjectileCalculator");

contract("ProjectileCalculator", function (/* accounts */) {
  it("should have the correct value for cos(0)", async function () {
    let instance = await ProjectileCalculator.deployed();
    let result = await instance.cos(0);
    console.log(`Cos(0): ${result}`);
    return true;
  });

  it("should have the correct value for cos(8192)", async function () {
    let instance = await ProjectileCalculator.deployed();
    let result = await instance.cos(8192);
    console.log(`Cos(8192): ${result}`);
    return true;
  });

  it("should have the correct value for sin(0)", async function () {
    let instance = await ProjectileCalculator.deployed();
    let result = await instance.sin(0);
    console.log(`Sin(0): ${result}`);
    return true;
  });

  it("should have the correct value for sin(8192)", async function () {
    let instance = await ProjectileCalculator.deployed();
    let result = await instance.sin(8192);
    console.log(`Sin(8192): ${result}`);
    return true;
  });

  it("should have the correct value vector components", async function () {
    let instance = await ProjectileCalculator.deployed();
    let vectorX = await instance.getVectorX(2048, 1);
    let vectorY = await instance.getVectorY(2048, 1);
    console.log(`
    Vector X: ${vectorX}
    Vector Y: ${vectorY}
    `);
    return true;
  });

  it("pathX", async function () {
    let instance = await ProjectileCalculator.deployed();
    await instance.makePathX(2048, 1, 0);
    let path = await instance.path(2);
    console.log(`${path.toString()}`);
    return true;
  });
});
