const { BN, expectEvent, expectRevert } = require("@openzeppelin/test-helpers");
const tanks = artifacts.require("Tanks");
const ammo = artifacts.require("Ammo");
const scrap = artifacts.require("Scrap");
const stockpile = artifacts.require("Stockpile");
const AmmoFactory = artifacts.require("AmmoFactory");

contract("testing stockpile", function (accounts) {
	let instance;
	let scrapInstance;
	let ammoInstance;
	let ammoFactoryInstance;
	before("Initializing...", async function () {
		instance = await stockpile.deployed();
		scrapInstance = await scrap.deployed();
		ammoInstance = await ammo.deployed();
		tankInstance = await tanks.deployed();
		ammoFactoryInstance = await AmmoFactory.deployed();
	});

	it("should fail when trying to claim when contract is OFF", async () => {
		expectRevert(
			instance.stockpileClaim(accounts[0]),
			"stockpileClaim: must be ON in order to claim"
		);
	});

	it("Is checking ammoTypeCouner on ammo contract", async () => {
		let ammoCount = await ammoInstance.ammoTypeCounter();

		assert.equal(ammoCount.toString(), "2");
		console.log(`Ammo count developed is ${ammoCount.toString()}`);
	});

	it("making sure all values are correct", async () => {
		const scrap = await instance.AMOUNT_OF_SCRAP();
		const ammo = await instance.AMOUNT_OF_AMMO();
		const maxClaim = await instance.maxClaim();

		console.log(`Scrap balance is ${scrap.toString()}`);
		console.log(`Ammo balance is ${ammo.toString()}`);
		console.log(`Max claim is ${maxClaim.toString()}`);
	});

	it("should allow caller to claim", async () => {
		const contractOn = 1;
		await instance.setState(contractOn);

		await instance.stockpileClaim();
	});

	it("checking balance of claim totals for accounts[0]", async () => {
		const address = accounts[0];
		const collection1 = 1;

		const ammoBalance = await ammoInstance.balanceOf(address, collection1);
		const tankBalance = await tankInstance.balanceOf(address);
		const scrapBalance = await scrapInstance.balanceOf(address);
		assert.equal(ammoBalance.toString(), "100");
		console.log(
			`Account 0 has minted ${ammoBalance.toString()} ammo NFT from collection 1`
		);
		assert.equal(tankBalance.toString(), "1");
		console.log(
			`Account 0 has minted ${tankBalance.toString()} tank NFT from collection 1`
		);
		assert.equal(scrapBalance.toString(), "200");
		console.log(`Account 0 has minted ${scrapBalance.toString()} scrap`);
	});

	it("should fail when caller already did a claim", async () => {
		expectRevert(
			instance.stockpileClaim(accounts[0]),
			"stockpileClaim: caller already claimed"
		);
	});
});
