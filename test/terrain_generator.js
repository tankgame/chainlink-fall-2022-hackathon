const TerrainGenerator = artifacts.require("TerrainGenerator");
const Table = require('cli-table');

const settings = {
  samplePeriods: [0,16,32,64,128]
}

contract("TerrainGenerator", function (/* accounts */) {

  it("should have random values in naive terrain", async function () {

    const table = new Table();

    let instance = await TerrainGenerator.deployed();
    let width = await instance.width();
    let height;

    let terrain = await instance.getSampledTerrain(0);
    console.log(terrain.map(x=>x.toString()));
    // for(let i = 0; i < width.toNumber(); i++){
      
      // height = await instance.naiveTerrain(i);
      // assert.notEqual(height.toString(), "0", `Height at ${i} is ${height.toString()}`);
      // table.push([i, height.toString()]);
    // }
    // console.log(table.toString());
  });

  for(let i = 0; i < settings.samplePeriods.length; i++){
    it(`should have sampled values with sample period ${settings.samplePeriods[i]}`, async function () {
      let instance = await TerrainGenerator.deployed();
      let sampledTerrain = await instance.getSampledTerrain(settings.samplePeriods[i]);
      console.log(sampledTerrain.map(x=>x.toString()));
    });
  }

  // it("should have sampled values with sample period 2", async function () {
  //   let instance = await TerrainGenerator.deployed();
  //   // await instance.populateSampledTerrain(
  //   //   2
  //   // );
  //   let sampledTerrain = await instance.getSampledTerrain(2);
  //   console.log(sampledTerrain.map(x=>x.toString()));
  // });

  // it("should have sampled values with sample period 4", async function () {
  //   let instance = await TerrainGenerator.deployed();
  //   // await instance.populateSampledTerrain(
  //   //   4
  //   // );
  //   let sampledTerrain = await instance.getSampledTerrain(4);
  //   console.log(sampledTerrain.map(x=>x.toString()));
  // });

  // it("should have sampled values with sample period 4", async function () {
  //   let instance = await TerrainGenerator.deployed();
  //   // await instance.populateSampledTerrain(
  //   //   8
  //   // );
  //   let sampledTerrain = await instance.getSampledTerrain(8);
  //   console.log(sampledTerrain.map(x=>x.toString()));
  // });

  it("should have smoother terrain", async function () {
    let instance = await TerrainGenerator.deployed();
    // await instance.populateSmoothTerrain(
    //   [0,2,4,8],
    //   [1,10,100,1000],
    //   0,
    //   511
    // );

    // await instance.populateSmoothTerrain(
    //   [0,2,4,8],
    //   [1,10,100,1000],
    //   512,
    //   1023
    // );

    let smoothTerrain = await instance.getSmoothTerrain();
    console.log(smoothTerrain.map(x=>x.toString()));
  });

});
