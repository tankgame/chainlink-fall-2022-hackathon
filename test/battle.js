const { BN, expectEvent, expectRevert } = require("@openzeppelin/test-helpers");
const { web3 } = require("@openzeppelin/test-helpers/src/setup");
const Tanks = artifacts.require("Tanks");
const Ammo = artifacts.require("Ammo");
const Scrap = artifacts.require("Scrap");
const Stockpile = artifacts.require("Stockpile");
const Battle = artifacts.require("Battle");
const VRFCoordinatorV2Mock = artifacts.require("VRFCoordinatorV2Mock");
const Terrain = artifacts.require("Terrain");

let tanksInstance;
let ammoInstance;
let scrapInstance;
let stockpileInstance;
let battleInstance;
let vrfInstance;
let terrainInstance;

let mapScale;

contract("battle", function (accounts) {
  before("Initializing...", async function () {
    tanksInstance = await Tanks.deployed();
    ammoInstance = await Ammo.deployed();
    scrapInstance = await Scrap.deployed();
    stockpileInstance = await Stockpile.deployed();
    battleInstance = await Battle.deployed();
    vrfInstance = await VRFCoordinatorV2Mock.deployed();
    terrainInstance = await Terrain.deployed();

    mapScale = await battleInstance.MAP_SCALE();

    console.log("Activating Stockpile");
    await stockpileInstance.setState(1);

    console.log("Approving Battle to pull Tanks from account 0");
    await tanksInstance.setApprovalForAll(Battle.address, true, {
      from: accounts[0],
    });
    console.log("Approving Battle to pull Tanks from account 1");
    await tanksInstance.setApprovalForAll(Battle.address, true, {
      from: accounts[1],
    });
    console.log("Validating Battle VRF Settings");
    let coordinatorAddress = await battleInstance.COORDINATOR();
    assert.equal(
      coordinatorAddress,
      VRFCoordinatorV2Mock.address,
      "coordinator contract does not match"
    );
  });

  it("should allow account 0 and account 1 to claim from the stockpile", async function () {
    await checkBalances(accounts[0], 0, 0, 0);
    await checkBalances(accounts[1], 0, 0, 0);

    await stockpileInstance.stockpileClaim({ from: accounts[0] });
    await stockpileInstance.stockpileClaim({ from: accounts[1] });

    await checkBalances(accounts[0], 1, 100, 200);
    await checkBalances(accounts[1], 1, 100, 200);
  });

  it("should fail if account 0 creates a game using a tank owned by account 1", async function () {
    await expectRevert(
      battleInstance.createGame(2),
      "ERC721: transfer from incorrect owner."
    );
  });

  it("should allow account 0 to create game 1 using tank 1", async function () {
    let tankNumber = 1;
    await battleInstance.createGame(tankNumber, { from: accounts[0] });
    let gameId = await getGameCounter();
    assert.equal(gameId.toString(), 1, "game counter does not match");
    await checkBalances(accounts[0], 0, 100, 200);
    await checkBalances(battleInstance.address, 1, 0, 0);

    await getGameData(1);
  });

  it("should allow account 1 to join game 1 using tank 2", async function () {
    let tankNumber = 2;
    let gameId = 1;
    await battleInstance.joinGame(gameId, tankNumber, {
      from: accounts[1],
    });
    await checkBalances(accounts[1], 0, 100, 200);
    await checkBalances(battleInstance.address, 2, 0, 0);

    await getGameData(1);
  });

  it("should initialize the game when the random words are returned from chainlink", async function () {
    // 0 - home team on left
    // 250 - left window offset
    // 250 - right window offset
    // 0 - home team first
    let receipt = await vrfInstance.fulfillRandomWordsWithOverride(
      1,
      Battle.address,
      [0, 250, 250, 0]
    );
    console.log("Gas Used to Intialize Game: ", receipt.receipt.gasUsed);
    await getGameData(1);
  });

  it("should allow home (account 0) to fire", async function () {
    let gameId = 1;
    let ammo = 1;
    let angle = 0;
    let power = 500000;
    let gamedata = await getGameData(1);

    const yPosFire = await terrainInstance.terrain(gamedata.home.location);
    const yPosTarget = await terrainInstance.terrain(gamedata.away.location);

    const xPosFire = web3.utils.toBN(
      Number(gamedata.home.location) * Number(mapScale)
    );
    const xposTarget = web3.utils.toBN(
      Number(gamedata.away.location) * Number(mapScale)
    );

    console.log("bn", xPosFire, xPosFire.toString());

    const detectColision = await battleInstance.detectCollision(
      {
        x: xPosFire.toString(),
        y: yPosFire.toString(),
      },
      {
        x: xposTarget.toString(),
        y: yPosTarget.toString(),
      },
      1,
      (100).toString(),
      0
    );
    // const detectColisionGas = await battleInstance.methods
    //   .detectCollision(
    //     {
    //       x: xPosFire.toString(),
    //       y: yPosFire.toString(),
    //     },
    //     {
    //       x: xposTarget.toString(),
    //       y: yPosTarget.toString(),
    //     },
    //     0,
    //     (1).toString(),
    //     0
    //   )
    //   .estimateGas();

    // console.log({ detectColisionGas });
    const sine = await battleInstance.sin((100).toString());
    console.log({ sin: sine.toString() });
    // const path = await battleInstance.drawPathX(
    //   {
    //     x: xPosFire.toString(),
    //     y: yPosFire.toString(),
    //   },
    //   {
    //     x: xposTarget.toString(),
    //     y: yPosTarget.toString(),
    //   },
    //   (100).toString(),
    //   (100).toString(),
    //   0
    // );

    // console.log({
    //   detectColision,
    //   path: path[0].map((point) => point.toString()),
    //   pathIteratins: path[1].toString(),
    // });

    console.log("FIRE!");
    let receipt = await battleInstance.fire(gameId, ammo, angle, power);
    printGas(receipt, "Fire");
    receipt = await vrfInstance.fulfillRandomWordsWithOverride(
      2,
      Battle.address,
      [0, 1, 1, 1, 1]
    );
    printGas(receipt, "Chainlink Return Fire");
    console.log("Txn: ", receipt.tx);
    await getGameData(1);
    await checkBalances(accounts[0], 0, 99, 200);
  });
});

async function checkBalances(
  account,
  expectedTank,
  expectedAmmo,
  expectedScrap
) {
  let tankBalance = await tanksInstance.balanceOf(account);
  let ammoBalance = await ammoInstance.balanceOf(account, 1);
  let scrapBalance = await scrapInstance.balanceOf(account);
  assert.equal(tankBalance.toString(), expectedTank);
  assert.equal(ammoBalance.toString(), expectedAmmo);
  assert.equal(scrapBalance.toString(), expectedScrap);
}

async function getGameCounter() {
  let counter = await battleInstance.gameCounter();
  return counter;
}

async function getGameData(gameId) {
  let gameData = await battleInstance.gameData(gameId);
  console.log(gameData);
  return gameData;
}

function printGas(receipt, tag) {
  console.log(`Gas Used ${tag}: ${receipt.receipt.gasUsed}`);
}
