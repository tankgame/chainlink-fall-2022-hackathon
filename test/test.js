const {
  BN,
  expectEvent,
  expectRevert,
  time,
} = require("@openzeppelin/test-helpers");
const tanks = artifacts.require("Tanks");
const ammo = artifacts.require("Ammo");
const scrap = artifacts.require("Scrap");
const ammoFactory = artifacts.require("AmmoFactory");
const tankFactory = artifacts.require("TankFactory");
const delay = require("delay");
const cliProgress = require("cli-progress");

// contract("minting", function (accounts) {
//   let ammoInstance;
//   let tankInstance;
//   let scrapInstance;
//   let tankFactoryInstance;
//   let ammoFactoryInstance;
//   let delayTime;
//   before("Initializing...", async function () {
//     ammoInstance = await ammo.deployed();
//     tankInstance = await tanks.deployed();
//     scrapInstance = await scrap.deployed();
//     ammoFactoryInstance = await ammoFactory.deployed();
//     tankFactoryInstance = await tankFactory.deployed();
//     delayTime = 1000;

//     let role = web3.utils.sha3("SCRAP_MINER");
//     let role1 = web3.utils.sha3("FACTORY_MANAGER");

//     await grantRole(role, accounts[0], scrapInstance);
//     await grantRole(role1, accounts[0], ammoFactoryInstance);
//     await grantRole(role1, accounts[0], tankFactoryInstance);

//     await scrapInstance.approve(
//       ammoFactory.address,
//       web3.utils.toWei("10000000000000000000", "ether")
//     );
//     await scrapInstance.approve(
//       tankFactory.address,
//       web3.utils.toWei("10000000000000000000", "ether")
//     );

//     destination = accounts[0];
//     amount = 10000;

//     await scrapInstance.collect(destination, amount);
//     let balanceOfScrap = await scrapInstance.balanceOf(destination);
//     assert.equal(balanceOfScrap.toString(), "10000");
//     console.log(`Account 0 has ${balanceOfScrap.toString()} scrap`);
//   });

//   it("Should be able to mint a tank", async () => {
//     const destination = accounts[0];

//     await tankInstance.mint(destination);

//     const numberOfTanks = await tankInstance.numberOfTanks();
//     console.log(`Account 0 has minted ${numberOfTanks.toString()} tank NFT`);
//   });

//   it("Should be able to mint 1 ammo NFT from collection 1", async () => {
//     const address = accounts[0];
//     const collectionId = 1;
//     const amount = 1;

//     await ammoInstance.mint(address, collectionId, amount);

//     const balance = await ammoInstance.balanceOf(address, collectionId);
//     assert.equal(balance.toString(), "1");
//     console.log(
//       `Account 0 has minted ${balance.toString()} ammo NFT from collection 1`
//     );

//     let ammoCount = await ammoInstance.ammoTypeCounter();

//     assert.equal(ammoCount.toString(), "2");
//     console.log(`Ammo count developed is ${ammoCount.toString()}`);
//   });

//   it("Should be able to mint 1 ammo NFT from collection 2", async () => {
//     const address = accounts[0];
//     const collectionId = 2;
//     const amount = 1;

//     await ammoInstance.mint(address, collectionId, amount);

//     const balance = await ammoInstance.balanceOf(address, collectionId);
//     assert.equal(balance.toString(), "1");
//     console.log(
//       `Account 0 has minted ${balance.toString()} ammo NFT from collection 2`
//     );

//     let ammoCount = await ammoInstance.ammoTypeCounter();

//     assert.equal(ammoCount.toString(), "2");
//     console.log(`Ammo count developed is ${ammoCount.toString()}`);
//   });

//   it("Should be able to mintBatch 2 ammo NFT's from collection 1", async () => {
//     const address = accounts[0];
//     const collectionId = [1];
//     const amount = [2];

//     await ammoInstance.mintBatch(address, collectionId, amount);

//     const balance = await ammoInstance.balanceOf(address, collectionId);
//     assert.equal(balance.toString(), "3");
//     console.log(
//       `Account 0 has minted ${balance.toString()} ammo NFT from collection 1`
//     );
//   });

//   it("Should be able to mintBatch 2 ammo NFT's from collection 1 and 2", async () => {
//     const address = accounts[0];
//     const collectionIds = [1, 2];
//     const amount = [2, 2];
//     const collection1 = 1;
//     const collection2 = 2;

//     await ammoInstance.mintBatch(address, collectionIds, amount);

//     const balance1 = await ammoInstance.balanceOf(address, collection1);
//     const balance2 = await ammoInstance.balanceOf(address, collection2);
//     assert.equal(balance1.toString(), "5");
//     console.log(
//       `Account 0 has minted ${balance1.toString()} ammo NFT from collection 1`
//     );
//     assert.equal(balance2.toString(), "3");
//     console.log(
//       `Account 0 has minted ${balance2.toString()} ammo NFT from collection 2`
//     );
//   });

//   it("should be able to turn the factory contract business hours to active", async () => {
//     await ammoFactoryInstance.openFactory();

//     const businessHours = await ammoFactoryInstance.businessHours();
//     assert.equal(businessHours, true);
//     console.log(`The business hours are set to ${businessHours}`);
//   });

//   it("should be able to set the production details", async () => {
//     let ammoType = 1;
//     let productionsDetails = { active: true, scrapToAmmo: "1" };
//     await ammoFactoryInstance.setProductionDetails(
//       ammoType,
//       productionsDetails
//     );
//   });

//   it("should be able to press ammo", async () => {
//     const ammoType = 1;
//     const amount = 1;

//     await ammoFactoryInstance.pressAmmo(ammoType, amount);
//   });

//   it("testing balance of scrap on ammo factory contract", async () => {
//     let ammoFactoryBalance = await scrapInstance.balanceOf(
//       ammoFactoryInstance.address
//     );

//     console.log(
//       `The ammo factory contract balance is ${ammoFactoryBalance.toString()}`
//     );
//   });

//   it("testing withdraw on ammo factory", async () => {
//     await ammoFactoryInstance.withdrawScrap();

//     let ammoFactoryBalance = await scrapInstance.balanceOf(
//       ammoFactoryInstance.address
//     );

//     console.log(
//       `The ammo factory contract balance is ${ammoFactoryBalance.toString()}`
//     );
//   });

//   it("should be able to turn on business hours for the tank factory", async () => {
//     await tankFactoryInstance.openFactory();

//     const businessHours = await tankFactoryInstance.businessHours();
//     assert.equal(businessHours, true);
//     console.log(`The business hours are set to ${businessHours}`);
//   });

//   it("should be able to comission a tank from the tank factory", async () => {
//     await tankFactoryInstance.commissionTank();
//   });

//   it("should be able to set new price for tank in scrap", async () => {
//     await tankFactoryInstance.setTankPriceInScrap(2);
//     let newPrice = await tankFactoryInstance.tankPriceInScrap();

//     assert.equal(newPrice.toString(), "2");
//     console.log(`The new price for a tank in scrap is ${newPrice.toString()}`);
//   });

//   it("testing balance of scrap on tank factory contract", async () => {
//     let tankFactoryBalance = await scrapInstance.balanceOf(
//       tankFactoryInstance.address
//     );

//     console.log(
//       `The tank factory contract balance is ${tankFactoryBalance.toString()}`
//     );
//   });

//   it("testing withdraw on tank factory", async () => {
//     let destination = accounts[0];
//     await tankFactoryInstance.withdrawScrap(destination);

//     let tankFactoryBalance = await scrapInstance.balanceOf(
//       tankFactoryInstance.address
//     );

//     console.log(
//       `The tank factory contract balance is ${tankFactoryBalance.toString()}`
//     );
//   });
// });

contract("Testing Keepers", function (accounts) {
  let ammoInstance;
  let tankInstance;
  let scrapInstance;
  let tankFactoryInstance;
  let ammoFactoryInstance;
  let delayTime;

  before("Initializing...", async function () {
    ammoInstance = await ammo.deployed();
    tankInstance = await tanks.deployed();
    scrapInstance = await scrap.deployed();
    ammoFactoryInstance = await ammoFactory.deployed();
    tankFactoryInstance = await tankFactory.deployed();
    delayTime = 20;

    let SCRAP_MINER = web3.utils.sha3("SCRAP_MINER");
    let FACTORY_MANAGER = web3.utils.sha3("FACTORY_MANAGER");

    console.log("Assigning SCRAP MINER role to Account 0");
    await grantRole(SCRAP_MINER, accounts[0], scrapInstance);
    console.log(
      "Assigning FACTORY MANAGER role to Account 0 on the ammo factory"
    );
    await grantRole(FACTORY_MANAGER, accounts[0], ammoFactoryInstance);
    console.log(
      "Assigning FACTORY MANAGER role to Account 0 on the tank factory"
    );
    await grantRole(FACTORY_MANAGER, accounts[0], tankFactoryInstance);

    console.log(
      "Approving ammo factory as an Account 0 spender on the scrap contract"
    );
    await scrapInstance.approve(
      ammoFactory.address,
      web3.utils.toWei("10000000000000000000", "ether")
    );

    console.log(
      "Approving tank factory as an Account 0 spender on the scrap contract"
    );
    await scrapInstance.approve(
      tankFactory.address,
      web3.utils.toWei("10000000000000000000", "ether")
    );

    destination = accounts[0];
    amount = 10000;

    console.log("Collecting Scrap");
    await scrapInstance.collect(destination, amount);
    let balanceOfScrap = await scrapInstance.balanceOf(destination);
    assert.equal(balanceOfScrap.toString(), "10000");
    console.log(`Account 0 has ${balanceOfScrap.toString()} scrap`);
  });

  it("testing commissionTank with keepers", async () => {
    const bar1 = new cliProgress.SingleBar({}, cliProgress.Presets.legacy);
    await tankFactoryInstance.openFactory();
    console.log("Commissioning Tank");
    await tankFactoryInstance.commissionTank();
    console.log("Checking Upkeep on Tank Factory");
    let checkUpkeep = await tankFactoryInstance.checkUpkeep("0x");
    console.log("Verifying no upkeep needed");
    assert.equal(checkUpkeep[0], false);
    console.log("Checking balance");
    let balance = await tankInstance.balanceOf(accounts[0]);
    console.log("Verifying zero balance");
    assert.equal(balance, 0);
    console.log(checkUpkeep);
    let incrementTimestamp;
    incrementTimestamp = await tankFactoryInstance.getBlockTimestamp();
    console.log(`Current Timestamp: ${incrementTimestamp}`);
    bar1.start(delayTime, 0);
    for (let i = 0; i < delayTime; i++) {
      await delay(1000);

      bar1.increment();
    }
    bar1.stop();

    // console.log("Advancing the ganache clock 20 seconds");
    // await time.increase(20);
    console.log("Mining empty block to advance the clock");
    await time.advanceBlock();

    incrementTimestamp = await tankFactoryInstance.getBlockTimestamp();
    console.log(`Current Timestamp: ${incrementTimestamp}`);
    checkUpkeep = await tankFactoryInstance.checkUpkeep("0x");

    checkAndPerformUpkeep(tankFactoryInstance);

    balance = await tankInstance.balanceOf(accounts[0]);
    assert.equal(balance.toString(), "1");
  });
});

async function checkAndPerformUpkeep(instance) {
  checkUpkeep = await instance.checkUpkeep("0x");

  if (checkUpkeep[0]) {
    console.log("Performing upkeep");
    console.log("Perform Data: ", checkUpkeep[1]);
    await instance.performUpkeep(checkUpkeep[1]);
  } else {
    console.log("skipping upkeep");
  }
}

async function grantRole(role, address, instance) {
  console.log(`Granting ${address} the ${role} ROLE`);
  console.log(
    "=================================================================="
  );
  console.log("Confirming Role");
  await instance.grantRole(role, address);
  let hasRole = await instance.hasRole(role, address);
  console.log(`${address}: ${hasRole}`);
}
